//
//  Majordomo Protocol client example - asynchronous.
//  Uses the mdcli API to hide all MDP aspects
//

package main

import (
	"fmt"
	"log"

	"gitlab.com/plantd/go-zapi/mdp"
)

var doneEcho = make(chan bool)
var doneAcquire = make(chan bool)
var doneAnalyze = make(chan bool)
var doneControl = make(chan bool)
var doneRecord = make(chan bool)
var doneState = make(chan bool)

func main() {

	go echo()
	go acquire()
	go analyze()
	go control()
	go record()
	go state()

	<-doneEcho
	<-doneAcquire
	<-doneAnalyze
	<-doneControl
	<-doneRecord
	<-doneState
}

func echo() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("echo", "test-echo")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for echo\n", count)
	doneEcho <- true
}

func acquire() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("acquire", "test-acquire")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for acquire\n", count)
	doneAcquire <- true
}

func analyze() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("analyze", "test-analyze")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for analyze\n", count)
	doneAnalyze <- true
}

func control() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("control", "test-control")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for control\n", count)
	doneControl <- true
}

func record() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("record", "test-record")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for record\n", count)
	doneRecord <- true
}

func state() {
	session, _ := mdp.NewClient("tcp://localhost:7200")

	var count int
	for count = 0; count < 100000; count++ {
		err := session.Send("state", "test-state")
		if err != nil {
			log.Println("Send:", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err := session.Recv()
		if err != nil {
			log.Println("Recv:", err)
			break
		}
	}

	fmt.Printf("%d replies received for state\n", count)
	doneState <- true
}
