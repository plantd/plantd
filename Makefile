.DEFAULT_GOAL = build

-include build/common.mk

BUILD_DEPS += root/build
STATIC_DEPS += root/static
IMAGE_DEPS += root/image
CONTAINER_DEPS += root/container
INSTALL_DEPS += root/install
UNINSTALL_DEPS += root/uninstall

-include build/master.mk
-include build/broker.mk
-include build/configure.mk
-include build/events.mk
-include build/metrics.mk

BUILD_DEPS += root/build/post
INSTALL_DEPS += root/install/post

all: build

root/build: schema ; $(info $M Building projects)
root/static: ; $(info $M Build static binaries for containers)
root/image: static ; $(info $M Building application images)
root/container: image ; $(info $M Running application containers)

root/install: ; $(info $M Installing plantd)
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@mkdir -p /run/plantd

root/build/post: ; $(info $M Finished building projects)
root/install/post:
	@systemctl daemon-reload

root/uninstall: ; $(info $M Uninstalling plantd)

.PHONY: version
version:
	@echo $(VERSION)

.PHONY: lint
lint: ; $(info $M Linting the files)
	@./tools/checks lint

.PHONY: test
test: ; $(info $M Running unit tests)
	@./tools/checks test

.PHONY: race
race: ; $(info $M Running data race detector)
	@./tools/checks race

.PHONY: msan
msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

setup: ; $(info $M Fetching golang dependencies...)
	@echo Setup steps in Makefile have been deprecated

schema: ; $(info $M Embedding schema files ino binary...)
	@./tools/genschema

build: $(BUILD_DEPS)

static: $(STATIC_DEPS)

.PHONY: image
image: $(IMAGE_DEPS)

.PHONY: container
container: $(CONTAINER_DEPS)

# FIXME: this assumes master branch and os/arch, consider using build output
.PHONY: install
install: $(INSTALL_DEPS)

.PHONY: uninstall
uninstall: $(UNINSTALL_DEPS)

.PHONY: clean
clean: ; $(info $M Removing generated files... )
	@rm -rf target/*

.PHONY: all build print-version coverage coverhtml setup schema static
