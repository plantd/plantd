#!/usr/bin/env bash

set -e

REGISTRY_BASE=registry.gitlab.com/plantd

# debug=0, info=1, warn=2, error=3
LOG_LEVEL_DEBUG=0
LOG_LEVEL_INFO=1
LOG_LEVEL_WARN=2
LOG_LEVEL_ERROR=3
LOG_LEVEL=$LOG_LEVEL_DEBUG
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[1;33m'
NC='\033[0m'

# This doesn't do anything yet other than change the colour
function log {
    if (( $1 == $LOG_LEVEL_DEBUG && $LOG_LEVEL == $LOG_LEVEL_DEBUG )); then
        printf "${BLUE}[DEBUG]${NC} ${2}\n"
    elif (( $1 == $LOG_LEVEL_INFO && $LOG_LEVEL <= $LOG_LEVEL_INFO )); then
        printf "${GREEN}[ INFO]${NC} ${2}\n"
    elif (( $1 == $LOG_LEVEL_WARN && $LOG_LEVEL <= $LOG_LEVEL_WARN )); then
        printf "${YELLOW}[ WARN]${NC} ${2}\n"
    elif (( $1 == $LOG_LEVEL_ERROR )); then
        printf "${RED}[ERROR]${NC} ${2}\n"
    fi
}

function debug {
    log $LOG_LEVEL_DEBUG "${1}"
}

function info {
    log $LOG_LEVEL_INFO "${1}"
}

function warn {
    log $LOG_LEVEL_WARN "${1}"
}

function error {
    log $LOG_LEVEL_ERROR "${1}"
}

# Build a single image.
function __build_image {
    case "$1" in
        dev) docker build -t $REGISTRY_BASE/$2:dev -f $3 . ;;
        v2)  docker build -t $REGISTRY_BASE/$2:v2 -f $3 . ;;
        *)   error "${1} is not a valid choice for 'build-image'"
             exit 1
             ;;
    esac
}

# Build one of the container base images.
function __build_base {
    case "$1" in
        dev) __build_image dev plantd build/Dockerfile.dev ;;
        v2)  __build_image v2 plantd build/Dockerfile ;;
        *)   error "${1} is not a valid choice for 'build-base'"
             exit 1
             ;;
    esac
}

# Build the container that compiles the static binaries
function __build_static {
    debug "Building static binaries..."
    docker build -t $REGISTRY_BASE/plantd:static -f build/Dockerfile.static .
    # TODO: move the build into a different function to make rebuilding easier
    docker build -t $REGISTRY_BASE/plantd:build -f build/Dockerfile.build .
    debug "Copying static binaries..."
    docker run --rm -v /tmp/static:/tmp/static:z \
        -it registry.gitlab.com/plantd/plantd:build \
        cp target/plantd-{broker,configure,events,master,metrics}-static /tmp/static/
    # This is only necessary when running with docker-machine
    if [[ ! -z "${DOCKER_MACHINE_NAME}" ]]; then
        debug "Copy static binaries from the remote host ${DOCKER_MACHINE_NAME}"
        rsync -avhe ssh $DOCKER_MACHINE_NAME:/tmp/static/ target/
    fi
    tar cjf target/plantd-broker-static.tar.xz target/plantd-broker-static
    tar cjf target/plantd-configure-static.tar.xz target/plantd-configure-static
    tar cjf target/plantd-events-static.tar.xz target/plantd-events-static
    tar cjf target/plantd-master-static.tar.xz target/plantd-master-static
    tar cjf target/plantd-metrics-static.tar.xz target/plantd-metrics-static
    rm -f target/*-static
}

# Build images.
#
# When run as './tools/build build v2' the standard images for typical execution are
# built, these require that static binaries to be built and located in ./target
# beforehand.
#
# When run as './tools/build build dev' the development images with hot reloading
# to use during development are built.
#
# TODO: might be able to replace this the individual builds with `make image`
function __build {
    case "$1" in
        dev)
            __build_base dev
            __build_image dev plantd/master build/master/Dockerfile.dev
            __build_image dev plantd/broker build/broker/Dockerfile.dev
            __build_image dev plantd/configure build/configure/Dockerfile.dev
            __build_image dev plantd/events build/events/Dockerfile.dev
            __build_image dev plantd/metrics build/metrics/Dockerfile.dev
            ;;
        v2)
            make clean
            # TODO: create a base container or remove this
            # __build_base
            info "Prepare the development container for building static binaries"
            __build_base dev
            debug "Build static binaries"
            __build_static
            debug "Build containers that execute static binaries"
            __build_image v2 plantd/master build/master/Dockerfile
            __build_image v2 plantd/broker build/broker/Dockerfile
            __build_image v2 plantd/configure build/configure/Dockerfile
            __build_image v2 plantd/events build/events/Dockerfile
            __build_image v2 plantd/metrics build/metrics/Dockerfile
            ;;
        *)
            error "${1} is not a valid choice for 'build'"
            exit 1
            ;;
    esac
}

# Push a single image.
function __push_image {
    case "$1" in
        dev) docker push $REGISTRY_BASE/$2:dev ;;
        v2)  docker push $REGISTRY_BASE/$2:v2 ;;
        *)   error "${1} is not a valid choice for 'push-image'"
             exit 1
             ;;
    esac
}

# Push images.
#
# When run as './tools/build push v2' the standard images for typical execution
# are pushed.
#
# When run as './tools/build push dev' the development images with hot reloading
# to use during development are pushed.
function __push {
    case "$1" in
        dev)
            __push_image dev plantd/master
            __push_image dev plantd/broker
            __push_image dev plantd/configure
            __push_image dev plantd/events
            __push_image dev plantd/metrics
            ;;
        v2)
            __push_image v2 plantd/master
            __push_image v2 plantd/broker
            __push_image v2 plantd/configure
            __push_image v2 plantd/events
            __push_image v2 plantd/metrics
            ;;
        *)
            error "${1} is not a valid choice for 'push'"
            exit 1
            ;;
    esac
}

usage_args="[build [dev|v2] | push [dev|v2]]"
usage="$(basename "$0") $usage_args -- manage building pushing container images

where:
    build - use docker to build :v2 or :dev images
    push  - use docker to push :v2 or :dev images to the gitlab registry
"

function __help {
    printf "${usage}\n"
}

case "$1" in
    build)       __build $2 ;;
    push)        __push $2 ;;
    # helper, unsupported
    static)      __build_static ;;
    # image specific use is unsupported, just for development needs
    build-image) __build_image $3 plantd/$2 build/$2/Dockerfile ;;
    push-image)  __push_image $3 plantd/$2 ;;
    *)           __help "$@"
                 exit 1
                 ;;
esac
