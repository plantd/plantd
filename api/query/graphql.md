# Queries and Mutations

Eventually these should be moved in with the web application that will load the
queries and mutations by the components that use them. There is a `webpack`
loader that can be used to load these files to avoid using template strings.
