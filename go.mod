module gitlab.com/plantd/plantd

go 1.13

require (
	github.com/99designs/gqlgen v0.12.2
	github.com/apache/thrift v0.12.0 // indirect
	github.com/cmelgarejo/go-gql-server v0.0.0-20200628225422-cfb27ae0504a
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/contrib v0.0.0-20200913005814-1c32036e7ea4
	github.com/gin-gonic/gin v1.6.3
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2
	github.com/goware/prefixer v0.0.0-20160118172347-395022866408 // indirect
	github.com/graph-gophers/dataloader v5.0.0+incompatible
	github.com/graph-gophers/dataloader/v6 v6.0.0
	github.com/graph-gophers/graphql-go v0.0.0-20200819123640-3b5ddcd884ae
	github.com/hashicorp/golang-lru v0.5.4
	github.com/jcmturner/gokrb5/v8 v8.3.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/klauspost/compress v1.10.7 // indirect
	github.com/lib/pq v1.6.0 // indirect
	github.com/markbates/goth v1.64.2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/openzipkin/zipkin-go v0.1.6 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pebbe/zmq4 v1.2.1 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/pressly/sup v0.5.3 // indirect
	github.com/rs/xid v1.2.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	github.com/vektah/gqlparser/v2 v2.0.1
	github.com/xakep666/mongo-migrate v0.2.1
	gitlab.com/plantd/go-plantd v0.1.22
	gitlab.com/plantd/go-zapi v0.0.0-20191214044601-930fec727b30
	gitlab.com/plantd/plantctl v0.2.4
	go.mongodb.org/mongo-driver v1.4.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980 // indirect
	golang.org/x/tools v0.0.0-20200612220849-54c614fe050c // indirect
	google.golang.org/genproto v0.0.0-20200604104852-0b0486081ffb // indirect
	google.golang.org/grpc v1.32.0
	gopkg.in/gormigrate.v1 v1.6.0
	gopkg.in/ini.v1 v1.57.0 // indirect
)
