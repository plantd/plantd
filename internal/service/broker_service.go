package service

import (
	"errors"
	"fmt"
	"strings"
	"time"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/util"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// BrokerService is used to communicate with network message brokers
type BrokerService struct {
	clients          map[string]pb.EndpointClient
	clientsConnected map[string]bool
	//mdpClients map[string]*api.Client
}

// NewBrokerService returns a new BrokerService instance
func NewBrokerService(config *gcontext.MasterConfig) *BrokerService {
	clients := make(map[string]pb.EndpointClient)
	clientsConnected := make(map[string]bool)
	//var mdpClients map[string]*api.Client = make(map[string]*api.Client)

	for name, client := range config.Broker {
		var err error

		// Connect gRPC clients
		clientsConnected[name] = true
		clients[name], err = gcontext.BrokerClient(client.Host, client.Port)
		if err != nil {
			clientsConnected[name] = false
			logger.WithFields(logrus.Fields{
				"host":    client.Host,
				"port":    client.Port,
				"service": name,
				"err":     err,
			}).Error("Unable to connect to broker service")
		}

		// Connect ZeroMQ clients
		/*
		 *mdpClients[name], err = gcontext.ConnectClient(config)
		 *if err != nil {
		 *    logger.Fatalf("Unable to connect to broker service as MDP client: %s\n", err)
		 *}
		 */
	}

	return &BrokerService{
		clients,
		clientsConnected,
		//mdpClients,
	}
}

func (b *BrokerService) checkService(serviceName string) error {
	if _, ok := b.clientsConnected[serviceName]; !ok {
		logger.Debugf("Checking if %s is configured", serviceName)
		err := errors.New("Target service not configured")
		logger.WithFields(logrus.Fields{"service": serviceName}).Error(err)
		return err
	}

	if !b.clientsConnected[serviceName] {
		logger.Debugf("Checking if %s is connected", serviceName)
		err := errors.New("Target service not connected")
		logger.WithFields(logrus.Fields{"service": serviceName}).Error(err)
		return err
	}

	return nil
}

// Module returns module information retrieved for the name provided
func (b *BrokerService) Module(ctx context.Context, moduleName string) (*model.Module, error) {
	if err := b.checkService("state"); err != nil {
		return nil, err
	}

	var req []*pb.Property

	// FIXME: this is gross and only specific to a single implementation
	// FIXME: consider putting a property list as configuration
	req = append(req, &pb.Property{Key: "image1-uri"})
	req = append(req, &pb.Property{Key: "image2-uri"})
	req = append(req, &pb.Property{Key: "image3-uri"})
	req = append(req, &pb.Property{Key: "image4-uri"})
	req = append(req, &pb.Property{Key: "image1-name"})
	req = append(req, &pb.Property{Key: "image2-name"})
	req = append(req, &pb.Property{Key: "image3-name"})
	req = append(req, &pb.Property{Key: "image4-name"})
	req = append(req, &pb.Property{Key: "system-status"})
	req = append(req, &pb.Property{Key: "run-mode"})
	req = append(req, &pb.Property{Key: "duration-hour"})
	req = append(req, &pb.Property{Key: "duration-min"})
	req = append(req, &pb.Property{Key: "duration-sec"})
	req = append(req, &pb.Property{Key: "interval-min"})
	req = append(req, &pb.Property{Key: "interval-sec"})
	req = append(req, &pb.Property{Key: "g-force"})
	req = append(req, &pb.Property{Key: "start-time"})
	req = append(req, &pb.Property{Key: "time-elapsed"})
	req = append(req, &pb.Property{Key: "est-duration"})
	req = append(req, &pb.Property{Key: "recipe-id"})
	req = append(req, &pb.Property{Key: "recipe-name"})
	req = append(req, &pb.Property{Key: "recipe-raw"})
	req = append(req, &pb.Property{Key: "state-core"})
	req = append(req, &pb.Property{Key: "state-cam"})
	req = append(req, &pb.Property{Key: "state-plc"})
	req = append(req, &pb.Property{Key: "state-acq"})
	req = append(req, &pb.Property{Key: "state-ana"})

	properties, err := b.GetModuleProperties("state", moduleName, req)
	if err != nil {
		return &model.Module{}, err
	}

	return &model.Module{
		Properties: properties,
	}, nil
}

//
// Plant unit service calls
//

// GetModule : Sends a GRPC request to the broker for all the properties of the Module
func (b *BrokerService) GetModule(serviceName string, moduleID string) (*model.Module, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending grpc request for GetModule with %s, %s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	currentModule := &model.Module{}
	request := &pb.ModuleRequest{Id: moduleID}
	{
		logger.Debugf("Sending grpc request for GetModule with %s, %s", serviceName, moduleID)
		response, err := b.clients[serviceName].GetModule(ctx, request)

		if err != nil {
			// Send a dead module model back
			deadModule := &model.Module{
				ModuleName:  moduleID,
				ServiceName: serviceName,
				State:       &model.AllModuleState[1],
			}
			logger.Errorf("Broker service GetModule error: %s\n", err)
			return deadModule, err
		}

		currentModule.ModuleName = response.Module.Id
		currentModule.ServiceName = response.Module.ServiceName
	}

	{
		status := false
		logger.Debugf("Sending grpc request for GetModuleStatus with %s, %s", serviceName, moduleID)
		response, err := b.clients[serviceName].GetModuleStatus(ctx, request)

		if err != nil {
			currentModule.Status = &model.Status{
				Enabled: &status,
				Loaded:  &status,
				Active:  &status,
			}
			currentModule.State = &model.AllModuleState[2]
			logger.Errorf("Broker service GetModuleStatus error: %s\n", err)
			return currentModule, err
		}

		var details []*model.Property
		for key, value := range response.Status.Details {
			details = append(details, &model.Property{Key: &key, Value: &value})
		}

		currentModule.Status = &model.Status{
			Enabled: &response.Status.Enabled,
			Loaded:  &response.Status.Loaded,
			Active:  &response.Status.Active,
			Details: details,
		}

		// TODO: get the actual state
		currentModule.State = &model.AllModuleState[3]
	}

	// TODO: get properties from GetModuleValues
	currentModule.Properties = nil
	{
		logger.Debugf("Sending grpc request for GetModuleConfiguration with %s, %s", serviceName, moduleID)
		response, err := b.clients[serviceName].GetModuleConfiguration(ctx, request)
		if err != nil {
			logger.Errorf("Broker service GetModuleConfiguration error: %s\n", err)
		} else {
			if err := currentModule.Configuration.FromProtobuf(response.Configuration); err != nil {
				return nil, err
			}
		}
	}

	// TODO: Jobs
	return currentModule, nil
}

// GetModules : Sends a gRPC request for the information about a module
// rpc GetModules(Empty) return (ModulesResponse);
func (b *BrokerService) GetModules(serviceName string) (*[]*model.Module, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	// TODO: Use pagination data with grpc
	logger.Debugf("Sending grpc request for GetModules")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.Empty{}
	response, err := b.clients[serviceName].GetModules(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not get modules for %s: %s", serviceName, err)
	}

	// TODO: Need to test
	modules := make([]*model.Module, len(response.Modules))
	for index, Module := range response.Modules {
		modules[index] = &model.Module{
			ModuleName:  Module.Name,
			ServiceName: Module.ServiceName,
		}
	}

	return &modules, err
}

//
// Plant module calls that the unit brokers
//

// GetModuleConfiguration : Sends a gRPC request for a module configuration
// rpc GetModuleConfiguration(ModuleRequest) returns (ConfigurationResponse);
func (b *BrokerService) GetModuleConfiguration(serviceName string, moduleID string) (*model.Configuration, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending grpc request for GetModuleConfiguration with %s, %s", serviceName, moduleID)
	configuration := &model.Configuration{}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	request := &pb.ModuleRequest{
		Id: moduleID,
	}
	response, err := b.clients[serviceName].GetModuleConfiguration(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not read module configuration for %s: %s", moduleID, err)
	}

	logger.Debugf("gRPC conf: %s", response.Configuration.Id)

	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		return nil, err
	}

	return configuration, nil
}

// GetModuleStatus : Sends a gRPC request to the broker and expects a response that matches the Status model
// rpc GetModuleStatus(ModuleRequest) returns (StatusResponse);
func (b *BrokerService) GetModuleStatus(serviceName string, moduleID string) (*model.Status, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending grpc request for GetModuleStatus with %s, %s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleRequest{Id: moduleID}
	response, err := b.clients[serviceName].GetModuleStatus(ctx, request)
	if err != nil {
		status := false
		// If there is an error, we should send a dead status model back
		deadStatus := &model.Status{
			Enabled: &status,
			Loaded:  &status,
			Active:  &status,
			Details: nil,
		}
		logger.Errorf("Broker service GetModuleStatus error: %s", err)
		return deadStatus, err
	}

	var details []*model.Property
	for key, value := range response.Status.Details {
		details = append(details, &model.Property{Key: &key, Value: &value})
	}

	// When there are no issues, map the response to the Status model and return it
	currentStatus := &model.Status{
		Enabled: &response.Status.Enabled,
		Loaded:  &response.Status.Loaded,
		Active:  &response.Status.Active,
		Details: details,
	}

	return currentStatus, nil
}

// GetModuleSettings : Sends a gRPC request for the settings of a module
// rpc GetModuleSettings(ModuleRequest) returns (SettingsResponse);
func (b *BrokerService) GetModuleSettings(serviceName string, moduleID string) (*[]*model.Property, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for GetModuleSettings to %s/%s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleRequest{Id: moduleID}
	response, err := b.clients[serviceName].GetModuleSettings(ctx, request)
	if err != nil {
		return nil, err
	}

	// TODO: Need to test
	moduleSettings := make([]*model.Property, len(response.Settings))
	i := 0
	for key, value := range response.Settings {
		moduleSettings[i] = &model.Property{
			Key:   &key,
			Value: &value,
		}
		i++
	}

	return &moduleSettings, err
}

// GetModuleJob : Sends a gRPC request for a module job
// rpc GetModuleJob(ModuleRequest) returns (JobResponse);
func (b *BrokerService) GetModuleJob(serviceName string, moduleID string, jobID string) (*model.Job, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for GetModuleJob to %s/%s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleJobRequest{Id: moduleID, JobId: jobID}
	response, err := b.clients[serviceName].GetModuleJob(ctx, request)
	if err != nil {
		return nil, err
	}

	status := strings.ToUpper(response.Job.Status.String())
	job := &model.Job{
		ID:     response.Job.Id,
		Status: &model.AllJobStatus[util.IndexOf(status, util.JobStatusToString(model.AllJobStatus))],
	}

	return job, nil
}

// GetModuleJobs : Sends a gRPC request for a list of module jobs
// rpc GetModuleJobs(ModuleRequest) returns (JobsResponse);
func (b *BrokerService) GetModuleJobs(serviceName string, moduleID string) (*[]*model.Job, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for GetModuleJobs to %s/%s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleRequest{Id: moduleID}
	response, err := b.clients[serviceName].GetModuleJobs(ctx, request)
	if err != nil {
		return nil, err
	}

	// TODO: Need to test
	moduleJobs := make([]*model.Job, len(response.Jobs))
	for index, job := range response.Jobs {
		status := strings.ToUpper(job.Status.String())
		moduleJobs[index] = &model.Job{
			ID:     job.Id,
			Status: &model.AllJobStatus[util.IndexOf(status, util.JobStatusToString(model.AllJobStatus))],
		}
	}

	return &moduleJobs, err
}

// GetModuleActiveJob : Sends a gRPC request for the active module job
// rpc GetModuleActiveJob(ModuleRequest) returns (JobResponse);
func (b *BrokerService) GetModuleActiveJob(serviceName string, moduleID string) (*model.Job, error) {
	logger.Debugf("Sending gRPC request for GetModuleActiveJob to %s/%s", serviceName, moduleID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleRequest{Id: moduleID}
	response, err := b.clients[serviceName].GetModuleActiveJob(ctx, request)
	if err != nil {
		return nil, err
	}

	status := strings.ToUpper(response.Job.Status.String())
	job := &model.Job{
		ID:     response.Job.Id,
		Status: &model.AllJobStatus[util.IndexOf(status, util.JobStatusToString(model.AllJobStatus))],
	}

	return job, nil
}

// ModuleCancelJob : Sends a gRPC request to cancel a module job
// rpc ModuleCancelJob(ModuleJobRequest) returns (JobResponse);
func (b *BrokerService) ModuleCancelJob(serviceName string, moduleID string, jobID string) (*model.Job, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for ModuleCancelJob to %s/%s for job %s", serviceName, moduleID, jobID)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the request
	request := &pb.ModuleJobRequest{
		Id:    moduleID,
		JobId: jobID,
	}
	response, err := b.clients[serviceName].ModuleCancelJob(ctx, request)
	if err != nil {
		return nil, err
	}

	job := &model.Job{
		ID:     response.Job.Id,
		Status: &model.AllJobStatus[6],
	}

	return job, nil
}

// ModuleSubmitJob : Sends a gRPC request to submit a job to a module
// rpc ModuleSubmitJob(ModuleJobRequest) returns (JobResponse);
func (b *BrokerService) ModuleSubmitJob(
	serviceName string,
	moduleID string,
	action string,
	value string,
	properties []*pb.Property,
) (*model.Job, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending grpc request for ModuleSubmitJob to %s/%s:%s", serviceName, moduleID, action)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	request := &pb.ModuleJobRequest{
		Id:            moduleID,
		JobId:         action,
		JobValue:      value,
		JobProperties: properties,
	}

	response, err := b.clients[serviceName].ModuleSubmitJob(ctx, request)
	if err != nil {
		return nil, err
	}

	job := &model.Job{
		ID:     response.Job.Id,
		Status: &model.AllJobStatus[1],
	}

	return job, nil
}

// ModuleSubmitEvent : Sends a gRPC request to raise an event in a module
// rpc ModuleSubmitEvent(ModuleEventRequest) returns (JobResponse);
func (b *BrokerService) ModuleSubmitEvent(serviceName string, moduleID string) (*model.Job, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for ModuleSubmitEvent to %s/%s", serviceName, moduleID)

	err := errors.New("RPC not implemented")

	return nil, err
}

// ModuleAvailableEvents : Sends a gRPC request to return a list of available events
// rpc ModuleAvailableEvents(ModuleRequest) returns (EventsResponse);
func (b *BrokerService) ModuleAvailableEvents(serviceName string, moduleID string) (*[]*model.Event, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for ModuleAvailableEvents to %s/%s", serviceName, moduleID)

	err := errors.New("RPC not implemented")

	return nil, err
}

// GetModuleProperty : Sends a gRPC request to get a property from a module
// rpc GetModuleProperty(PropertyRequest) returns (PropertyResponse);
func (b *BrokerService) GetModuleProperty(
	serviceName string,
	moduleID string,
	propertyName string,
) (*model.Property, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for GetModuleProperty to %s/%s with property %s",
		serviceName, moduleID, propertyName)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the property request
	request := &pb.PropertyRequest{
		Id:  moduleID,
		Key: propertyName,
	}
	// Send the request to the specified service
	response, err := b.clients[serviceName].GetModuleProperty(ctx, request)
	if err != nil {
		return nil, err
	}
	// Convert the protobuf property into a model property
	key := response.GetProperty().GetKey()
	value := response.GetProperty().GetValue()
	_property := &model.Property{
		Key:   &key,
		Value: &value,
	}

	return _property, nil
}

// GetModuleProperties : Sends a gRPC request to get a list of properties from a module
// rpc GetModuleProperties(PropertyRequest) returns (PropertiesResponse);
func (b *BrokerService) GetModuleProperties(
	serviceName string,
	moduleID string,
	properties []*pb.Property,
) ([]*model.Property, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for GetModuleProperties to %s/%s for keys %s",
		serviceName, moduleID, properties)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the properties request
	request := &pb.PropertiesRequest{
		Id:         moduleID,
		Properties: properties,
	}
	// Send the request to the specified service
	response, err := b.clients[serviceName].GetModuleProperties(ctx, request)
	if err != nil {
		return nil, err
	}
	//

	/*
	 *    request := make([]string, 2)
	 *    request[0] = "get-module-properties"
	 *
	 *    // Create the properties request
	 *    in := &pb.PropertiesRequest{
	 *        Id:         moduleID,
	 *        Properties: properties,
	 *    }
	 *
	 *    response := &pb.PropertiesResponse{}
	 *
	 *    // Serialize message body to send
	 *    data, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	 *    if err := marshaler.Marshal(&data, in); err != nil {
	 *        //logger.Error(e.logger, "msg", "jsonpb marshal", "data", err)
	 *        return nil, err
	 *    }
	 *
	 *    // Send the message
	 *    request[1] = data.String()
	 *    _ = b.mdpClients[serviceName].Send(serviceName, request...)
	 *    // Wait for a reply
	 *    reply, err := b.mdpClients[serviceName].Recv()
	 *    if err != nil {
	 *        //logger.Error(e.logger, "msg", request[0], "data", err)
	 *        return nil, err
	 *    }
	 *
	 *    if len(reply) == 0 {
	 *        er := errors.New("didn't receive expected response")
	 *        //logger.Error(e.logger, "msg", request[0], "data", er)
	 *        return nil, er
	 *    } else if len(reply) > 1 {
	 *        //logger.Warn(e.logger, "msg", request[0], "data", "multiple replies received, only first was handled")
	 *        //logger.Error("multiple replies received")
	 *    }
	 *
	 *    // Deserialize reply into a response
	 *    if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
	 *        //logger.Error(e.logger, "msg", request[0], "data", err)
	 *        return nil, err
	 *    }
	 */

	//

	// Convert the pb PropertiesResponse into an array of type Property
	var convertedProperties []*model.Property
	responseProperties := response.GetProperties()
	for _, property := range responseProperties {
		logger.Debug(*property)
		key := property.GetKey()
		value := property.GetValue()
		tempProperty := &model.Property{
			Key:   &key,
			Value: &value,
		}
		convertedProperties = append(convertedProperties, tempProperty)
	}

	return convertedProperties, err
}

// SetModuleProperty : Sends a gRPC request to set a property in a module
// rpc SetModuleProperty(PropertyRequest) returns (PropertyResponse);
func (b *BrokerService) SetModuleProperty(
	serviceName string,
	moduleID string,
	property *pb.Property,
) (*model.Property, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for SetModuleProperty to %s/%s for %s:%s",
		serviceName, moduleID, property.GetKey(), property.GetValue())
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the property request
	request := &pb.PropertyRequest{
		Id:    moduleID,
		Key:   property.GetKey(),
		Value: property.GetValue(),
	}
	// Send the request to the specified service
	response, err := b.clients[serviceName].SetModuleProperty(ctx, request)
	if err != nil {
		return nil, err
	}
	// Convert the protobuf property into a model property
	key := response.GetProperty().GetKey()
	value := response.GetProperty().GetValue()
	_property := &model.Property{
		Key:   &key,
		Value: &value,
	}

	return _property, nil
}

// SetModuleProperties : Sends a gRPC request to set a list of properties in a module
// rpc SetModuleProperties(PropertiesRequest) returns (PropertiesResponse);
func (b *BrokerService) SetModuleProperties(
	serviceName string,
	moduleID string,
	properties []*pb.Property,
) ([]*model.Property, error) {
	if err := b.checkService(serviceName); err != nil {
		return nil, err
	}

	logger.Debugf("Sending gRPC request for SetModuleProperties to %s/%s for %s",
		serviceName, moduleID, properties)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the properties request
	request := &pb.PropertiesRequest{
		Id:         moduleID,
		Properties: properties,
	}
	// Send the request to the specified service
	response, err := b.clients[serviceName].SetModuleProperties(ctx, request)
	if err != nil {
		return nil, err
	}
	// Convert the protobuf properties into an array of model property
	var convertedProperties []*model.Property
	responseProperties := response.GetProperties()
	for _, property := range responseProperties {
		logger.Debug(*property)
		key := property.GetKey()
		value := property.GetValue()
		tempProperty := &model.Property{
			Key:   &key,
			Value: &value,
		}
		convertedProperties = append(convertedProperties, tempProperty)
	}

	return convertedProperties, nil
}
