package service

import (
	"fmt"
	"time"

	pb "gitlab.com/plantd/plantctl/proto"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"

	"golang.org/x/net/context"
)

// ManagerService is an interface to the plantctl gRPC service
type ManagerService struct {
	client pb.ManagerClient
}

// NewManagerService returns a new ManagerService instances
func NewManagerService(client pb.ManagerClient) *ManagerService {
	return &ManagerService{client}
}

// Start configured plantd services
func (c *ManagerService) Start() (*model.Response, error) {
	logger.Debugf("Sending grpc Start request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Start(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Start to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Stop configured plantd services
func (c *ManagerService) Stop() (*model.Response, error) {
	logger.Debugf("Sending grpc Stop request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Stop(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Stop to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Restart configured plantd services
func (c *ManagerService) Restart() (*model.Response, error) {
	logger.Debugf("Sending grpc restart request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Restart(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Restart to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Status checks configured plantd services
// rpc Status (Empty) returns (ServiceResponse) {}
func (c *ManagerService) Status() (*[]*model.Property, error) {
	logger.Debugf("Sending grpc Status request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Send the status command via GRPC
	rawResponse, err := c.client.Status(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Status to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	// Check if the response was successful
	if rawResponse.GetError() != nil {
		return nil, fmt.Errorf("Error response: %s", rawResponse.GetError().String())
	}

	var properties []*model.Property
	for _, service := range rawResponse.GetServices() {
		key := service.GetTarget()
		value := service.GetState().String()
		properties = append(properties, &model.Property{
			Key:   &key,
			Value: &value,
		})
	}

	return &properties, nil
}

// TODO:
// Stream log messages for configured plantd services
// rpc Watch (Empty) returns (stream LogEntry) {}

// TODO:
// Receive updates when configured services change
// rpc Monitor (Empty) returns (stream Service) {}

// Install a plantd system
func (c *ManagerService) Install() (*model.Response, error) {
	logger.Debugf("Sending grpc Install request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Install(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Install to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Uninstall a plantd system
func (c *ManagerService) Uninstall() (*model.Response, error) {
	logger.Debugf("Sending grpc uninstall request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Uninstall(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Uninstall to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}

// Upgrade a plantd system
func (c *ManagerService) Upgrade() (*model.Response, error) {
	logger.Debugf("Sending grpc Upgrade request to Manager")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	rawResponse, err := c.client.Upgrade(ctx, &pb.Empty{})
	if err != nil {
		return nil, fmt.Errorf("could not send Upgrade to Manager: %s", err)
	}
	logger.Debugf("gRPC response: %d", rawResponse.Code)

	response := &model.Response{
		Code:  int(rawResponse.Code),
		Error: rawResponse.Error.GetMessage(),
	}

	return response, nil
}
