package service

import (
	dbm "gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
	"github.com/rs/xid"
)

// NetworkService is used to work with network records in the database
type NetworkService struct {
	db *gorm.DB
}

// NewNetworkService returns a new NetworkService instance
func NewNetworkService(db *gorm.DB) *NetworkService {
	return &NetworkService{db: db}
}

// FindByUserID returns all networks associated with a user ID
func (n *NetworkService) FindByUserID(userID *string) ([]*dbm.Network, error) {
	networks := make([]*dbm.Network, 0)

	n.db.Table("networks").Select("networks.*").Joins(
		"inner join users on network.id = users.network_id",
	).Where(
		"networks.user_id = ?", userID,
	).Find(&networks)

	// TODO: remove error or do something to create one
	return networks, nil
}

// FindByName returns a network record with the name provided
func (n *NetworkService) FindByName(name string) (*dbm.Network, error) {
	network := &dbm.Network{}
	n.db.Where("name = ?", name).First(&network)
	return network, nil
}

// FindByDomain returns a network record with the domain provided
func (n *NetworkService) FindByDomain(domain string) (*dbm.Network, error) {
	network := &dbm.Network{}
	n.db.Where("domain = ?", domain).First(&network)
	return network, nil
}

// CreateNetwork inserts a new network record into the database
func (n *NetworkService) CreateNetwork(network *dbm.Network) (*dbm.Network, error) {
	networkID := xid.New()
	id, err := uuid.FromString(networkID.String())
	if err != nil {
		return nil, err
	}
	network.ID = id
	n.db.NewRecord(network)
	n.db.Create(&network)
	return network, nil
}

// List returns all network records that exist in the database
func (n *NetworkService) List(first *int32, after *string) ([]*dbm.Network, error) {
	networks := make([]*dbm.Network, 0)
	var fetchSize int32
	if first == nil {
		fetchSize = defaultListFetchSize
	} else {
		fetchSize = *first
	}

	if after != nil {
		decodedIndex, _ := DecodeCursor(after)
		n.db.Order("created_at desc").Limit(fetchSize).Where(
			"created_at < ?", n.db.Table("networks").Select("created_at").Where("id = ?", decodedIndex).QueryExpr(),
		).Find(&networks)
		return networks, nil
	}

	n.db.Order("created_at desc").Limit(fetchSize).Find(&networks)
	return networks, nil
}

// Update a network record if it exists in the database
func (n *NetworkService) Update(network *dbm.Network) *dbm.Network {
	n.db.Save(&network)
	return network
}

// Delete a network record if it exists in the database
func (n *NetworkService) Delete(network *dbm.Network) *dbm.Network {
	n.db.Delete(&network)
	return network
}

// Count returns the number of network records that exist in the database
func (n *NetworkService) Count() (int, error) {
	var count int
	n.db.Table("networks").Count(&count)
	return count, nil
}
