package role

import (
	"errors"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	dbm "gitlab.com/plantd/plantd/internal/orm/model"
	"gitlab.com/plantd/plantd/internal/resolver"
	tf "gitlab.com/plantd/plantd/internal/resolver/transformation"
)

// TODO: use these functions in data loaders

var role *roleService

func init() {
	role = NewRoleService()
}

type roleService struct {
}

func NewRoleService() *roleService {
	return &roleService{}
}

func CreateUpdate(
	r *resolver.MutationResolver,
	input model.RoleInput,
	update bool,
	ids ...string,
) (*model.Role, error) {
	dbo, err := tf.GQLInputRoleToDBRole(&input, update, ids...)
	if err != nil {
		return nil, err
	}

	// Create scoped clean db interface
	db := r.ORM.DB.New().Begin()
	if !update {
		db = db.Create(dbo).First(dbo) // Create the role
		if db.Error != nil {
			return nil, db.Error
		}
	} else {
		db = db.Model(&dbo).Update(dbo).First(dbo) // Or update it
	}

	gql, err := tf.DBRoleToGQLRole(dbo)
	if err != nil {
		db.Rollback()
		return nil, err
	}

	db = db.Commit()

	return gql, db.Error
}

func Delete(r *resolver.MutationResolver, id string) (bool, error) {
	return false, errors.New("not implemented")
}

func List(r *resolver.QueryResolver, id *string) (*model.RolesConnection, error) {
	entity := "roles"
	whereID := "id = ?"
	record := &model.RolesConnection{}
	var dbRecords []*dbm.Role
	db := r.ORM.DB.New()
	if id != nil {
		db = db.Where(whereID, *id)
	}

	db = db.Find(&dbRecords).Count(&record.TotalCount)
	for _, dbRec := range dbRecords {
		if rec, err := tf.DBRoleToGQLRole(dbRec); err != nil {
			logger.Errorfn(entity, err)
		} else {
			node := &model.RolesEdge{Node: rec}
			record.Edges = append(record.Edges, node)
		}
	}
	return record, db.Error
}
