package experiment

import (
	"errors"

	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/resolver"
)

var experiment *experimentService

func init() {
	experiment = NewExperimentService()
}

type experimentService struct {
}

func NewExperimentService() *experimentService {
	return &experimentService{}
}

func CreateUpdate(
	r *resolver.MutationResolver,
	input model.ExperimentInput,
	update bool,
	ids ...string,
) (*model.Experiment, error) {
	return nil, errors.New("not implemented yet")
}

func Delete(r *resolver.MutationResolver, id string) (bool, error) {
	return false, errors.New("not implemented yet")
}

func List(r *resolver.QueryResolver, id *string) (*model.ExperimentsConnection, error) {
	return nil, errors.New("not implemented yet")
}
