package configure

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"

	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

var configure *configureService

func init() {
	configure = NewConfigureService()
}

type configureService struct {
	client pb.ConfigureEndpointClient
}

func NewConfigureService() *configureService {
	return &configureService{}
}

func Setup(config *gcontext.MasterConfig) {
	// Create a configuration service connection
	client, err := gcontext.ConfigureClient(config)
	if err != nil {
		logger.Fatalf("Unable to connect to configuration service: %s\n", err)
	}
	configure.client = client
}

// FindByID returns a configuration for the given ID
func FindByID(confID string, confNamespace string) (*model.Configuration, error) {
	configuration := &model.Configuration{}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Id:        confID,
		Namespace: pb.Configuration_Namespace(namespace),
	}
	response, err := configure.client.Read(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not read configuration %s: %s", confID, err)
	}

	logger.Debugf("gRPC conf: %s", response.Configuration.Id)

	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		return nil, err
	}

	logger.Debugf("%v", configuration)

	return configuration, nil
}

// List returns all configurations that can be read from the configure service
func List(confNamespace string, first *int, after *string) ([]*model.Configuration, error) {
	// TODO: Until go-plantd supports pagination for configurations, just load them all
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Namespace: pb.Configuration_Namespace(namespace),
	}

	response, err := configure.client.List(ctx, request)
	if err != nil {
		return nil, fmt.Errorf("could not read configurations: %s", err)
	}

	configurations := make([]*model.Configuration, len(response.Configurations))

	for i, element := range response.Configurations {
		configuration := &model.Configuration{}
		logger.Debugf("Found and checking: %v", element)
		err = configuration.FromProtobuf(response.Configurations[i])
		if err != nil {
			return nil, err
		}
		configurations[i] = configuration
	}

	return configurations, nil
}

// Count returns the number of configurations that can be read from the configure service
func Count(confNamespace string) (int, error) {
	// XXX: There might be a better way to do this. plantd-configure does not have a method to just get count.
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(confNamespace)]
	if !ok {
		return 0, errors.New("invalid namespace provided")
	}

	request := &pb.ConfigurationRequest{
		Namespace: pb.Configuration_Namespace(namespace),
	}

	response, err := configure.client.List(ctx, request)
	if err != nil {
		return 0, fmt.Errorf("could not read configurations: %s", err)
	}

	return len(response.Configurations), nil
}

// CreateConfiguration requests that the configure service adds the configuration provided
func CreateConfiguration(conf *pb.Configuration) (*model.Configuration, error) {
	logger.Printf("Sending grpc request for CreateConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Create the GRPC request
	request := &pb.ConfigurationRequest{
		Id:            conf.Id,
		Namespace:     conf.Namespace,
		Configuration: conf,
	}

	// Send the request
	response, err := configure.client.Create(ctx, request)
	if err != nil {
		logger.Error("GRPC send issue")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		logger.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}

// UpdateConfiguration requests that the configure service updates the configuration provided
func UpdateConfiguration(
	id string,
	namespace string,
	conf *pb.Configuration,
) (*model.Configuration, error) {
	logger.Printf("Sending grpc request for UpdateConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Check if the namespace is valid
	_namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Create the gRPC request
	request := &pb.ConfigurationRequest{
		Id:            id,
		Namespace:     pb.Configuration_Namespace(_namespace),
		Configuration: conf,
	}

	// Send the request
	response, err := configure.client.Update(ctx, request)
	if err != nil {
		logger.Error("GRPC send issue for update")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		logger.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}

// DeleteConfiguration requests that the configure service deletes the configuration for the provided ID
func DeleteConfiguration(id string, namespace string) (*model.Configuration, error) {
	logger.Printf("Sending grpc request for DeleteConfiguration")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Check if the namespace is valid
	_namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Create the grpc request
	request := &pb.ConfigurationRequest{
		Id:        id,
		Namespace: pb.Configuration_Namespace(_namespace),
	}

	// Send the request
	response, err := configure.client.Delete(ctx, request)
	logger.Print(err)
	if err != nil {
		logger.Print("error at response")
		return nil, err
	}

	// Convert from the protobuf spec to plantd-master configuration model
	configuration := &model.Configuration{}
	err = configuration.FromProtobuf(response.Configuration)
	if err != nil {
		logger.Print("error at protobuf conversion")
		return nil, err
	}

	return configuration, err
}
