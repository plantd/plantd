package service

import (
	"gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

// RoleService is used to work with role records in the database
type RoleService struct {
	db *gorm.DB
}

// NewRoleService returns a new RoleService instance
func NewRoleService(db *gorm.DB) *RoleService {
	return &RoleService{db: db}
}

// FindByUserID returns all roles associated with a user ID
func (r *RoleService) FindByUserID(userID *string) ([]*model.Role, error) {
	roles := make([]*model.Role, 0)

	r.db.Table("roles").Select("roles.*").Joins(
		"inner join users on role.id = users.role_id",
	).Where(
		"roles.user_id = ?", userID,
	).Find(&roles)

	// TODO: remove error or do something to create one
	return roles, nil
}

// FindByName returns a role record with the name provided
func (r *RoleService) FindByName(name string) (*model.Role, error) {
	role := &model.Role{}
	r.db.Where("name = ?", name).First(&role)
	return role, nil
}

// CreateRole inserts a new role record into the database
func (r *RoleService) CreateRole(role *model.Role) (*model.Role, error) {
	roleID, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	role.ID = roleID
	r.db.NewRecord(role)
	r.db.Create(&role)
	return role, nil
}

// List returns all role records that exist in the database
func (r *RoleService) List(first *int, after *string) ([]*model.Role, error) {
	roles := make([]*model.Role, 0)
	var fetchSize int
	if first == nil {
		fetchSize = defaultListFetchSize
	} else {
		fetchSize = *first
	}

	if after != nil {
		decodedIndex, _ := DecodeCursor(after)
		r.db.Order("created_at desc").Limit(fetchSize).Where(
			"created_at < ?", r.db.Table("roles").Select("created_at").Where("id = ?", decodedIndex).QueryExpr(),
		).Find(&roles)
		return roles, nil
	}

	r.db.Order("created_at desc").Limit(fetchSize).Find(&roles)
	return roles, nil
}

// Update a role record if it exists in the database
func (r *RoleService) Update(role *model.Role) *model.Role {
	r.db.Model(&role).Update(&role)
	return role
}

// Delete a role record if it exists in the database
func (r *RoleService) Delete(role *model.Role) *model.Role {
	r.db.Delete(&role)
	return role
}

// Count returns the number of role records that exist in the database
func (r *RoleService) Count() (int, error) {
	var count int
	r.db.Table("roles").Count(&count)
	return count, nil
}
