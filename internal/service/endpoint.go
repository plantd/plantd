package service

import (
	"bytes"
	"errors"
	"sync"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"gitlab.com/plantd/go-zapi/mdp"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

type Endpoint struct {
	name   string
	client *mdp.Client
	mutex  *sync.Mutex
}

func NewEndpoint(name string, client *mdp.Client, config *gcontext.BrokerConfig) *Endpoint {
	return &Endpoint{
		name:   name,
		client: client,
		mutex:  &sync.Mutex{},
	}
}

func (e *Endpoint) sendMessage(message string, in interface{}) ([]string, error) {
	request := make([]string, 2)
	request[0] = message
	logger.Debug(request[0])

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in.(proto.Message)); err != nil {
		logger.Error(err)
		return nil, err
	}

	e.mutex.Lock()
	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return reply, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)
	e.mutex.Unlock()

	return reply, nil
}

// GetConfiguration <- ConfigurationRequest -> ConfigurationResponse
func (e *Endpoint) GetConfiguration(
	ctx context.Context,
	in *pb.ConfigurationRequest,
) (*pb.ConfigurationResponse, error) {
	request := make([]string, 2)
	request[0] = "get-configuration"
	logger.Debug(request[0])

	response := &pb.ConfigurationResponse{}

	// FIXME: this seems unecessary on an Empty message
	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.Error(err)
		return nil, err
	}

	// XXX: this was from the "get-unit-configuration" concept, this should be used to get
	// a configuration from the configure service

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetUnitConfiguration <- Empty -> ConfigurationResponse
func (e *Endpoint) GetUnitConfiguration(ctx context.Context, in *pb.Empty) (*pb.ConfigurationResponse, error) {
	request := make([]string, 2)
	request[0] = "get-unit-configuration"
	logger.Debug(request[0])

	response := &pb.ConfigurationResponse{}

	// FIXME: this seems unecessary on an Empty message
	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.Error(err)
		return nil, err
	}

	// XXX: this was from the "get-unit-configuration" concept, this should be used to get
	// a configuration from the configure service

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetStatus <- StatusRequest -> StatusResponse
func (e *Endpoint) GetStatus(ctx context.Context, in *pb.StatusRequest) (*pb.StatusResponse, error) {
	request := make([]string, 2)
	request[0] = "get-status"
	logger.Debug(request[0])

	response := &pb.StatusResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetSettings <- SettingsRequest -> SettingsResponse
func (e *Endpoint) GetSettings(ctx context.Context, in *pb.SettingsRequest) (*pb.SettingsResponse, error) {
	request := make([]string, 2)
	request[0] = "get-settings"
	logger.Debug(request[0])

	response := &pb.SettingsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetChannel <- ChannelRequest -> ChannelResponse
func (e *Endpoint) GetChannel(ctx context.Context, in *pb.ChannelRequest) (*pb.ChannelResponse, error) {
	request := make([]string, 2)
	request[0] = "get-channel"
	logger.Debug(request[0])

	response := &pb.ChannelResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetChannels <- Empty -> ChannelsResponse
func (e *Endpoint) GetChannels(ctx context.Context, in *pb.Empty) (*pb.ChannelsResponse, error) {
	request := make([]string, 2)
	request[0] = "get-channels"
	logger.Debug(request[0])

	response := &pb.ChannelsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetJob <- JobRequest -> JobResponse
func (e *Endpoint) GetJob(ctx context.Context, in *pb.JobRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "get-job"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetJobs <- Empty -> JobsResponse
func (e *Endpoint) GetJobs(ctx context.Context, in *pb.Empty) (*pb.JobsResponse, error) {
	request := make([]string, 2)
	request[0] = "get-jobs"
	logger.Debug(request[0])

	response := &pb.JobsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetJobStatus <- JobRequest -> JobStatusResponse
func (e *Endpoint) GetJobStatus(ctx context.Context, in *pb.JobRequest) (*pb.JobStatusResponse, error) {
	request := make([]string, 2)
	request[0] = "get-job-status"
	logger.Debug(request[0])

	response := &pb.JobStatusResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModule <- ModuleRequest -> ModuleResponse
func (e *Endpoint) GetModule(ctx context.Context, in *pb.ModuleRequest) (*pb.ModuleResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module"
	logger.Debug(request[0])

	response := &pb.ModuleResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModules <- Empty -> ModulesResponse
func (e *Endpoint) GetModules(ctx context.Context, in *pb.Empty) (*pb.ModulesResponse, error) {
	request := make([]string, 2)
	request[0] = "get-modules"
	logger.Debug(request[0])

	response := &pb.ModulesResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleConnect <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleConnect(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-connect"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleDisconnect <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleDisconnect(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-disconnect"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleLoad <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleLoad(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-load"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleUnload <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleUnload(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-unload"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleReload <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleReload(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-reload"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleEnable <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleEnable(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-enable"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// TryModuleDisable <- ModuleRequest -> JobResponse
func (e *Endpoint) TryModuleDisable(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "try-module-disable"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleConfiguration <- ModuleRequest -> ConfigurationResponse
func (e *Endpoint) GetModuleConfiguration(ctx context.Context, in *pb.ModuleRequest) (*pb.ConfigurationResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-configuration"
	logger.Debug(request[0])

	response := &pb.ConfigurationResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleStatus <- ModuleRequest -> StatusResponse
func (e *Endpoint) GetModuleStatus(ctx context.Context, in *pb.ModuleRequest) (*pb.StatusResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-status"
	logger.Debug(request[0])

	response := &pb.StatusResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleSettings <- ModuleRequest -> SettingsResponse
func (e *Endpoint) GetModuleSettings(ctx context.Context, in *pb.ModuleRequest) (*pb.SettingsResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-settings"
	logger.Debug(request[0])

	response := &pb.SettingsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleJob <- ModuleRequest -> JobResponse
func (e *Endpoint) GetModuleJob(ctx context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-job"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleJobs <- ModuleRequest -> JobsResponse
func (e *Endpoint) GetModuleJobs(ctx context.Context, in *pb.ModuleRequest) (*pb.JobsResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-jobs"
	logger.Debug(request[0])

	response := &pb.JobsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleActiveJob <- ModuleRequest -> JobResponse
func (e *Endpoint) GetModuleActiveJob(ctx context.Context, in *pb.ModuleRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-active-job"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// ModuleCancelJob <- ModuleJobRequest -> JobResponse
func (e *Endpoint) ModuleCancelJob(ctx context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "module-cancel-job"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// ModuleSubmitJob <- ModuleJobRequest -> JobResponse
func (e *Endpoint) ModuleSubmitJob(ctx context.Context, in *pb.ModuleJobRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "module-submit-job"
	//request[0] = "submit-job"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// ModuleSubmitEvent <- ModuleEventRequest -> JobResponse
func (e *Endpoint) ModuleSubmitEvent(ctx context.Context, in *pb.ModuleEventRequest) (*pb.JobResponse, error) {
	request := make([]string, 2)
	request[0] = "module-submit-event"
	logger.Debug(request[0])

	response := &pb.JobResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// ModuleAvailableEvents <- ModuleRequest -> EventsResponse
func (e *Endpoint) ModuleAvailableEvents(ctx context.Context, in *pb.ModuleRequest) (*pb.EventsResponse, error) {
	request := make([]string, 2)
	request[0] = "module-available-events"
	logger.Debug(request[0])

	response := &pb.EventsResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleProperty <- PropertyRequest -> PropertyResponse
func (e *Endpoint) GetModuleProperty(ctx context.Context, in *pb.PropertyRequest) (*pb.PropertyResponse, error) {

	message := "get-module-property"
	/*
	 *reply, err := e.sendMessage(message, in)
	 *if err != nil {
	 *    return nil, err
	 *}
	 */

	request := make([]string, 2)
	request[0] = "get-module-property"
	//request[0] = "get-property"
	logger.Debug(request[0])

	response := &pb.PropertyResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": message}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": message}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": message}).Error(err)
		return nil, err
	}

	return response, nil
}

// SetModuleProperty <- PropertyRequest -> PropertyResponse
func (e *Endpoint) SetModuleProperty(ctx context.Context, in *pb.PropertyRequest) (*pb.PropertyResponse, error) {
	request := make([]string, 2)
	request[0] = "set-module-property"
	logger.Debug(request[0])

	response := &pb.PropertyResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// GetModuleProperties <- PropertiesRequest -> PropertiesResponse
func (e *Endpoint) GetModuleProperties(ctx context.Context, in *pb.PropertiesRequest) (*pb.PropertiesResponse, error) {
	request := make([]string, 2)
	request[0] = "get-module-properties"

	// A response should always be sent, even if it's empty

	response := &pb.PropertiesResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return response, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	logger.WithFields(logrus.Fields{}).Debug(request)
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		response.Error = &pb.Error{
			Code:    600,
			Message: err.Error(),
		}
		return response, nil
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response:", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		response.Error.Code = 601
		response.Error.Message = "invalid response received"
		return response, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
		response.Error.Code = 602
		response.Error.Message = "multiple responses received"
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		response.Error.Code = 603
		response.Error.Message = "failed to unmarshal response data"
		return response, err
	}

	return response, nil
}

// SetModuleProperties <- PropertiesRequest -> PropertiesResponse
func (e *Endpoint) SetModuleProperties(ctx context.Context, in *pb.PropertiesRequest) (*pb.PropertiesResponse, error) {
	request := make([]string, 2)
	request[0] = "set-module-properties"
	logger.Debug(request[0])

	response := &pb.PropertiesResponse{}

	// Serialize message body to send
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, in); err != nil {
		logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
		return nil, err
	}

	e.mutex.Lock()

	// Send the message
	request[1] = b.String()
	_ = e.client.Send(e.name, request...)
	// Wait for a reply
	reply, err := e.client.Recv()
	if err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}
	logger.WithFields(logrus.Fields{"request": request[0]}).Debug("response", reply)

	e.mutex.Unlock()

	// Validate response
	// TODO: allow multipart messages
	// TODO: check first message part for correct response type
	if len(reply) == 0 {
		er := errors.New("didn't receive expected response")
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(er)
		return nil, er
	} else if len(reply) > 1 {
		logger.WithFields(logrus.Fields{"request": request[0]}).Warn("multiple replies received, only first was handled")
	}

	// Deserialize reply into a response
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(reply[0])), response); err != nil {
		logger.WithFields(logrus.Fields{"request": request[0]}).Error(err)
		return nil, err
	}

	return response, nil
}

// --

/*
 *func (e *Endpoint) ReadConfiguration(ctx context.Context, in *pb.Empty) (*pb.Configuration, error) {
 *    return &pb.Configuration{}, nil
 *}
 */

// XXX: should messages be sent and then managed by a queue?

/*
 *func (e *Endpoint) ReadStatus(ctx context.Context, in *pb.Empty) (*pb.Status, error) {
 *    logger.Debug("ReadStatus call")
 *
 *    request := make([]string, 2)
 *    request[0] = "read-status"
 *    // Serialize message body to send
 *    b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
 *    if err := marshaler.Marshal(&b, in); err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("jsonpb marshal", err)
 *        return nil, err
 *    }
 *    request[1] = b.String()
 *    // Send the message and body
 *    _ = e.client.Send(e.name, request...)
 *    // Wait for a response
 *    reply, err := e.client.Recv()
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("read-status failed", err)
 *        return nil, err
 *    }
 *
 *    logger.WithFields(logrus.Fields{}).Debug("received data", reply)
 *
 *    // TODO: allow multipart messages
 *    // TODO: check first message part for correct response type
 *    if len(reply) == 0 {
 *        er := errors.New("didn't receive expected response")
 *        logger.Error(er)
 *        return nil, er
 *    } else if len(response) > 1 {
 *        logger.Warn("Multiple replies received, only first was handled")
 *    }
 *
 *    status := &pb.Status{}
 *    if err := jsonpb.Unmarshal(bytes.NewReader([]byte(response[0])), status); err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("Failed to create status response", err)
 *        return nil, err
 *    }
 *
 *    return status, nil
 *}
 */

// TODO: implement message format same as ReadStatus for all others following here

/*
 *func (e *Endpoint) ReadSettings(ctx context.Context, in *pb.SettingsRequest) (*pb.Settings, error) {
 *    logger.Debug("ReadSettings call")
 *
 *    req, err := message.CreateSettingsRequest("read")
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("Failed to create settings request", err)
 *        return nil, err
 *    }
 *
 *    _ = e.client.Send(e.name, string(req))
 *    response, err := e.client.Recv()
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("ReadSettings failed", err)
 *        return nil, err
 *    }
 *
 *    logger.WithFields(logrus.Fields{}).Debug("received", response)
 *
 *    // TODO: allow multipart messages
 *    if len(response) == 0 {
 *        er := errors.New("didn't receive expected response")
 *        logger.Error(er)
 *        return nil, er
 *    } else if len(response) > 1 {
 *        logger.Warn("Multiple replies received, only first was handled")
 *    }
 *
 *    settings, err := message.CreateSettingsResponse([]byte(response[0]))
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("Failed to create response", err)
 *        return nil, err
 *    }
 *
 *    return settings, nil
 *}
 */

/*
 *func (e *Endpoint) ModuleList(ctx context.Context, in *pb.ModuleRequest) (*pb.ModuleListReply, error) {
 *    logger.Debug("ModuleList call")
 *
 *    req, err := message.CreateModuleListRequest()
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("Failed to create module list request", err)
 *        return nil, err
 *    }
 *
 *    _ = e.client.Send(e.name, string(req))
 *    response, err := e.client.Recv()
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("ModuleList failed", err)
 *        return nil, err
 *    }
 *
 *    logger.Debug("Received:", response)
 *
 *    // TODO: allow multipart messages
 *    if len(response) == 0 {
 *        er := errors.New("didn't receive expected response")
 *        logger.Error(e.logger, er)
 *        return nil, er
 *    } else if len(response) > 1 {
 *        logger.Warn("Multiple replies received, only first was handled")
 *    }
 *
 *    list, err := message.CreateModuleListResponse([]byte(response[0]))
 *    if err != nil {
 *        logger.WithFields(logrus.Fields{}).Error("Failed to create response", err)
 *        return nil, err
 *    }
 *
 *    return list, nil
 *}
 */

/*
 *func (e *Endpoint) ModuleListAll(ctx context.Context, in *pb.Empty) (*pb.ModuleListReply, error) {
 *    logger.WithFields(logrus.Fields{}).Debug("ModuleListAll call")
 *
 *    req, err := message.CreateModuleListAllRequest()
 *    if err != nil {
 *        logger.Error("Failed to create module list all request", err)
 *        return nil, err
 *    }
 *
 *    _ = e.client.Send(e.name, string(req))
 *    response, err := e.client.Recv()
 *    if err != nil {
 *        logger.Error("ModuleListAll failed:", err)
 *        return nil, err
 *    }
 *
 *    logger.Debug("Received:", response)
 *
 *    // TODO: allow multipart messages
 *    if len(response) == 0 {
 *        er := errors.New("didn't receive expected response")
 *        logger.Error(e.logger, er)
 *        return nil, er
 *    } else if len(response) > 1 {
 *        logger.Warn("Multiple replies received, only first was handled")
 *    }
 *
 *    list, err := message.CreateModuleListAllResponse([]byte(response[0]))
 *    if err != nil {
 *        logger.Error("Failed to create response: ", err)
 *        return nil, err
 *    }
 *
 *    return list, nil
 *}
 */
