package service

import (
	"errors"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	orm "gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

const (
	defaultListFetchSize = 10
)

// UserService is used to work with user records in the database
type UserService struct {
	db          *gorm.DB
	roleService *RoleService
}

// NewUserService returns a new UserService instance
func NewUserService(db *gorm.DB, roleService *RoleService) *UserService {
	return &UserService{db: db, roleService: roleService}
}

// FindByEmail returns a user record for the given email
func (u *UserService) FindByEmail(email string) (*orm.User, error) {
	user := &orm.User{}
	u.db.Where("email = ?", email).First(&user)
	if user.Email == "" {
		return user, nil
	}

	id := user.ID.String()
	roles, err := u.roleService.FindByUserID(&id)
	if err != nil {
		logger.Errorf("Error in retrieving roles : %v", err)
		return nil, err
	}

	for _, el := range roles {
		user.Roles = append(user.Roles, *el)
	}

	return user, nil
}

// FindByID returns a user record for the given ID
func (u *UserService) FindByID(id string) (*orm.User, error) {
	user := &orm.User{}
	u.db.First(&user, id)
	if user.Email == "" {
		return user, nil
	}

	uid := user.ID.String()
	roles, err := u.roleService.FindByUserID(&uid)
	if err != nil {
		logger.Errorf("Error in retrieving roles : %v", err)
		return nil, err
	}

	for _, el := range roles {
		user.Roles = append(user.Roles, *el)
	}

	return user, nil
}

// Create inserts a new user record into the database
func (u *UserService) Create(user *orm.User) (*orm.User, error) {
	id, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	user.ID = id
	if err := user.HashedPassword(); err != nil {
		return nil, err
	}
	u.db.NewRecord(user)
	u.db.Create(&user)
	logger.Infof("Created: %+v", user)
	return user, nil
}

// List returns all user records that exist in the database
func (u *UserService) List(first *int32, after *string) ([]*orm.User, error) {
	users := make([]*orm.User, 0)
	var fetchSize int32
	if first == nil {
		fetchSize = defaultListFetchSize
	} else {
		fetchSize = *first
	}

	if after != nil {
		decodedIndex, _ := DecodeCursor(after)
		u.db.Order(
			"created_at desc",
		).Limit(
			fetchSize,
		).Where(
			"created_at < ?",
			u.db.Table("users").Select("created_at").Where("id = ?", decodedIndex).QueryExpr(),
		).Find(&users)
		return users, nil
	}

	u.db.Order("created_at desc").Limit(fetchSize).Find(&users)
	return users, nil
}

// Update a user record if it exists in the database
func (r *UserService) Update(user *orm.User) *orm.User {
	r.db.Model(&user).Update(&user)
	return user
}

// Delete a user record if it exists in the database
func (r *UserService) Delete(user *orm.User) *orm.User {
	r.db.Delete(&user)
	return user
}

// Count returns the number of user records that exist in the database
func (u *UserService) Count() (int, error) {
	var count int
	u.db.Table("users").Count(&count)
	return count, nil
}

// ComparePassword checks if a user exists with the provided credentials and returns it if one does
func (u *UserService) ComparePassword(userCredentials *model.UserCredentials) (*orm.User, error) {
	user, err := u.FindByEmail(userCredentials.Email)
	if err != nil {
		return nil, errors.New(context.UnauthorizedAccess)
	}
	if result := user.ComparePassword(userCredentials.Password); !result {
		return nil, errors.New(context.UnauthorizedAccess)
	}
	return user, nil
}
