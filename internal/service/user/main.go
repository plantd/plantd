package user

import (
	"github.com/gofrs/uuid"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/orm"
	dbm "gitlab.com/plantd/plantd/internal/orm/model"
	tf "gitlab.com/plantd/plantd/internal/resolver/transformation"
)

var user *userService

func init() {
	user = NewUserService()
}

type userService struct {
}

func NewUserService() *userService {
	return &userService{}
}

// FindByEmail returns a user record for the given email
func FindByEmail(o *orm.ORM, email string) (*model.User, error) {
	u := &model.User{}
	o.DB.Where("email = ?", email).First(&u)
	if u.Email == "" {
		return u, nil
	}

	// FIXME: add roles back in
	//roles, err := roles.FindByUserID(&u.ID)
	//if err != nil {
	//	logger.Errorf("Error in retrieving roles : %v", err)
	//	return nil, err
	//}
	//
	//for _, el := range roles {
	//	u.Roles = append(u.Roles, *el)
	//}

	return u, nil
}

// FindByID returns a user record for the given ID
func FindByID(o *orm.ORM, id string) (*model.User, error) {
	u := &model.User{}
	o.DB.Where("id = ?", id).First(&u)
	if u.ID == "" {
		return u, nil
	}

	// FIXME: add roles back in
	//roles, err := u.roleService.FindByUserID(&u.ID)
	//if err != nil {
	//	logger.Errorf("Error in retrieving roles : %v", err)
	//	return nil, err
	//}
	//
	//for _, el := range roles {
	//	u.Roles = append(u.Roles, *el)
	//}

	return u, nil
}

// CreateUpdate is used to create a new user if one does not exist for the input, or update it if one does
func CreateUpdate(
	o *orm.ORM,
	input model.UserInput,
	update bool,
	ids ...string,
) (*model.User, error) {
	dbo, err := tf.GQLInputUserToDBUser(&input, update, ids...)
	if err != nil {
		return nil, err
	}

	// Create scoped clean db interface
	db := o.DB.New().Begin()
	if !update {
		db = db.Create(dbo).First(dbo) // Create the user
		if db.Error != nil {
			return nil, db.Error
		}
	} else {
		db = db.Model(&dbo).Update(dbo).First(dbo) // Or update it
	}

	gql, err := tf.DBUserToGQLUser(dbo)
	if err != nil {
		db.Rollback()
		return nil, err
	}

	db = db.Commit()

	return gql, db.Error
}

// Delete removes a user from the database for the given ID
func Delete(o *orm.ORM, id string) (bool, error) {
	u, _ := FindByID(o, id)
	if u.ID == "" {
		return false, nil
	}
	ID, err := uuid.FromString(id)
	if err != nil {
		return false, err
	}
	// FIXME: this doesn't work
	o.DB.Delete(&dbm.BaseModel{ID: ID})
	return true, nil
}

func List(o *orm.ORM, id *string) (*model.UsersConnection, error) {
	entity := "users"
	whereID := "id = ?"
	record := &model.UsersConnection{}
	var dbRecords []*dbm.User
	db := o.DB.New()
	if id != nil {
		db = db.Where(whereID, *id)
	}

	db = db.Find(&dbRecords).Count(&record.TotalCount)
	for _, dbRec := range dbRecords {
		if rec, err := tf.DBUserToGQLUser(dbRec); err != nil {
			logger.Errorfn(entity, err)
		} else {
			node := &model.UsersEdge{Node: rec}
			record.Edges = append(record.Edges, node)
		}
	}
	return record, db.Error
}

// ## Permissions
// TODO: need to implement roles and networks for better resource management

// CanCreate checks if the user can create user resources
func CanCreate(u *dbm.User) bool {
	return true
}

// CanRead checks if the user can read user resources
func CanRead(u *dbm.User) bool {
	return true
}

// CanUpdate checks if the user can update user resources
func CanUpdate(u *dbm.User) bool {
	return true
}

// CanDelete checks if the user can delete user resources
func CanDelete(u *dbm.User) bool {
	return true
}
