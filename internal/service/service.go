package service

import dbm "gitlab.com/plantd/plantd/internal/orm/model"

// Service is the base type for others add
type Service struct{}

// ## Permissions
// TODO: need to implement roles and networks for better resource management
// TODO: add Service to another type and use instead of using module functions directly
// TODO: figure out custom permissions by resource type
// TODO: return (bool, error) from these and pick up with:
//   `if allowed, err := CanX(...); && !allowed { return nil, err }`
// TODO: determine what limits permissions (network, resource ID, role)

// CanCreate checks if the user can create user resources
func CanCreate(user *dbm.User) bool {
	return true
}

// CanRead checks if the user can read user resources
func CanRead(user *dbm.User) bool {
	return true
}

// CanUpdate checks if the user can update user resources
func CanUpdate(user *dbm.User) bool {
	return true
}

// CanDelete checks if the user can delete user resources
func CanDelete(user *dbm.User) bool {
	return true
}
