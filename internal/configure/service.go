package service

import (
	"encoding/json"
	"errors"
	"strings"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/pkg/model"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
)

type Configure struct {
	db *mongo.Database
}

// Create a new configure service.
func NewConfigure(db *mongo.Database) *Configure {
	return &Configure{db: db}
}

// List the configurations for a given namespace.
func (c *Configure) List(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationsResponse, error) {
	logger.Debug("List called")

	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))

	// TODO: don't need full document, just retrieve ID and namespace
	cur, err := coll.Find(ctx, bson.D{})
	if err != nil {
		logger.Error(err)
		return nil, err
	}
	defer cur.Close(ctx)

	var configurations []*pb.Configuration

	for cur.Next(context.TODO()) {
		configuration := model.Configuration{}
		err := cur.Decode(&configuration)
		if err != nil {
			logger.Error("Couldn't decode configuration")
			return nil, err
		}

		// Uncomment to display configuration during testing
		out, err := json.MarshalIndent(configuration, " ", " ")
		if err != nil {
			logger.Error("Couldn't unmarshall configuration")
		}
		logger.Debug("Configuration: ", string(out))

		// Create the protobuf message
		conf := &pb.Configuration{
			Id:         configuration.ID.Hex(),
			Name:       configuration.Name,
			Namespace:  in.Namespace,
			Objects:    []*pb.Object{},
			Properties: []*pb.Property{},
		}

		configurations = append(configurations, conf)
	}

	return &pb.ConfigurationsResponse{
		Configurations: configurations,
	}, nil
}

// List all configurations from every namespace found in the Protobuf files.
func (c *Configure) ListAll(ctx context.Context, in *pb.Empty) (*pb.ConfigurationsResponse, error) {
	var configurations []*pb.Configuration

	logger.Debug("ListAll called")

	for key, value := range pb.Configuration_Namespace_name {
		coll := c.db.Collection(strings.ToLower(value))

		// TODO: don't need full document, just retrieve ID and namespace
		cur, err := coll.Find(ctx, nil)
		if err != nil {
			return nil, err
		}

		defer cur.Close(ctx)

		for cur.Next(context.TODO()) {
			configuration := model.Configuration{}
			err := cur.Decode(&configuration)
			if err != nil {
				logger.Error("Couldn't decode configuration")
				return nil, err
			}

			// All that we care about for the list is the ID and namespace
			conf := &pb.Configuration{
				Id:         configuration.ID.Hex(),
				Namespace:  pb.Configuration_Namespace(key),
				Objects:    []*pb.Object{},
				Properties: []*pb.Property{},
			}

			configurations = append(configurations, conf)
		}
	}

	return &pb.ConfigurationsResponse{
		Configurations: configurations,
	}, nil
}

// Create a new configuration in the database using the Protobuf message given.
func (c *Configure) Create(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	logger.Printf("Create called for %s", in.Namespace)

	// Handle a missing ID
	id := primitive.NewObjectID()
	if in.Id != "" {
		logger.Debug("Object ID exists. Re-using...")
		// Convert to an ObjectID type
		objectID, idErr := primitive.ObjectIDFromHex(in.Id)
		if idErr != nil {
			logger.Errorf("Unable to convert %s to ObjectID", in.Id)
			return nil, idErr
		}
		id = objectID
	}

	// Handle a missing name
	name := id.Hex()
	if in.GetConfiguration().GetName() != "" {
		name = in.GetConfiguration().GetName()
	}

	// Convert objects to the desired model
	objects, objErr := model.ConvertObjects(in.GetConfiguration().GetObjects())
	if objErr != nil {
		logger.Errorf("Unable to convert objects.")
		return nil, objErr
	}

	// Create the new document
	conf := model.Configuration{
		ID:         id,
		Name:       name,
		Namespace:  in.GetNamespace().String(),
		Objects:    objects,
		Properties: model.ConvertProperties(in.GetConfiguration().GetProperties()),
	}

	// Insert the document
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	insertResponse, insertErr := coll.InsertOne(ctx, conf)
	if insertErr != nil {
		logger.Error("Could not insert new configuration")
		return nil, insertErr
	}

	// TODO: Ideally, send the updated protobuf response configuration instead of the old one
	return &pb.ConfigurationResponse{
		Configuration: &pb.Configuration{
			Id:         insertResponse.InsertedID.(primitive.ObjectID).Hex(),
			Name:       name,
			Namespace:  in.Namespace,
			Objects:    in.GetConfiguration().GetObjects(),
			Properties: in.GetConfiguration().GetProperties(),
		},
	}, nil
}

// Read out an entire configuration.
// TODO: create conf.ToProtobuf and do most of this there
func (c *Configure) Read(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))

	configuration := model.Configuration{}
	id, err := primitive.ObjectIDFromHex(in.Id)
	if err != nil {
		logger.Error("Invalid ID value provided")
		return nil, err
	}

	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	err = coll.FindOne(ctx, filter).Decode(&configuration)
	if err != nil {
		logger.Error("Couldn't decode configuration")
		return nil, err
	}

	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		logger.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}

// Update a configuration using the given Protobuf message.
func (c *Configure) Update(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	logger.Printf("Update called: %s %s", in.Id, in.Namespace.String())
	// TODO: Handle the case where the namespace actually changes the selected DB (ideally it should not)

	// Use the original ID as
	if in.GetConfiguration().GetId() == "" {
		in.GetConfiguration().Id = in.GetId()
	} else if in.GetId() != in.GetConfiguration().GetId() {
		idErr := errors.New("cannot write requested ID as documents are immutable")
		logger.Error("Provided target ID is not matching the source ID")
		return nil, idErr
	}

	// Convert the source ID to an ObjectID
	sourceID, sourceErr := primitive.ObjectIDFromHex(in.Id)
	if sourceErr != nil {
		logger.Error("Unable to convert hexID to sourceID")
		return nil, sourceErr
	}

	// Convert the target ID to an ObjectID
	targetID, targetErr := primitive.ObjectIDFromHex(in.GetConfiguration().GetId())
	if targetErr != nil {
		logger.Error("Unable to convert hexID to targetID")
		return nil, targetErr
	}

	// Convert objects to the desired model
	objects, objErr := model.ConvertObjects(in.GetConfiguration().GetObjects())
	if objErr != nil {
		logger.Errorf("Unable to convert objects.")
		return nil, objErr
	}

	// Attempt to update the document with UpdateOne
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	filter := bson.D{primitive.E{Key: "_id", Value: sourceID}}

	// Create the new document
	conf := model.Configuration{
		ID:         targetID,
		Name:       in.GetConfiguration().GetName(),
		Namespace:  in.GetNamespace().String(),
		Objects:    objects,
		Properties: model.ConvertProperties(in.GetConfiguration().GetProperties()),
	}

	// Attempt to update the configuration
	configuration := model.Configuration{}
	returnDoc := options.After
	replaceOptions := options.FindOneAndReplace()
	replaceOptions.SetReturnDocument(returnDoc)
	updateErr := coll.FindOneAndReplace(ctx, filter, conf, replaceOptions).Decode(&configuration)
	if updateErr != nil {
		logger.Errorf("Error updating configuration %s", in.Id)
		return nil, updateErr
	}

	// Note: We are currently returning the old configuration
	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		logger.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	// Return the protobuf configuration as a response
	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}

// Delete a configuration.
func (c *Configure) Delete(ctx context.Context, in *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	logger.Printf("Delete called: %s %s", in.Id, in.Namespace.String())

	// Convert to an ObjectID type
	objectID, idErr := primitive.ObjectIDFromHex(in.Id)
	if idErr != nil {
		logger.Error("Unable to convert hexID to ObjectID")
		return nil, idErr
	}

	// Attempt to delete the document with DeleteOne
	coll := c.db.Collection(strings.ToLower(in.Namespace.String()))
	filter := bson.D{primitive.E{Key: "_id", Value: objectID}}
	configuration := model.Configuration{}
	delErr := coll.FindOneAndDelete(ctx, filter).Decode(&configuration)
	if delErr != nil {
		logger.Errorf("Error deleting configuration %s %s", in.Id, in.Namespace.String())
		return nil, delErr
	}

	// Convert the returned configuration into a protobuf configuration
	_configuration, protoErr := configuration.ToProtobuf()
	if protoErr != nil {
		logger.Error("Error converting configuration into protobuf version")
		return nil, protoErr
	}

	// Return the protobuf configuration as a response
	return &pb.ConfigurationResponse{
		Configuration: _configuration,
	}, nil
}
