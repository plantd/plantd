package migrations

import (
	"context"

	migrate "github.com/xakep666/mongo-migrate"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func init() {
	_ = migrate.Register(func(db *mongo.Database) error {
		doc := bson.D{
			{"name", "Sample UI"},
			{"namespace", "PRESENT"},
			{"objects", bson.A{
				bson.D{{"foo", "bar"}},
			}},
		}
		_, err := db.Collection("present").InsertOne(context.TODO(), doc)
		if err != nil {
			return err
		}
		return nil
	}, func(db *mongo.Database) error {
		return nil
	})
}
