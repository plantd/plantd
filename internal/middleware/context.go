package middleware

import (
	"context"

	"gitlab.com/plantd/plantd/pkg/util"

	"github.com/gin-gonic/gin"
)

func Context() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), util.MasterContextKeys.GinContextKey, c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}
