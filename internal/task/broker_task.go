package task

import (
	"fmt"
	"net"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/service"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Task struct {
	name     string
	endpoint *service.Endpoint
	config   *context.BrokerConfig
}

func NewTask(name string, endpoint *service.Endpoint, config *context.BrokerConfig) (task *Task, err error) {
	// Initialize service client / task
	task = &Task{
		name,
		endpoint,
		config,
	}

	return task, nil
}

func (t *Task) Start() error {
	// Create the channel to listen on
	addr := fmt.Sprintf("%s:%d", t.config.Units[t.name].RPCBind, t.config.Units[t.name].RPCPort)
	logger.WithFields(logrus.Fields{"endpoint": addr}).Debug("bind task")
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.WithFields(logrus.Fields{"err": err}).Error("failed to listen")
		return err
	}

	var opts []grpc.ServerOption

	logger.WithFields(logrus.Fields{"task": t.name}).Debug("start gRPC server")

	// Start the gRPC endpoint
	srv := grpc.NewServer(opts...)
	pb.RegisterEndpointServer(srv, t.endpoint)
	reflection.Register(srv)
	if err := srv.Serve(lis); err != nil {
		logger.WithFields(logrus.Fields{"err": err}).Error("failed to serve")
		return err
	}

	return nil
}
