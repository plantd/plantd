package model

import (
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

//type Object struct {
//	ID         string
//	Name       string
//	Type       string
//	Objects    []*Object
//	Properties []*Property
//}

func (o *Object) FromProtobuf(in *pb.Object) error {
	var objects []*Object
	for _, obj := range in.Objects {
		var _obj Object
		err := _obj.FromProtobuf(obj)
		if err != nil {
			return err
		}
		objects = append(objects, &_obj)
	}

	var properties []*Property
	for _, prop := range in.Properties {
		var _prop Property
		err := _prop.FromProtobuf(prop)
		if err != nil {
			return err
		}
		properties = append(properties, &_prop)
	}

	o.ID = in.Id
	o.Name = &in.Name
	o.Type = &in.Type
	o.Objects = objects
	o.Properties = properties

	return nil
}
