package model

import (
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

// Property : Describes the details through KV structure
//type Property struct {
//	Key   string
//	Value string
//}

// FromProtobuf : Sets the Property's key and value from the Protobuf property
func (p *Property) FromProtobuf(in *pb.Property) error {
	p.Key = &in.Key
	p.Value = &in.Value

	return nil
}
