package model

import (
	pb "gitlab.com/plantd/go-plantd/proto/v1"
)

//type Configuration struct {
//	ID         string
//	Name       string
//	Namespace  string
//	Objects    []*Object
//	Properties []*Property
//}

func (c *Configuration) FromProtobuf(in *pb.Configuration) error {
	c.ID = in.Id
	c.Name = &in.Name
	namespace := in.Namespace.String()
	c.Namespace = &namespace

	for _, pbo := range in.Objects {
		obj := &Object{}
		if err := obj.FromProtobuf(pbo); err != nil {
			return err
		}
		c.Objects = append(c.Objects, obj)
	}

	for _, pbo := range in.Properties {
		prop := &Property{}
		if err := prop.FromProtobuf(pbo); err != nil {
			return err
		}
		c.Properties = append(c.Properties, prop)
	}

	return nil
}
