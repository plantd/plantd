package util

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"

	"gitlab.com/plantd/plantd/internal/logger"

	"gitlab.com/plantd/plantctl/pkg"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/pkg/util"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
)

// Check input arguments
func CheckArgs(args []string) {
	if len(args) > 1 {
		r, _ := regexp.Compile("^-v$|^(-{2})?version$")
		if r.Match([]byte(args[1])) {
			fmt.Println(pkg.VERSION)
		}
		os.Exit(0)
	}
}

func ExperimentStatusToString(in []model.ExperimentStatus) (out []string) {
	for _, el := range in {
		out = append(out, string(el))
	}
	return
}

func JobStatusToString(in []model.JobStatus) (out []string) {
	for _, el := range in {
		out = append(out, string(el))
	}
	return
}

func ModuleStateToString(in []model.ModuleState) (out []string) {
	for _, el := range in {
		out = append(out, string(el))
	}
	return
}

func IndexOf(element string, data []string) int {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1
}

// RootDir returns the root path of the project.
// This only works at this level, if it's moved to a different depth it won't work.
func RootDir() string {
	a, b, c, x := runtime.Caller(0)
	logger.Debug(a, c, x)
	d := path.Join(path.Dir(b), "..")
	return filepath.Dir(d)
}

// GinContextFromContext can be used to read the Gin context in a request, use with:
//
//	gc, err := util.GinContextFromContext(ctx)
// if err != nil {
//   return nil, err
// }
func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value(util.MasterContextKeys.GinContextKey)
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}
