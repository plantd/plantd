package loader

import (
	"fmt"
	"sync"

	"gitlab.com/plantd/plantd/internal/service/configure"

	"github.com/graph-gophers/dataloader"
	"gitlab.com/plantd/plantd/internal/model"
	"golang.org/x/net/context"
)

type confLoader struct {
}

func newConfLoader() dataloader.BatchFunc {
	return confLoader{}.loadBatch
}

func (ldr confLoader) loadBatch(ctx context.Context, keys dataloader.Keys) []*dataloader.Result {
	var (
		n       = len(keys)
		results = make([]*dataloader.Result, n)
		wg      sync.WaitGroup
	)

	wg.Add(n)

	for i, key := range keys {
		go func(i int, key dataloader.Key) {
			defer wg.Done()
			conf, err := configure.FindByID(key.String(), "present")
			results[i] = &dataloader.Result{Data: conf, Error: err}
		}(i, key)
	}

	wg.Wait()

	return results
}

func LoadConf(ctx context.Context, key string) (*model.Configuration, error) {
	var conf *model.Configuration

	ldr, err := extract(ctx, confLoaderKey)
	if err != nil {
		return nil, err
	}

	data, err := ldr.Load(ctx, dataloader.StringKey(key))()
	if err != nil {
		return nil, err
	}

	conf, ok := data.(*model.Configuration)
	if !ok {
		return nil, fmt.Errorf("wrong type: the expected type is %T but got %T", conf, data)
	}

	return conf, nil
}
