package loader

import (
	"fmt"
	"time"

	"github.com/graph-gophers/dataloader"
	cache "github.com/patrickmn/go-cache"

	//"github.com/hashicorp/golang-lru"
	"golang.org/x/net/context"
)

type key string

const (
	userLoaderKey   key = "user"
	roleLoaderKey   key = "role"
	confLoaderKey   key = "conf"
	moduleLoaderKey key = "module"
)

type Client interface {
	moduleGetter
}

// Initialize a lookup map of context keys to batch functions.
//
// When Attach is called on the Collection, the batch functions are used to create new dataloader
// instances. The instances are attached to the request context at the provided keys.
//
// The keys are then used to extract the dataloader instances from the request context.
func NewCollection(client Client) Collection {
	//arc, _ := lru.NewARC(100)
	//cache := &Cache{arc}
	c := cache.New(2*time.Second, 2*time.Second)
	return Collection{
		dataloaderFuncMap: map[key]dataloader.BatchFunc{
			userLoaderKey:   newUserLoader(),
			roleLoaderKey:   newRoleLoader(),
			confLoaderKey:   newConfLoader(),
			moduleLoaderKey: newModuleLoader(client),
		},
		moduleCache: &Cache{c},
	}
}

// Collection holds an internal lookup of initialized batch data load functions.
type Collection struct {
	dataloaderFuncMap map[key]dataloader.BatchFunc
	moduleCache       *Cache
}

func (c Collection) Attach(ctx context.Context) context.Context {
	for k, batchFn := range c.dataloaderFuncMap {
		if k == moduleLoaderKey {
			ctx = context.WithValue(ctx, k, dataloader.NewBatchedLoader(batchFn, dataloader.WithCache(c.moduleCache)))
		} else {
			ctx = context.WithValue(ctx, k, dataloader.NewBatchedLoader(batchFn))
		}
	}

	return ctx
}

// extract is a helper function to make common get-value, assert-type, return-error-or-value
// operations easier.
func extract(ctx context.Context, k key) (*dataloader.Loader, error) {
	ldr, ok := ctx.Value(k).(*dataloader.Loader)
	if !ok {
		return nil, fmt.Errorf("unable to find %s loader on the request context", k)
	}

	return ldr, nil
}

// Implements Stringer.
func (k key) String() string {
	return string(k)
}
