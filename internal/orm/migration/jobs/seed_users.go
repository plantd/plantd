package jobs

import (
	"gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

var (
	uname       = "Test User"
	fname       = "Test"
	lname       = "User"
	nname       = "Foo Bar"
	description = "This is the first user ever!"
	location    = "His house, maybe?"
	users       = []*model.User{
		&model.User{
			Email:       "test@test.com",
			Name:        &uname,
			FirstName:   &fname,
			LastName:    &lname,
			NickName:    &nname,
			Description: &description,
			Location:    &location,
		},
		&model.User{
			Email:       "test2@test.com",
			Name:        &uname,
			FirstName:   &fname,
			LastName:    &lname,
			NickName:    &nname,
			Description: &description,
			Location:    &location,
		},
	}
)

// SeedUsers inserts the first users
var SeedUsers *gormigrate.Migration = &gormigrate.Migration{
	ID: "SEED_USERS",
	Migrate: func(db *gorm.DB) error {
		for _, u := range users {
			if err := db.Create(u).Error; err != nil {
				return err
			}
			if err := db.Create(&model.UserAPIKey{UserID: u.ID}).Error; err != nil {
				return err
			}
		}
		return nil
	},
	Rollback: func(db *gorm.DB) error {
		for _, u := range users {
			if err := db.Delete(u).Error; err != nil {
				return err
			}
		}
		return nil
	},
}
