package migration

import (
	"fmt"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/orm/migration/jobs"
	orm "gitlab.com/plantd/plantd/internal/orm/model"
	consts "gitlab.com/plantd/plantd/pkg/util/const"

	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
)

func updateMigration(db *gorm.DB) (err error) {
	// Migrate the schema
	err = db.AutoMigrate(
		&orm.Network{},
		&orm.Permission{},
		&orm.Role{},
		&orm.User{},
		&orm.UserAPIKey{},
		&orm.UserProfile{},
	).Error
	if err != nil {
		return err
	}
	return addIndexes(db)
}

func addIndexes(db *gorm.DB) (err error) {
	// Entity names
	//db.NewScope(&models.User{}).GetModelStruct().TableName(db)

	// FKs
	if err := db.Model(&orm.UserProfile{}).
		AddForeignKey("user_id", consts.GetTableName(consts.EntityNames.Users)+"(id)", "RESTRICT", "RESTRICT").Error; err != nil {
		return err
	}
	if err := db.Model(&orm.UserAPIKey{}).
		AddForeignKey("user_id", consts.GetTableName(consts.EntityNames.Users)+"(id)", "RESTRICT", "RESTRICT").Error; err != nil {
		return err
	}
	if err := db.Model(&orm.UserRole{}).
		AddForeignKey("user_id", consts.GetTableName(consts.EntityNames.Users)+"(id)", "CASCADE", "CASCADE").Error; err != nil {
		return err
	}
	if err := db.Model(&orm.UserRole{}).
		AddForeignKey("role_id", consts.GetTableName(consts.EntityNames.Roles)+"(id)", "CASCADE", "CASCADE").Error; err != nil {
		return err
	}
	if err := db.Model(&orm.UserPermission{}).
		AddForeignKey("user_id", consts.GetTableName(consts.EntityNames.Users)+"(id)", "CASCADE", "CASCADE").Error; err != nil {
		return err
	}
	if err := db.Model(&orm.UserPermission{}).
		AddForeignKey("permission_id", consts.GetTableName(consts.EntityNames.Permissions)+"(id)", "CASCADE", "CASCADE").Error; err != nil {
		return err
	}
	// Indexes
	// None needed so far
	return nil
}

// ServiceAutoMigration migrates all the tables and modifications to the connected source
func ServiceAutoMigration(db *gorm.DB) error {
	// Initialize the migration empty so InitSchema runs always first on creation
	m := gormigrate.New(db, gormigrate.DefaultOptions, nil)
	m.InitSchema(func(db *gorm.DB) error {
		logger.Info("[Migration.InitSchema] Initializing database schema")
		switch db.Dialect().GetName() {
		case "postgres":
			db.Exec("CREATE EXTENSION IF NOT EXISTS\"uuid-ossp\";")
		}
		if err := updateMigration(db); err != nil {
			return fmt.Errorf("[Migration.InitSchema]: %v", err)
		}
		return nil
	})

	if err := m.Migrate(); err != nil {
		return err
	}

	if err := updateMigration(db); err != nil {
		return err
	}
	// Keep a list of migrations here
	m = gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		jobs.SeedUsers,
		jobs.SeedRBAC,
	})
	return m.Migrate()
}
