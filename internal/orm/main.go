// Package orm provides `GORM` helpers for the creation, migration and access
// on the project's database
package orm

import (
	"errors"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/orm/migration"
	"gitlab.com/plantd/plantd/internal/orm/model"
	"gitlab.com/plantd/plantd/internal/resolver/transformation"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/markbates/goth"
)

var autoMigrate, logMode, seedDB bool
var dsn, dialect string

// ORM struct to holds the gorm pointer to db
type ORM struct {
	DB *gorm.DB
}

func init() {
}

// Factory creates a db connection with the selected dialect and connection
// string
func Factory(config *context.MasterConfig) (*ORM, error) {
	db, err := gorm.Open(config.DB.Dialect, config.DB.DSN)
	if err != nil {
		logger.Panic("[ORM] err: ", err)
	}

	orm := &ORM{DB: db}
	// Log every SQL command on dev, @prod: this should be disabled
	db.LogMode(config.DB.LogMode)

	// Automigrate tables
	if config.DB.AutoMigrate {
		err = migration.ServiceAutoMigration(orm.DB)
		if err != nil {
			logger.Error("[ORM.autoMigrate] err: ", err)
		}
	}

	logger.Info("[ORM] Database connection initialized.")

	return orm, nil
}

//FindUserByAPIKey finds the user that is related to the API key
func (o *ORM) FindUserByAPIKey(apiKey string) (*model.User, error) {
	db := o.DB.New()
	uak := &model.UserAPIKey{}

	if apiKey == "" {
		return nil, errors.New("API key is empty")
	}

	if err := db.Preload("User").Where("api_key = ?", apiKey).Find(uak).Error; err != nil {
		return nil, err
	}

	return &uak.User, nil
}

// FindUserByJWT finds the user that is related to the APIKey token
func (o *ORM) FindUserByJWT(email string, provider string, userID string) (*model.User, error) {
	db := o.DB.New()
	up := &model.UserProfile{}

	if provider == "" || userID == "" {
		return nil, errors.New("provider or userId empty")
	}

	if err := db.Preload("User").Where("email  = ? AND provider = ? AND external_user_id = ?", email, provider, userID).
		First(up).Error; err != nil {
		return nil, err
	}

	return &up.User, nil
}

// UpsertUserProfile saves the user if doesn't exists and adds the OAuth profile
func (o *ORM) UpsertUserProfile(input *goth.User) (*model.User, error) {
	db := o.DB.New()
	u := &model.User{}
	up := &model.UserProfile{}
	u, err := transformation.GothUserToDBUser(input, false)

	if err != nil {
		return nil, err
	}

	if tx := db.Where("email = ?", input.Email).First(u); !tx.RecordNotFound() && tx.Error != nil {
		return nil, tx.Error
	}

	if tx := db.Model(u).Save(u); tx.Error != nil {
		return nil, err
	}

	if tx := db.Where("email = ? AND provider = ? AND external_user_id = ?",
		input.Email, input.Provider, input.UserID).First(up); !tx.RecordNotFound() && tx.Error != nil {
		return nil, err
	}

	up, err = transformation.GothUserToDBUserProfile(input, false)
	if err != nil {
		return nil, err
	}

	up.User = *u
	if tx := db.Model(up).Save(up); tx.Error != nil {
		return nil, tx.Error
	}

	return u, nil
}
