package model

// Role defines a role for the user
type Role struct {
	BaseModelSoftDelete
	Name        *string      `gorm:"not null"`
	Description *string      `gorm:"size:1024"`
	ParentRoles []Role       `gorm:"many2many:role_parents;association_jointable_foreignkey:parent_role_id"`
	ChildRoles  []Role       `gorm:"many2many:role_parents;association_jointable_foreignkey:role_id"`
	Permissions []Permission `gorm:"many2many:role_permissions"`
}

// RolePermission defines relation between a role and permissions
type RolePermission struct {
	RoleID       int
	PermissionID int
}
