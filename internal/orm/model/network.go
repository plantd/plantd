package model

type Network struct {
	BaseModel
	Domain string
	Name   string
	Users  []*User
}
