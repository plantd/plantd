package model

// Permission defines a permission scope for the user
type Permission struct {
	BaseModelSeq
	Tag         string `gorm:"not null;unique_index"`
	Description string `gorm:"size:1024"`
}
