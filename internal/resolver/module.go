package resolver

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/plantd/plantd/internal/service/configure"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/service"

	"golang.org/x/net/context"
)

// FIXME: don't think a dataloader is necessary here

func (r *QueryResolver) Module(ctx context.Context, serviceName string, moduleName string) (*model.Module, error) {
	/*
	 *    broker := ctx.Value("brokerService").(*service.BrokerService)
	 *    moduleResponse, err := broker.GetModule(args.ServiceName, args.ModuleName)
	 *    if err != nil {
	 *        fmt.Errorf("GetModule error: %s", err)
	 *    }
	 *
	 *    return &ModuleResolver{moduleResponse}, err
	 */

	moduleResolver, err := NewModule(ctx, NewModuleArgs{Name: moduleName})
	if err != nil {
		return nil, err
	}
	return moduleResolver.module, nil
}

func (r *QueryResolver) Modules(ctx context.Context, first *int, after *string) (*model.ModulesConnection, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	// TODO: allow serviceName argument instead of hardcoded 'acquire'
	broker := ctx.Value("brokerService").(*service.BrokerService)
	modulesResponse, err := broker.GetModules("acquire")
	if err != nil {
		return nil, fmt.Errorf("GetModules error: %s", err)
	}

	// TODO: get count from service
	count := len(*modulesResponse)
	hasNext := false

	edges := make([]*model.ModulesEdge, count)
	for i, element := range *modulesResponse {
		edge := &model.ModulesEdge{
			Cursor: element.ModuleName,
			Node:   element,
		}
		edges[i] = edge
	}

	return &model.ModulesConnection{
		TotalCount: count,
		Edges:      edges,
		PageInfo: &model.PageInfo{
			StartCursor: &(*modulesResponse)[0].ModuleName,
			EndCursor:   &(*modulesResponse)[len(*modulesResponse)-1].ModuleName,
			HasNextPage: hasNext,
		},
	}, nil
}

func (r *MutationResolver) SetModuleProperty(
	ctx context.Context,
	serviceName string,
	moduleName string,
	property model.PropertyInput,
) (*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to update modules")
	}

	// Convert a property input into the pb Property
	convertedProperty := &pb.Property{
		Key:   property.Key,
		Value: property.Value,
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	propertyResponse, err := broker.SetModuleProperty(serviceName, moduleName, convertedProperty)
	if err != nil {
		return nil, fmt.Errorf("SetModuleProperty error: %s", err)
	}

	return propertyResponse, nil
}

func (r *MutationResolver) SetModuleProperties(
	ctx context.Context,
	serviceName string,
	moduleName string,
	properties []*model.PropertyInput,
) ([]*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to update modules")
	}

	// Parse the properties into a pb property array
	var reqProperties []*pb.Property
	if properties != nil {
		reqProperties = ConvertProperties(properties)
	}

	// Use the broker service
	broker := ctx.Value("brokerService").(*service.BrokerService)
	propertiesResponse, err := broker.SetModuleProperties(serviceName, moduleName, reqProperties)
	if err != nil {
		return nil, fmt.Errorf("SetModuleProperties error: %s", err)
	}

	// Create property resolvers for each property
	var _properties []*model.Property
	_properties = append(_properties, propertiesResponse...)

	return _properties, nil
}

func (r *MutationResolver) SetModuleRecipe(
	ctx context.Context,
	serviceName string,
	moduleName string,
	recipeID string,
) ([]*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to update modules")
	}

	// Handle the situation where it is reset
	if recipeID == "" {
		logger.Debugf("Resetting module recipe")
		var resetProperties []*pb.Property
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-id",
			Value: "",
		})
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-name",
			Value: "",
		})
		resetProperties = append(resetProperties, &pb.Property{
			Key:   "recipe-raw",
			Value: "0,0,0,0,0,10",
		})

		// Use the broker service
		broker := ctx.Value("brokerService").(*service.BrokerService)
		propertiesResponse, err := broker.SetModuleProperties(serviceName, moduleName, resetProperties)
		if err != nil {
			return nil, fmt.Errorf("SetModuleProperties error: %s", err)
		}
		// Create property resolvers for each property
		var properties []*model.Property
		properties = append(properties, propertiesResponse...)

		return properties, nil
	}

	// Get the recipe
	conf, err := configure.FindByID(recipeID, "RECIPE")
	if err != nil {
		logger.Debugf("GraphQL error : %v", err)
		return nil, err
	}

	// Get the properties from the recipe and create the request
	recipe := &ConfigurationResolver{conf}
	var reqProperties []*pb.Property
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-id",
		Value: recipeID,
	})
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-name",
		Value: *recipe.Name(),
	})
	// Convert the recipe from object to a string
	var rawRecipe []string
	for _, someObject := range *recipe.Objects() {
		var phaseString []string
		for _, someProperty := range *someObject.Properties() {
			phaseString = append(phaseString, *someProperty.Value())
		}
		rawRecipe = append(rawRecipe, strings.Join(phaseString, ","))
	}
	reqProperties = append(reqProperties, &pb.Property{
		Key:   "recipe-raw",
		Value: strings.Join(rawRecipe, "|"),
	})

	// Use the broker service
	broker := ctx.Value("brokerService").(*service.BrokerService)
	propertiesResponse, err := broker.SetModuleProperties(serviceName, moduleName, reqProperties)
	if err != nil {
		return nil, fmt.Errorf("SetModuleProperties error: %s", err)
	}

	// Create property resolvers for each property
	var properties []*model.Property
	properties = append(properties, propertiesResponse...)

	return properties, nil
}

func (r *QueryResolver) ModuleConfiguration(
	ctx context.Context,
	serviceName string,
	moduleName string,
) (*model.Configuration, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	conf, err := broker.GetModuleConfiguration(serviceName, moduleName)
	if err != nil {
		return nil, fmt.Errorf("GetModuleConfiguration error: %s", err)
	}

	return conf, nil
}

func (r *QueryResolver) ModuleStatus(
	ctx context.Context,
	serviceName string,
	moduleName string,
) (*model.Status, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	stat, err := broker.GetModuleStatus(serviceName, moduleName)
	if err != nil {
		return nil, fmt.Errorf("GetModuleConfiguration error: %s", err)
	}

	return stat, nil
}

func (r *QueryResolver) ModuleSettings(
	ctx context.Context,
	serviceName string,
	moduleName string,
) ([]*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	settingsResponse, err := broker.GetModuleSettings(serviceName, moduleName)
	if err != nil {
		return nil, fmt.Errorf("GetModuleSettings error: %s", err)
	}

	settings := make([]*model.Property, len(*settingsResponse))
	for index, property := range *settingsResponse {
		settings[index] = property
	}

	return settings, nil
}

func (r *QueryResolver) ModuleJob(
	ctx context.Context,
	serviceName string,
	moduleName string,
	id string,
) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.GetModuleJob(serviceName, moduleName, id)
	if err != nil {
		return nil, fmt.Errorf("GetModuleJob error: %s", err)
	}

	return job, nil
}

func (r *QueryResolver) ModuleJobs(
	ctx context.Context,
	serviceName string,
	moduleName string,
	first *int,
	after *string,
) (*model.JobsConnection, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	jobs, err := broker.GetModuleJobs(serviceName, moduleName)
	if err != nil {
		return nil, fmt.Errorf("GetModuleJobs error: %s", err)
	}

	// TODO: get count from service
	count := len(*jobs)
	hasNext := false

	edges := make([]*model.JobsEdge, count)
	for i, element := range *jobs {
		edge := &model.JobsEdge{
			Cursor: element.ID,
			Node:   element,
		}
		edges[i] = edge
	}

	return &model.JobsConnection{
		TotalCount: count,
		Edges:      edges,
		PageInfo: &model.PageInfo{
			StartCursor: &(*jobs)[0].ID,
			EndCursor:   &(*jobs)[len(*jobs)-1].ID,
			HasNextPage: hasNext,
		},
	}, nil

}

func (r *QueryResolver) ModuleProperty(
	ctx context.Context,
	serviceName string,
	moduleName string,
	propertyName string,
) (*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	property, err := broker.GetModuleProperty(serviceName, moduleName, propertyName)
	if err != nil {
		return nil, fmt.Errorf("GetModuleProperty error: %s", err)
	}

	return property, nil
}

func (r *QueryResolver) ModuleProperties(
	ctx context.Context,
	serviceName string,
	moduleName string,
	propertyNames []*string,
) ([]*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read modules")
	}

	// Create an array of protobuf property from the entered names
	var reqProperties []*pb.Property
	for i := range propertyNames {
		reqProperties = append(reqProperties, &pb.Property{
			Key: *(propertyNames)[i],
		})
	}

	// Use the broker service
	broker := ctx.Value("brokerService").(*service.BrokerService)
	propertiesResponse, err := broker.GetModuleProperties(serviceName, moduleName, reqProperties)
	if err != nil {
		return nil, fmt.Errorf("GetModuleProperties error: %s", err)
	}

	// Create property resolvers for each property
	var properties []*model.Property
	properties = append(properties, propertiesResponse...)

	return properties, nil
}

// ## Resolvers
// TODO: remove? these are from old graphql implementation

type ModulesConnectionResolver struct {
	modules    []*model.Module
	totalCount int
	from       *string
	to         *string
}

func (r *ModulesConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *ModulesConnectionResolver) Edges() *[]*ModulesEdgeResolver {
	l := make([]*ModulesEdgeResolver, len(r.modules))
	for i := range l {
		l[i] = &ModulesEdgeResolver{
			cursor: r.modules[i].ModuleName,
			model:  r.modules[i],
		}
	}
	return &l
}

func (r *ModulesConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}

type ModulesEdgeResolver struct {
	cursor string
	model  *model.Module
}

func (r *ModulesEdgeResolver) Cursor() string {
	return r.cursor
}

func (r *ModulesEdgeResolver) Node() *ModuleResolver {
	return &ModuleResolver{module: r.model}
}
