package resolver

import (
	"errors"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/loader"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	orm "gitlab.com/plantd/plantd/internal/orm/model"
	tf "gitlab.com/plantd/plantd/internal/resolver/transformation"
	"gitlab.com/plantd/plantd/internal/service"

	"github.com/gofrs/uuid"
	"github.com/graph-gophers/graphql-go"
	"golang.org/x/net/context"
)

func (r *Resolver) Role(ctx context.Context, name string) (*model.Role, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read roles")
	}

	dbRole, err := loader.LoadRole(ctx, name)
	if err != nil {
		logger.Errorf("Graphql error : %v", err)
		return nil, err
	}

	gqlRole, err := tf.DBRoleToGQLRole(dbRole)
	if err != nil {
		logger.Errorf("Graphql error : %v", err)
		return nil, err
	}

	logger.Debugf("Retrieved role by user_id[%s] : %v", currentUser.ID.String(), *gqlRole)

	return gqlRole, nil
}

func (r *Resolver) Roles(ctx context.Context, first *int, after *string) (*model.RolesConnection, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read roles")
	}

	roles, err := ctx.Value("roleService").(*service.RoleService).List(first, after)
	if err != nil {
		return nil, err
	}
	count, err := ctx.Value("roleService").(*service.RoleService).Count()
	if err != nil {
		return nil, err
	}
	logger.Debugf("Retrieved roles by user_id[%s] :", currentUser.ID.String())
	config := ctx.Value("config").(*gcontext.MasterConfig)

	if config.Log.Debug {
		for _, role := range roles {
			logger.Debugf("%v", *role)
		}
	}

	logger.Debugf("Retrieved total roles count by user_id[%s] : %v", currentUser.ID.String(), count)

	if err != nil {
		logger.Errorf("Graphql error : %v", err)
		return nil, err
	}

	edges := make([]*model.RolesEdge, len(roles))
	for i, element := range roles {
		node, _ := tf.DBRoleToGQLRole(element)
		edge := &model.RolesEdge{
			Cursor: element.ID.String(),
			Node:   node,
		}
		edges[i] = edge
	}

	var from *string
	var to *string
	hasNext := false
	if count > 0 {
		fromID := roles[0].ID.String()
		from = &fromID
		toID := roles[len(roles)-1].ID.String()
		to = &toID
		if len(roles) < count {
			hasNext = true
		}
	}

	return &model.RolesConnection{
		TotalCount: count,
		Edges:      edges,
		PageInfo: &model.PageInfo{
			StartCursor: from,
			EndCursor:   to,
			HasNextPage: hasNext,
		},
	}, nil
}

// CreateRole returns a role if found by name in the database
func (r *Resolver) CreateRole(ctx context.Context, name string) (*model.Role, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanCreate(currentUser) {
		return nil, errors.New("not authorized to create roles")
	}

	dbRole := &orm.Role{Name: &name}
	dbRole, err := ctx.Value("roleService").(*service.RoleService).CreateRole(dbRole)
	if err != nil {
		logger.Errorf("Graphql error: %v", err)
		return nil, err
	}

	gqlRole, err := tf.DBRoleToGQLRole(dbRole)
	if err != nil {
		logger.Errorf("Graphql error : %v", err)
		return nil, err
	}

	logger.Debugf("Created role: %v", *gqlRole)
	return gqlRole, nil
}

// UpdateRole modifies a role if found by ID in the database
func (r *Resolver) UpdateRole(ctx context.Context, id string, input model.RoleInput) (*model.Role, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to create roles")
	}

	dbRole := &orm.Role{}
	if input.Name != nil {
		dbRole.Name = input.Name
	}

	roleID, err := uuid.FromString(id)
	if err != nil {
		return nil, err
	}

	dbRole.ID = roleID
	roleService := ctx.Value("roleService").(*service.RoleService)
	gqlRole, err := tf.DBRoleToGQLRole(roleService.Update(dbRole))
	if err != nil {
		return nil, err
	}

	logger.Debugf("Updated role; %v", gqlRole)

	return gqlRole, nil
}

// DeleteRole removes a role if found by ID in the database
func (r *Resolver) DeleteRole(ctx context.Context, id string) (bool, error) {
	dbRole := &orm.Role{}

	roleID, err := uuid.FromString(id)
	if err != nil {
		return false, err
	}

	dbRole.ID = roleID
	roleService := ctx.Value("roleService").(*service.RoleService)
	gqlRole, err := tf.DBRoleToGQLRole(roleService.Delete(dbRole))
	if err != nil {
		return false, err
	}

	logger.Debugf("Deleted role; %v", gqlRole)

	return true, nil
}

// ## Resolvers
// TODO: remove? these are from old graphql implementation

type RolesConnectionResolver struct {
	roles      []*orm.Role
	totalCount int
	from       *string
	to         *string
}

func (r *RolesConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *RolesConnectionResolver) Edges() *[]*RolesEdgeResolver {
	l := make([]*RolesEdgeResolver, len(r.roles))
	for i := range l {
		id := r.roles[i].ID.String()
		l[i] = &RolesEdgeResolver{
			cursor: service.EncodeCursor(&id),
			model:  r.roles[i],
		}
	}
	return &l
}

func (r *RolesConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}

type RolesEdgeResolver struct {
	cursor graphql.ID
	model  *orm.Role
}

func (r *RolesEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *RolesEdgeResolver) Node() *RoleResolver {
	return &RoleResolver{role: r.model}
}
