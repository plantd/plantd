package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"

	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
)

type JobResolver struct {
	job *model.Job
}

func (r *JobResolver) ID() graphql.ID {
	return graphql.ID(r.job.ID)
}

func (r *JobResolver) StartTime() *float64 {
	startTime := r.job.StartTime
	return startTime
}

func (r *JobResolver) FinishTime() *float64 {
	finishTime := r.job.StartTime
	return finishTime
}

func (r *JobResolver) Status() *model.JobStatus {
	return r.job.Status
}

func (r *JobResolver) Module() *ModuleResolver {
	return &ModuleResolver{r.job.Module}
}

func (r *JobResolver) Properties(args struct{ Filter *[]*string }) *[]*PropertyResolver {
	if args.Filter == nil {
		// Return all the properties
		l := make([]*PropertyResolver, len(r.job.Properties))
		for i := range l {
			l[i] = &PropertyResolver{
				property: r.job.Properties[i],
			}
		}
		return &l
	} else {
		// Return only the filtered properties
		// Create a map of the filters
		m := make(map[string]bool)
		filters := *args.Filter
		for i := range filters {
			m[*filters[i]] = true
			log.Print(*filters[i])
		}
		// Iterate through the entire properties array
		var l []*PropertyResolver
		for i := range r.job.Properties {
			if m[*r.job.Properties[i].Key] {
				l = append(l, &PropertyResolver{property: r.job.Properties[i]})
			}
		}
		return &l
	}
}
