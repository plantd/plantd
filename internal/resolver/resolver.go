package resolver

import (
	"gitlab.com/plantd/plantd/api/schema"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/orm"
	dbm "gitlab.com/plantd/plantd/internal/orm/model"
	"gitlab.com/plantd/plantd/pkg/util"

	"golang.org/x/net/context"
)

type Resolver struct {
	ORM *orm.ORM
}

// Mutation returns gql.MutationResolver implementation.
func (r *Resolver) Mutation() schema.MutationResolver {
	return &MutationResolver{r}
}

// Query returns gql.QueryResolver implementation.
func (r *Resolver) Query() schema.QueryResolver {
	return &QueryResolver{r}
}

type MutationResolver struct{ *Resolver }

type QueryResolver struct{ *Resolver }

func getCurrentUser(ctx context.Context) *dbm.User {
	logger.Info(ctx)
	return ctx.Value(util.MasterContextKeys.UserCtxKey).(*dbm.User)
}
