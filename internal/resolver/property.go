package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"
)

type PropertyResolver struct {
	property *model.Property
}

func (r *PropertyResolver) Key() *string {
	return r.property.Key
}

func (r *PropertyResolver) Value() *string {
	return r.property.Value
}
