package resolver

import (
	"errors"
	"fmt"
	"time"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/service"

	"github.com/google/uuid"
	"golang.org/x/net/context"
)

func (r *Resolver) SubmitManagerCommand(ctx context.Context, command string) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to send management commands")
	}

	var err error
	manager := ctx.Value("managerService").(*service.ManagerService)
	response := &model.Response{}

	switch command {
	case "start":
		response, err = manager.Start()
	case "stop":
		response, err = manager.Stop()
	case "restart":
		response, err = manager.Restart()
	case "status":
		err = fmt.Errorf("invalid manager command, use the managerStatus query instead")
	case "install":
		response, err = manager.Install()
	case "uninstall":
		response, err = manager.Uninstall()
	case "upgrade":
		response, err = manager.Upgrade()
	default:
		err = fmt.Errorf("unknown manager command: %s", command)
	}

	if err != nil {
		logger.Print(err)
		return nil, err
	}

	// Convert the manager response into a job
	key := "command"
	value := command
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	job := &model.Job{
		ID:        uuid.New().String(),
		Status:    &model.AllJobStatus[response.Code],
		StartTime: &currentTime,
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	return job, nil
}

func (r *Resolver) ManagerStatus(ctx context.Context) ([]*model.Property, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read manager status")
	}

	// Use the manager service
	response, err := ctx.Value("managerService").(*service.ManagerService).Status()
	if err != nil {
		logger.Errorf("Manager error: %s", err)
		return nil, err
	}

	// Create an array of property resolvers
	var properties []*model.Property
	properties = append(properties, *response...)

	return properties, nil
}
