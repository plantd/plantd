package resolver

import (
	"errors"
	"fmt"
	"strings"
	"time"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/service"

	"golang.org/x/net/context"
)

func (r *Resolver) Experiment(ctx context.Context, id string) (*model.Experiment, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read experiments")
	}

	key := "type"
	value := "camera"
	// Mock Module
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       &model.AllModuleState[0],
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	// Mock Experiment
	someExperiment := &model.Experiment{
		ID:     id,
		Status: &model.AllExperimentStatus[0],
		Modules: []*model.Module{
			cameraModule,
		},
	}

	return someExperiment, nil
}

func (r *Resolver) Experiments(ctx context.Context, serviceName string) (*model.Configuration, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read experiments")
	}

	// Use the broker service to submit the request
	broker := ctx.Value("brokerService").(*service.BrokerService)
	configuration, err := broker.GetModuleConfiguration(serviceName, "experiment")
	if err != nil {
		return nil, fmt.Errorf("GetModuleConfiguration error: %v", err)
	}

	return configuration, nil
}

func (r *Resolver) CurrentExperiment(ctx context.Context) (*model.Experiment, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read experiments")
	}

	key := "type"
	value := "camera"
	// Mock Module
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       &model.AllModuleState[0],
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	// Set some times for start, estimated duration, and time elapsed. Change these values to test.
	startTime := float64(1550182750324)
	estimatedDuration := float64(9850324)
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	timeElapsed := currentTime - startTime

	var status *model.ExperimentStatus
	if estimatedDuration <= timeElapsed {
		status = &model.AllExperimentStatus[3]
	} else {
		status = &model.AllExperimentStatus[1]
	}

	activeExperiment := &model.Experiment{
		ID:     "54321",
		Status: status,
		Modules: []*model.Module{
			cameraModule,
		},
		StartTime:         &startTime,
		EstimatedDuration: &estimatedDuration,
		TimeElapsed:       &timeElapsed,
	}

	// Normally you would search for the active experiment if there is any
	// For now, we will just send a mock object back and ensure the status is "RUNNING"
	if activeExperiment.Status != &model.AllExperimentStatus[1] {
		return nil, nil
	}

	return activeExperiment, nil
}

func (r *Resolver) SubmitExpUpdateJob(ctx context.Context, serviceName string) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to create experiments")
	}

	jobProperties := []*pb.Property{{
		Key: "data-path", Value: "/srv/data/experiments",
	}}

	// Use the broker service to submit the request
	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.ModuleSubmitJob("state", "experiment", "update-experiments", "", jobProperties)
	if err != nil {
		return nil, fmt.Errorf("ModuleSubmitJob error: %v", err)
	}

	return job, nil
}

func (r *Resolver) SubmitExpBundleJob(
	ctx context.Context,
	serviceName string,
	experimentIds []*string,
) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanCreate(currentUser) {
		return nil, errors.New("not authorized to create experiments")
	}

	var str strings.Builder
	for i := range experimentIds {
		// Append the experiment ID and comma delimit it
		str.WriteString(*experimentIds[i])
		str.WriteString(",")
	}

	var jobProperties []*pb.Property
	jobProperties = append(jobProperties, &pb.Property{
		Key:   "exp-id",
		Value: strings.TrimSuffix(str.String(), ","),
	})
	logger.Print("Bundle experiment IDs: " + jobProperties[0].GetValue())

	// Use the broker service to submit the request
	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.ModuleSubmitJob(serviceName, "experiment", "bundle-experiments", jobProperties[0].GetValue(), jobProperties)
	if err != nil {
		return nil, fmt.Errorf("ModuleSubmitJob error: %v", err)
	}

	return job, nil
}

func (r *MutationResolver) SubmitExpDeleteJob(
	ctx context.Context,
	serviceName string,
	experimentIds []*string,
) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanDelete(currentUser) {
		return nil, errors.New("not authorized to delete experiments")
	}

	var str strings.Builder
	for i := range experimentIds {
		// Append the experiment ID and comma delimit it
		str.WriteString(*experimentIds[i])
		str.WriteString(",")
	}

	var jobProperties []*pb.Property
	jobProperties = append(jobProperties, &pb.Property{
		Key:   "exp-id",
		Value: strings.TrimSuffix(str.String(), ","),
	})
	logger.Print("Deleting experiment IDs: " + jobProperties[0].GetValue())

	// Use the broker service to submit the request
	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.ModuleSubmitJob(
		serviceName,
		"experiment",
		"delete-experiments",
		jobProperties[0].GetValue(),
		jobProperties,
	)
	if err != nil {
		return nil, fmt.Errorf("ModuleSubmitJob error: %v", err)
	}

	return job, nil
}
