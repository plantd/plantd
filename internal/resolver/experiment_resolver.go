package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"

	graphql "github.com/graph-gophers/graphql-go"
	log "github.com/sirupsen/logrus"
)

type ExperimentResolver struct {
	exp *model.Experiment
}

func (r *ExperimentResolver) ID() graphql.ID {
	return graphql.ID(r.exp.ID)
}

func (r *ExperimentResolver) Status() *model.ExperimentStatus {
	status := r.exp.Status
	if !status.IsValid() {
		return nil
	}
	return status
}

func (r *ExperimentResolver) Name() *string {
	name := r.exp.Name
	if *name == "" {
		return nil
	}
	return name
}

func (r *ExperimentResolver) StartTime() *float64 {
	startTime := r.exp.StartTime
	return startTime
}

func (r *ExperimentResolver) EstimatedDuration() *float64 {
	estimatedDuration := r.exp.EstimatedDuration
	return estimatedDuration
}

func (r *ExperimentResolver) TimeElapsed() *float64 {
	timeElapsed := r.exp.TimeElapsed
	return timeElapsed
}

func (r *ExperimentResolver) Modules() *[]*ModuleResolver {
	l := make([]*ModuleResolver, len(r.exp.Modules))
	log.Println("hit")

	for i := range l {
		l[i] = &ModuleResolver{
			module: r.exp.Modules[i],
		}
	}

	return &l
}

func (r *ExperimentResolver) Properties() *[]*PropertyResolver {
	l := make([]*PropertyResolver, len(r.exp.Properties))

	for i := range l {
		l[i] = &PropertyResolver{
			property: r.exp.Properties[i],
		}
	}

	return &l
}
