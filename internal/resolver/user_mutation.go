package resolver

import (
	"github.com/gofrs/uuid"
	"gitlab.com/plantd/plantd/internal/model"
	orm "gitlab.com/plantd/plantd/internal/orm/model"
	"gitlab.com/plantd/plantd/internal/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

//func (r *mutationResolver) CreateUser(ctx context.Context, email string, password string) (*model.User, error) {
//	panic("not implemented")
//}

func (r *Resolver) CreateUser(ctx context.Context, args *struct {
	Email    string
	Password string
}) (*UserResolver, error) {
	user := &orm.User{
		Email:    args.Email,
		Password: args.Password,
		//IPAddress: *ctx.Value("requester_ip").(*string),
	}

	userService := ctx.Value("userService").(*service.UserService)
	user, err := userService.Create(user)
	if err != nil {
		log.Errorf("Graphql error: %v", err)
		return nil, err
	}

	log.Debugf("Created user: %v", *user)

	return &UserResolver{user}, nil
}

func (r *Resolver) UpdateUser(ctx context.Context, args *struct {
	ID    string
	Input model.UserInput
}) (*UserResolver, error) {
	user := &orm.User{}

	// TODO: create a transformer to go input to type
	if args.Input.Email != nil {
		user.Email = *args.Input.Email
	}

	if args.Input.Password != nil {
		user.Password = *args.Input.Password
	}

	//if args.Input.Roles != nil {
	//	for _, role := range args.Input.Roles {
	//		user.Roles = append(user.Roles, &orm.Role{Name: *role.Name})
	//	}
	//	log.Debugf("Adding roles: %+v", user.Roles)
	//}

	id, err := uuid.FromString(args.ID)
	if err != nil {
		return nil, err
	}

	user.ID = id
	userService := ctx.Value("userService").(*service.UserService)
	user = userService.Update(user)
	log.Debugf("Updated user; %v", *user)

	return &UserResolver{user}, nil
}

func (r *Resolver) DeleteUser(ctx context.Context, args *struct {
	ID string
}) (bool, error) {
	user := &orm.User{}

	id, err := uuid.FromString(args.ID)
	if err != nil {
		return false, err
	}

	user.ID = id
	userService := ctx.Value("userService").(*service.UserService)
	user = userService.Delete(user)
	log.Debugf("Deleted user; %v", *user)

	return true, nil
}
