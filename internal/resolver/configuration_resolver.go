package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"

	graphql "github.com/graph-gophers/graphql-go"
)

type ConfigurationResolver struct {
	conf *model.Configuration
}

func (r *ConfigurationResolver) ID() graphql.ID {
	return graphql.ID(r.conf.ID)
}

func (r *ConfigurationResolver) Name() *string {
	return r.conf.Name
}

func (r *ConfigurationResolver) Namespace() *string {
	return r.conf.Namespace
}

func (r *ConfigurationResolver) Objects() *[]*ObjectResolver {
	l := make([]*ObjectResolver, len(r.conf.Objects))

	for i := range l {
		l[i] = &ObjectResolver{
			object: r.conf.Objects[i],
		}
	}

	return &l
}

func (r *ConfigurationResolver) Properties() *[]*PropertyResolver {
	l := make([]*PropertyResolver, len(r.conf.Properties))

	for i := range l {
		l[i] = &PropertyResolver{
			property: r.conf.Properties[i],
		}
	}

	return &l
}
