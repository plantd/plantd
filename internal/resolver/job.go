package resolver

import (
	"errors"
	"fmt"
	"time"

	"github.com/graph-gophers/graphql-go"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/service"

	"golang.org/x/net/context"
)

func (r *QueryResolver) Job(
	ctx context.Context,
	serviceName string,
	moduleName string,
	id string,
) (*model.Job, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read jobs")
	}

	// MOCK MODULES
	properties := []string{"type", "camera", "FPS", "2", "button", "true"}
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       &model.AllModuleState[0],
		Properties: []*model.Property{{
			Key:   &properties[0],
			Value: &properties[1],
		}, {
			Key:   &properties[2],
			Value: &properties[3],
		}, {
			Key:   &properties[4],
			Value: &properties[5],
		}},
	}

	key := "somekey"
	value := "some value"
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	testJob := &model.Job{
		ID:        "1",
		Status:    &model.AllJobStatus[1],
		StartTime: &currentTime,
		Module:    cameraModule,
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	return testJob, nil
}

func (r *QueryResolver) Jobs(
	ctx context.Context,
	serviceName string,
	moduleName string,
	first *int32,
	after *string,
) (*model.JobsConnection, error) {
	currentUser := getCurrentUser(ctx)
	if !service.CanRead(currentUser) {
		return nil, errors.New("not authorized to read jobs")
	}

	// MOCK MODULES
	key := "type"
	value := "camera"
	cameraModule := &model.Module{
		ModuleName:  "acquire-genicam",
		ServiceName: "acquire",
		State:       &model.AllModuleState[0],
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	key = "somekey"
	value = "some value"
	currentTime := float64(time.Now().UnixNano() / int64(time.Millisecond))
	job1 := &model.Job{
		ID:        "1",
		Status:    &model.AllJobStatus[1],
		StartTime: &currentTime,
		Module:    cameraModule,
		Properties: []*model.Property{{
			Key:   &key,
			Value: &value,
		}},
	}

	job2 := &model.Job{
		ID:     "2",
		Status: &model.AllJobStatus[0],
		Module: cameraModule,
	}

	jobs := []*model.Job{
		job1,
		job2,
	}

	edges := make([]*model.JobsEdge, len(jobs))
	for i, element := range jobs {
		edge := &model.JobsEdge{
			Cursor: element.ID,
			Node:   element,
		}
		edges[i] = edge
	}

	from := &(jobs[0].ID)
	to := &(jobs[len(jobs)-1].ID)
	hasNext := false

	return &model.JobsConnection{
		TotalCount: len(jobs),
		Edges:      edges,
		PageInfo: &model.PageInfo{
			StartCursor: from,
			EndCursor:   to,
			HasNextPage: hasNext,
		},
	}, nil
}

func (r *MutationResolver) SubmitJob(
	ctx context.Context,
	serviceName string,
	moduleName string,
	action string,
	actionValue *string,
	properties []*model.PropertyInput,
) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanCreate(currentUser) {
		return nil, errors.New("not authorized to create jobs")
	}

	// Parse the properties into a pb property array
	var jobProperties []*pb.Property
	if properties != nil {
		jobProperties = ConvertProperties(properties)
	}

	// Parse the optional value
	var value string
	if actionValue != nil {
		value = *actionValue
	}

	// Use the broker service to submit the request
	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.ModuleSubmitJob(serviceName, moduleName, action, value, jobProperties)
	if err != nil {
		return nil, fmt.Errorf("ModuleSubmitJob error: %v", err)
	}

	return job, nil
}

func (r *MutationResolver) CancelJob(
	ctx context.Context,
	serviceName string,
	moduleName string,
	jobID string,
) (*model.Job, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	currentUser := getCurrentUser(ctx)
	if !service.CanDelete(currentUser) {
		return nil, errors.New("not authorized to delete jobs")
	}

	broker := ctx.Value("brokerService").(*service.BrokerService)
	job, err := broker.ModuleCancelJob(serviceName, moduleName, jobID)
	if err != nil {
		return nil, fmt.Errorf("ModuleCancelJob error: %s", err)
	}

	return job, nil
}

// ## Resolvers
// TODO: remove? these are from old graphql implementation

type JobsConnectionResolver struct {
	jobs       []*model.Job
	totalCount int
	from       *string
	to         *string
}

func (r *JobsConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *JobsConnectionResolver) Edges() *[]*JobsEdgeResolver {
	l := make([]*JobsEdgeResolver, len(r.jobs))
	for i := range l {
		l[i] = &JobsEdgeResolver{
			cursor: service.EncodeCursor(&(r.jobs[i].ID)),
			model:  r.jobs[i],
		}
	}
	return &l
}

func (r *JobsConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}

type JobsEdgeResolver struct {
	cursor graphql.ID
	model  *model.Job
}

func (r *JobsEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *JobsEdgeResolver) Node() *JobResolver {
	return &JobResolver{job: r.model}
}
