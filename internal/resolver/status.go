package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"
)

type StatusResolver struct {
	status *model.Status
}

func (r *StatusResolver) Enabled() *bool {
	return r.status.Enabled
}

func (r *StatusResolver) Loaded() *bool {
	return r.status.Loaded
}

func (r *StatusResolver) Active() *bool {
	return r.status.Active
}

func (r *StatusResolver) Details() *[]*PropertyResolver {
	l := make([]*PropertyResolver, len(r.status.Details))
	for _, detail := range r.status.Details {
		l = append(l, &PropertyResolver{&model.Property{Key: detail.Key, Value: detail.Value}})
	}
	return &l
}
