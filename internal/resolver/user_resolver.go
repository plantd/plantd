package resolver

import (
	"gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/graph-gophers/graphql-go"
)

// UserResolver is used to resolve a user model
type UserResolver struct {
	user *model.User
}

// ID returns the GraphQL value for the user model
func (r *UserResolver) ID() graphql.ID {
	return graphql.ID(r.user.ID.String())
}

// Email returns the email for the user model
func (r *UserResolver) Email() *string {
	return &r.user.Email
}

// Password returns the password for the user model
func (r *UserResolver) Password() *string {
	maskedPassword := "********"
	return &maskedPassword
}

// IPAddress returns the IP address the user model registered with
//func (r *UserResolver) IPAddress() *string {
//	return &r.user.IPAddress
//}

// CreatedAt returns the GraphQL value for the date the user registered
func (r *UserResolver) CreatedAt() (*graphql.Time, error) {
	if r.user.CreatedAt == nil {
		return nil, nil
	}

	// TODO: remove or create error correctly
	return &graphql.Time{Time: *r.user.CreatedAt}, nil
}

// Roles returns the roles the user model is associated with
//func (r *UserResolver) Roles() *[]*RoleResolver {
//	l := make([]*RoleResolver, len(r.user.Roles))
//
//	for i := range l {
//		l[i] = &RoleResolver{
//			role: r.user.Roles[i],
//		}
//	}
//
//	return &l
//}
