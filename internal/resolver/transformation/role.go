package transformation

import (
	"errors"

	gql "gitlab.com/plantd/plantd/internal/model"
	dbm "gitlab.com/plantd/plantd/internal/orm/model"
)

// DBRoleToGQLRole transforms [role] db input to gql type
func DBRoleToGQLRole(i *dbm.Role) (o *gql.Role, err error) {
	o = &gql.Role{
		ID:          i.ID.String(),
		Name:        i.Name,
		Description: i.Description,
		CreatedAt:   i.CreatedAt,
		UpdatedAt:   i.UpdatedAt,
	}
	return o, err
}

// GQLInputRoleToDBRole transforms [role] gql input to db model
func GQLInputRoleToDBRole(i *gql.RoleInput, update bool, ids ...string) (o *dbm.Role, err error) {
	o = &dbm.Role{
		Name:        i.Name,
		Description: i.Description,
	}
	if i.Name == nil && !update {
		return nil, errors.New("field [name] is required")
	}
	return o, err
}
