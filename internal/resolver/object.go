package resolver

import (
	"gitlab.com/plantd/plantd/internal/model"

	graphql "github.com/graph-gophers/graphql-go"
)

type ObjectResolver struct {
	object *model.Object
}

func (r *ObjectResolver) ID() graphql.ID {
	return graphql.ID(r.object.ID)
}

func (r *ObjectResolver) Name() *string {
	return r.object.Name
}

func (r *ObjectResolver) Type() *string {
	return r.object.Type
}

func (r *ObjectResolver) Objects() *[]*ObjectResolver {
	l := make([]*ObjectResolver, len(r.object.Objects))

	for i := range l {
		l[i] = &ObjectResolver{
			object: r.object.Objects[i],
		}
	}

	return &l
}

func (r *ObjectResolver) Properties() *[]*PropertyResolver {
	l := make([]*PropertyResolver, len(r.object.Properties))

	for i := range l {
		l[i] = &PropertyResolver{
			property: r.object.Properties[i],
		}
	}

	return &l
}
