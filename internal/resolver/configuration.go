package resolver

import (
	"errors"
	"fmt"
	"strings"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/model"
	"gitlab.com/plantd/plantd/internal/service"
	"gitlab.com/plantd/plantd/internal/service/configure"
	"gitlab.com/plantd/plantd/internal/service/user"

	"github.com/graph-gophers/graphql-go"
	"golang.org/x/net/context"
)

func (r *Resolver) Configuration(ctx context.Context, id string, namespace string) (*model.Configuration, error) {
	currentUser := getCurrentUser(ctx)
	//configureService := service.GetConfigureServiceInstance()

	//conf, err := configureService.FindByID(id, namespace)
	conf, err := configure.FindByID(id, namespace)
	if err != nil {
		logger.Debugf("GraphQL error : %v", err)
		return nil, err
	}

	logger.Debugf("Retrieved configuration by user_id[%s] : %v", currentUser.ID, *conf)

	return conf, nil
}

func (r *Resolver) Configurations(
	ctx context.Context,
	namespace string,
	first *int,
	after *string,
) (*model.ConfigurationsConnection, error) {
	//if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
	//	return nil, errors.New(gcontext.CredentialsError)
	//}

	// TODO: check if user has permissions
	currentUser := getCurrentUser(ctx)

	configurations, err := configure.List(namespace, first, after)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("GraphQL error : %v", err))
	}
	count, err := configure.Count(namespace)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("GraphQL error : %v", err))
	}

	logger.Debugf("Retrieved configurations by user_id[%s]", currentUser.ID)
	logger.Debugf("Retrieved %d configurations by user_id[%s]", count, currentUser.ID)
	for _, conf := range configurations {
		logger.Debugf("%v", *conf)
	}

	edges := make([]*model.ConfigurationsEdge, len(configurations))
	for i, element := range configurations {
		edge := &model.ConfigurationsEdge{
			Cursor: element.ID,
			Node:   element,
		}
		edges[i] = edge
	}

	// Handle the case where there are no configurations
	var from *string
	var to *string
	hasNext := false
	if count > 0 {
		from = &(configurations[0].ID)
		to = &(configurations[len(configurations)-1].ID)
		if len(configurations) < count {
			hasNext = true
		}
	}

	return &model.ConfigurationsConnection{
		TotalCount: count,
		Edges:      edges,
		PageInfo: &model.PageInfo{
			StartCursor: from,
			EndCursor:   to,
			HasNextPage: hasNext,
		},
	}, nil
}

// Converts an array of propertyInput to an array of protobuf properties
func ConvertProperties(input []*model.PropertyInput) []*pb.Property {
	var properties []*pb.Property
	for _, property := range input {
		properties = append(properties, &pb.Property{
			Key:   property.Key,
			Value: property.Value,
		})
	}
	return properties
}

// ConvertObjects converts an array of objectInput to an array of protobuf objects
func ConvertObjects(input []*model.ObjectInput) []*pb.Object {
	if len(input) < 1 {
		return nil
	}
	var objects []*pb.Object
	for _, object := range input {
		writeObject := &pb.Object{}
		if object.ID != nil {
			writeObject.Id = *object.ID
		}
		if object.Name != nil {
			writeObject.Name = *object.Name
		}
		if object.Objects != nil {
			writeObject.Objects = ConvertObjects(object.Objects)
		}
		if object.Properties != nil {
			writeObject.Properties = ConvertProperties(object.Properties)
		}
		objects = append(objects, writeObject)
	}
	return objects
}

func (r *Resolver) CreateConfiguration(
	ctx context.Context,
	conf model.ConfigurationInput,
) (*model.Configuration, error) {
	// TODO: check if user has permissions
	currentUser := getCurrentUser(ctx)

	// Check if the namespace is valid
	namespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(conf.Namespace)]
	if !ok {
		return nil, errors.New("invalid namespace provided")
	}

	// Defaults for optional arguments
	var id string
	if conf.ID != nil {
		id = *conf.ID
	}
	var name string
	if conf.Name != nil {
		name = *conf.Name
	}
	var objects []*pb.Object
	if conf.Objects != nil {
		objects = ConvertObjects(conf.Objects)
	}

	// Add the rest of the properties
	var properties []*pb.Property
	if conf.Properties != nil {
		properties = ConvertProperties(conf.Properties)
	}

	// Add the author property
	u, err := user.FindByID(r.ORM, currentUser.ID.String())
	if err == nil && u.Email != "" {
		properties = append(properties, &pb.Property{
			Key:   "author",
			Value: u.Email,
		})
	}

	// Create the a protobuf configuration
	request := &pb.Configuration{
		Id:         id,
		Name:       name,
		Namespace:  pb.Configuration_Namespace(namespace),
		Objects:    objects,
		Properties: properties,
	}

	// Send the configuration to the configure service
	config, err := configure.CreateConfiguration(request)
	if err != nil {
		logger.Print(err)
		return nil, err
	}

	return config, nil
}

// TODO: remove input.ID
func (r *Resolver) UpdateConfiguration(
	ctx context.Context,
	id string,
	namespace string,
	input model.ConfigurationInput,
) (*model.Configuration, error) {
	// TODO: check if user has permissions
	//currentUser := getCurrentUser(ctx)

	// Check if the new namespace is valid
	targetNamespace, ok := pb.Configuration_Namespace_value[strings.ToUpper(input.Namespace)]
	if !ok {
		return nil, errors.New("Target namespace provided is invalid")
	}

	// Defaults for optional arguments
	var confId string
	if input.ID != nil {
		confId = *input.ID
	}
	var name string
	if input.Name != nil {
		name = *input.Name
	}
	var objects []*pb.Object
	if input.Objects != nil {
		objects = ConvertObjects(input.Objects)
	}
	var properties []*pb.Property
	if input.Properties != nil {
		properties = ConvertProperties(input.Properties)
	}

	// Create the a protobuf configuration
	request := &pb.Configuration{
		Id:         confId,
		Name:       name,
		Namespace:  pb.Configuration_Namespace(targetNamespace),
		Objects:    objects,
		Properties: properties,
	}

	// Send the configuration update request to the configure service
	config, err := configure.UpdateConfiguration(id, namespace, request)
	if err != nil {
		logger.Print(err)
		return nil, err
	}

	return config, nil
}

func (r *Resolver) DeleteConfiguration(
	ctx context.Context,
	id string,
	namespace string,
) (*model.Configuration, error) {
	// TODO: check if user has permissions
	//currentUser := getCurrentUser(ctx)

	config, err := configure.DeleteConfiguration(id, namespace)
	if err != nil {
		logger.Print(err)
		return nil, err
	}

	return config, nil
}

// TODO: drop? these get replaced by models generated with gqlgen
type ConfigurationsConnectionResolver struct {
	configurations []*model.Configuration
	totalCount     int
	from           *string
	to             *string
}

func (r *ConfigurationsConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *ConfigurationsConnectionResolver) Edges() *[]*ConfigurationsEdgeResolver {
	l := make([]*ConfigurationsEdgeResolver, len(r.configurations))
	for i := range l {
		l[i] = &ConfigurationsEdgeResolver{
			cursor: service.EncodeCursor(&(r.configurations[i].ID)),
			model:  r.configurations[i],
		}
	}
	return &l
}

func (r *ConfigurationsConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}

// TODO: drop? these get replaced by models generated with gqlgen
type ConfigurationsEdgeResolver struct {
	cursor graphql.ID
	model  *model.Configuration
}

func (r *ConfigurationsEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *ConfigurationsEdgeResolver) Node() *ConfigurationResolver {
	return &ConfigurationResolver{conf: r.model}
}
