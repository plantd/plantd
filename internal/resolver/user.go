package resolver

import (
	"errors"

	"gitlab.com/plantd/plantd/internal/model"
	orm "gitlab.com/plantd/plantd/internal/orm/model"
	"gitlab.com/plantd/plantd/internal/service"
	"gitlab.com/plantd/plantd/internal/service/user"

	"github.com/graph-gophers/graphql-go"
	"golang.org/x/net/context"
)

// CreateUser creates a record
func (r *MutationResolver) CreateUser(ctx context.Context, input model.UserInput) (*model.User, error) {
	currentUser := getCurrentUser(ctx)
	if !user.CanCreate(currentUser) {
		return nil, errors.New("not authorized to create users")
	}
	return user.CreateUpdate(r.ORM, input, false)
}

// UpdateUser updates a record
func (r *MutationResolver) UpdateUser(ctx context.Context, id string, input model.UserInput) (*model.User, error) {
	currentUser := getCurrentUser(ctx)
	if !user.CanUpdate(currentUser) {
		return nil, errors.New("not authorized to update the user")
	}
	return user.CreateUpdate(r.ORM, input, true, id)
}

// DeleteUser deletes a record
func (r *MutationResolver) DeleteUser(ctx context.Context, id string) (bool, error) {
	currentUser := getCurrentUser(ctx)
	if !user.CanDelete(currentUser) {
		return false, errors.New("not authorized to delete users")
	}
	return user.Delete(r.ORM, id)
}

func (r *QueryResolver) User(ctx context.Context, email string) (*model.User, error) {
	currentUser := getCurrentUser(ctx)
	if !user.CanRead(currentUser) {
		return nil, errors.New("not authorized to read users")
	}

	u, err := user.FindByEmail(r.ORM, email)
	if err != nil {
		return nil, err
	}

	return u, nil
}

// Users lists records
func (r *QueryResolver) Users(ctx context.Context, first *int, after *string) (*model.UsersConnection, error) {
	currentUser := getCurrentUser(ctx)
	if !user.CanRead(currentUser) {
		return nil, errors.New("not authorized to read users")
	}

	// TODO: check first and after and handle if not nil

	return user.List(r.ORM, nil)
}

// ## Resolvers
// TODO: remove? these are from old graphql implementation

type UsersConnectionResolver struct {
	users      []*orm.User
	totalCount int
	from       *string
	to         *string
}

func (r *UsersConnectionResolver) TotalCount() int32 {
	return int32(r.totalCount)
}

func (r *UsersConnectionResolver) Edges() *[]*UsersEdgeResolver {
	l := make([]*UsersEdgeResolver, len(r.users))
	for i := range l {
		id := r.users[i].ID.String()
		l[i] = &UsersEdgeResolver{
			cursor: service.EncodeCursor(&id),
			model:  r.users[i],
		}
	}
	return &l
}

func (r *UsersConnectionResolver) PageInfo() *PageInfoResolver {
	return &PageInfoResolver{
		startCursor: service.EncodeCursor(r.from),
		endCursor:   service.EncodeCursor(r.to),
		hasNextPage: false,
	}
}

type UsersEdgeResolver struct {
	cursor graphql.ID
	model  *orm.User
}

func (r *UsersEdgeResolver) Cursor() graphql.ID {
	return r.cursor
}

func (r *UsersEdgeResolver) Node() *UserResolver {
	return &UserResolver{user: r.model}
}
