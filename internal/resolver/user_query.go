package resolver

import (
	"errors"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/loader"
	"gitlab.com/plantd/plantd/internal/service"

	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

// User resolves a user using the associated data loader
func (r *Resolver) User(ctx context.Context, args struct {
	Email string
}) (*UserResolver, error) {
	// Without using dataloader:
	//user, err := ctx.Value("userService").(*service.UserService).FindByEmail(args.Email)
	userID := ctx.Value("user_id").(*string)
	user, err := loader.LoadUser(ctx, args.Email)

	if err != nil {
		log.Errorf("GraphQL error : %v", err)
		return nil, err
	}

	log.Debugf("Retrieved user by user_id[%s] : %v", *userID, *user)

	return &UserResolver{user}, nil
}

// Users resolves a list of users using the associated data loader
func (r *Resolver) Users(ctx context.Context, args struct {
	First *int32
	After *string
}) (*UsersConnectionResolver, error) {
	if isAuthorized := ctx.Value("is_authorized").(bool); !isAuthorized {
		return nil, errors.New(gcontext.CredentialsError)
	}

	userID := ctx.Value("user_id").(*string)

	users, err := ctx.Value("userService").(*service.UserService).List(args.First, args.After)
	if err != nil {
		return nil, err
	}
	count, err := ctx.Value("userService").(*service.UserService).Count()
	if err != nil {
		return nil, err
	}
	log.Debugf("Retrieved users by user_id[%s] :", *userID)
	config := ctx.Value("config").(*gcontext.MasterConfig)

	if config.Log.Debug {
		for _, user := range users {
			log.Debugf("%v", *user)
		}
	}

	log.Debugf("Retrieved total users count by user_id[%s] : %v", *userID, count)

	if err != nil {
		log.Errorf("GraphQL error : %v", err)
		return nil, err
	}

	fromID := users[0].ID.String()
	toID := users[len(users)-1].ID.String()

	return &UsersConnectionResolver{
		users:      users,
		totalCount: count,
		from:       &fromID,
		to:         &toID,
	}, nil
}
