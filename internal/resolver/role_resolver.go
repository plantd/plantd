package resolver

import (
	orm "gitlab.com/plantd/plantd/internal/orm/model"

	"github.com/graph-gophers/graphql-go"
)

type RoleResolver struct {
	role *orm.Role
}

func (r *RoleResolver) ID() graphql.ID {
	return graphql.ID(r.role.ID.String())
}

func (r *RoleResolver) Name() *string {
	return r.role.Name
}

func (r *RoleResolver) CreatedAt() (*graphql.Time, error) {
	if r.role.CreatedAt == nil {
		return nil, nil
	}

	return &graphql.Time{Time: *r.role.CreatedAt}, nil
}
