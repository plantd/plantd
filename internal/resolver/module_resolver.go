package resolver

import (
	"fmt"

	"gitlab.com/plantd/plantd/internal/loader"
	"gitlab.com/plantd/plantd/internal/model"

	"golang.org/x/net/context"
)

type ModuleResolver struct {
	module *model.Module
}

type NewModuleArgs struct {
	Module *model.Module
	Name   string
}

func NewModule(ctx context.Context, args NewModuleArgs) (*ModuleResolver, error) {
	var module *model.Module
	var err error

	switch {
	//case args.Module.ModuleName != "":
	//module = args.Module
	case args.Name != "":
		module, err = loader.LoadModule(ctx, args.Name)
	default:
		err = fmt.Errorf("Unable to resolve module")
	}

	if err != nil {
		return nil, err
	}

	return &ModuleResolver{module: module}, nil
}

func (r *ModuleResolver) ModuleName() string {
	return r.module.ModuleName
}

func (r *ModuleResolver) ServiceName() string {
	return r.module.ServiceName
}

func (r *ModuleResolver) State() *model.ModuleState {
	return r.module.State
}

func (r *ModuleResolver) Properties(args struct{ Filter *[]*string }) *[]*PropertyResolver {
	if args.Filter == nil {
		// Return all the properties
		l := make([]*PropertyResolver, len(r.module.Properties))
		for i := range l {
			l[i] = &PropertyResolver{
				property: r.module.Properties[i],
			}
		}
		return &l
	} else {
		// Return only the filtered properties
		// Create a map of the filters
		m := make(map[string]bool)
		filters := *args.Filter
		for i := range filters {
			m[*filters[i]] = true
			//log.Print("Filtering: ", *filters[i])
		}
		// Iterate through the entire properties array
		var l []*PropertyResolver
		for i := range r.module.Properties {
			if m[*r.module.Properties[i].Key] {
				l = append(l, &PropertyResolver{property: r.module.Properties[i]})
			}
		}
		return &l
	}
}

func (r *ModuleResolver) Configuration() *ConfigurationResolver {
	return &ConfigurationResolver{
		conf: r.module.Configuration,
	}
}

func (r *ModuleResolver) Status() *StatusResolver {
	return &StatusResolver{
		status: r.module.Status,
	}
}

func (r *ModuleResolver) Jobs() *[]*JobResolver {
	// Return all of the jobs
	l := make([]*JobResolver, len(r.module.Jobs))
	for i := range l {
		l[i] = &JobResolver{
			job: r.module.Jobs[i],
		}
	}
	return &l
}
