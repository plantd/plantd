package handler

import (
	"time"

	gql "gitlab.com/plantd/plantd/api/schema"
	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/loader"
	"gitlab.com/plantd/plantd/internal/orm"
	"gitlab.com/plantd/plantd/internal/resolver"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/apollotracing"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/lru"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"github.com/graph-gophers/graphql-go"
)

type GraphQL struct {
	Schema  *graphql.Schema
	Loaders loader.Collection
	Logger  logger
}

//func (h *GraphQL) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	var params struct {
//		Query         string                 `json:"query"`
//		OperationName string                 `json:"operationName"`
//		Variables     map[string]interface{} `json:"variables"`
//	}
//
//	if err := json.NewDecoder(r.Body).Decode(&params); err != nil {
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//
//	ctx := h.Loaders.Attach(r.Context())
//
//	response := h.Schema.Exec(ctx, params.Query, params.OperationName, params.Variables)
//	responseJSON, err := json.Marshal(response)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//
//	w.Header().Set("Content-Type", "application/json")
//	if _, err = w.Write(responseJSON); err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//}

// logger defines an interface with a single method.
type logger interface {
	Printf(fmt string, values ...interface{})
}

// GraphqlHandler defines the GQLGen GraphQL server handler
func GraphqlHandler(config *context.MasterConfig, orm *orm.ORM) gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	c := gql.Config{
		Resolvers: &resolver.Resolver{
			ORM: orm, // pass in the ORM instance in the resolvers to be used
		},
		Directives: gql.DirectiveRoot{},
		Complexity: gql.ComplexityRoot{},
	}

	// setProjectComplexity(&c)
	// h := handler.GraphQL(gql.NewExecutableSchema(c), handler.ComplexityLimit(gqlConfig.ComplexityLimit))

	//h := handler.NewDefaultServer(gql.NewExecutableSchema(c))
	//
	//return func(c *gin.Context) {
	//	h.ServeHTTP(c.Writer, c.Request)
	//}

	srv := handler.New(gql.NewExecutableSchema(c))
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{})
	srv.Use(extension.FixedComplexityLimit(config.GraphQL.ComplexityLimit))
	if config.GraphQL.IsIntrospectionEnabled {
		srv.Use(extension.Introspection{})
	}
	srv.Use(apollotracing.Tracer{})
	srv.Use(extension.AutomaticPersistedQuery{
		Cache: lru.New(100),
	})

	return func(c *gin.Context) {
		// h.ServeHTTP(c.Writer, c.Request)
		srv.ServeHTTP(c.Writer, c.Request)
	}
}

// PlaygroundHandler defines a handler to expose the Playground
func PlaygroundHandler(path string) gin.HandlerFunc {
	h := playground.Handler("Plantd GraphQL", path)
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

//func setProjectComplexity(c *gql.Config) {
//	countComplexity := func(childComplexity int, limit *int, offset *int) int {
//		return *limit
//	}
//	fixedComplexity := func(childComplexity int) int {
//		return 100
//	}
//	// c.Complexity.Query.Users = func(childComplexity int, id *string, filters []*models.QueryFilter, limit *int, offset *int, orderBy *string, sortDirection *string) int {
//	// 	return *limit
//	// }
//	c.Complexity.User.CreatedBy = fixedComplexity
//	c.Complexity.User.UpdatedBy = fixedComplexity
//	c.Complexity.User.Profiles = countComplexity
//}
