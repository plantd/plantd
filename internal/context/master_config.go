package context

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// MasterConfig provides configuration information for the master application
type MasterConfig struct {
	App           string            `mapstructure:"app"`
	Host          string            `mapstructure:"host"`
	Port          int               `mapstructure:"port"`
	URISchema     string            `mapstructure:"uri-schema"`
	Version       string            `mapstructure:"version"`
	SessionSecret string            `mapstructure:"session-secret"`
	GraphQL       gql               `mapstructure:"graphql"`
	Auth          auth              `mapstructure:"auth"`
	Broker        map[string]client `mapstructure:"broker"`
	Configure     configure         `mapstructure:"configure"`
	DB            database          `mapstructure:"db"`
	Log           logging           `mapstructure:"log"`
	Manager       client            `mapstructure:"manager"`
}

// LoadMasterConfig loads a configuration file from disk
func LoadMasterConfig(path string) (*MasterConfig, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	file := os.Getenv("PLANTD_MASTER_CONFIG")
	if file == "" {
		config.SetConfigName("master")
		config.AddConfigPath(path)
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
		config.AddConfigPath("/etc/plantd")
	} else {
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "hcl") ||
			strings.HasSuffix(base, "toml") ||
			strings.HasSuffix(base, "conf") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
		}
		config.SetConfigName(base)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_MASTER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c MasterConfig

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	// initialize logging
	switch c.Log.Formatter {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	level, err := logrus.ParseLevel(c.Log.Level)
	if err == nil {
		logrus.SetLevel(level)
	}

	logrus.Debug("Configuration Location: ", config.ConfigFileUsed())

	return &c, nil
}

// ListenEndpoint builds the endpoint string (host + port)
func (c *MasterConfig) ListenEndpoint() string {
	if c.Port == 80 {
		return c.Host
	}
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

// VersionedEndpoint builds the endpoint string (host + port + version)
func (c *MasterConfig) VersionedEndpoint(path string) string {
	return "/" + c.Version + path
}

// SchemaVersionedEndpoint builds the schema endpoint string (schema + host + port + version)
func (c *MasterConfig) SchemaVersionedEndpoint(path string) string {
	if c.Port == 80 {
		return c.URISchema + c.Host + "/" + c.Version + path
	}
	return fmt.Sprintf("%s%s:%d/%s%s", c.URISchema, c.Host, c.Port, c.Version, path)
}
