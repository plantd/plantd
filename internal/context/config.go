package context

import (
	"time"
)

// Config provides a common configuration file interface
type Config interface {
	Has(setting string) bool
}

type provider struct {
	Provider  string   `mapstructure:"provider"`
	ClientKey string   `mapstructure:"client-key"`
	Secret    string   `mapstructure:"secret"`
	Domain    string   `mapstructure:"domain"` // If needed, like with auth0
	Scopes    []string `mapstructure:"scopes"`
}

type auth struct {
	JWTSecret    string        `mapstructure:"jwt-secret"`
	JWTAlgorithm string        `mapstructure:"jwt-algorithm"`
	JWTExpireIn  time.Duration `mapstructure:"jwt-expire-in"`
	Providers    []provider    `mapstructure:"providers"`
}

// GQLConfig defines the configuration for the GQL Server
type gql struct {
	Path                   string `mapstructure:"path"`
	PlaygroundPath         string `mapstructure:"playground-path"`
	IsPlaygroundEnabled    bool   `mapstructure:"playground-enabled"`
	IsIntrospectionEnabled bool   `mapstructure:"introspection-enabled"`
	ComplexityLimit        int    `mapstructure:"complexity-limit"`
}

type configure struct {
	CACert string `mapstructure:"cacert"`
	Cert   string `mapstructure:"cert"`
	Host   string `mapstructure:"host"`
	Key    string `mapstructure:"key"`
	Port   int    `mapstructure:"port"`
	UseTLS bool   `mapstructure:"tls"`
}

type client struct {
	Host string `mapstructure:"host"`
	Port int    `mapstructure:"port"`
}

type database struct {
	AutoMigrate bool   `mapstructure:"auto-migrate"`
	Dialect     string `mapstructure:"dialect"`
	DSN         string `mapstructure:"dsn"`
	Host        string `mapstructure:"host"`
	LogMode     bool   `mapstructure:"log-mode"`
	Name        string `mapstructure:"name"`
	Password    string `mapstructure:"password"`
	Port        int    `mapstructure:"port"`
	SeedDB      bool   `mapstructure:"seed"`
	User        string `mapstructure:"user"`
}

type logging struct {
	Debug     bool   `mapstructure:"debug"`
	Formatter string `mapstructure:"formatter"`
	Level     string `mapstructure:"level"`
}

type unit struct {
	Endpoint string `mapstructure:"endpoint"`
	RPCBind  string `mapstructure:"rpc-bind"`
	RPCPort  int    `mapstructure:"rpc-port"`
}
