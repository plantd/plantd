package context

import (
	"gitlab.com/plantd/go-zapi/mdp"
)

// ConnectClient connects a broker service client at the configured endpoint
func ConnectClient(config *BrokerConfig) (*mdp.Client, error) {
	client, err := mdp.NewClient(config.ClientEndpoint)
	if err != nil {
		return nil, err
	}

	return client, nil
}
