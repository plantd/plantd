package context

import (
	"context"
	"fmt"
	"time"

	_ "gitlab.com/plantd/plantd/internal/configure/migrations"
	"gitlab.com/plantd/plantd/internal/logger"

	migrate "github.com/xakep666/mongo-migrate"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func OpenMongoDB(config *ConfigureConfig) (*mongo.Database, error) {
	logger.Print("Database is connecting to", config.DB.Host, config.DB.Port)

	uri := fmt.Sprintf("mongodb://%s:%d", config.DB.Host, config.DB.Port)
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		return nil, fmt.Errorf("Couldn't connect to mongo: %v", err)
	}

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()

	if err := client.Connect(ctx); err != nil {
		logger.Print("Retry database connection in 30 seconds...")
		time.Sleep(time.Duration(30) * time.Second)
		return OpenMongoDB(config)
	}

	logger.Debugf("Using database %s", config.DB.Name)
	db := client.Database(config.DB.Name)

	// Run database migrations
	// TODO: add an environment variable for this like there is for gorm
	migrate.SetDatabase(db)
	if err := migrate.Up(migrate.AllAvailable); err != nil {
		return nil, err
	}

	logger.Print("Database is connected")

	return db, nil
}
