package context

import (
	"errors"
	"fmt"
	"time"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/connectivity"
)

// BrokerClient establishes a new gRPC client connection with a broker endpoint
func BrokerClient(host string, port int) (pb.EndpointClient, error) {
	log.WithFields(log.Fields{"host": host, "port": port}).Info("Connecting to broker service")

	ready := false
	nRetry := 5
	addr := fmt.Sprintf("%s:%d", host, port)

	// Create an insecure connection
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("could not dial: %s: %s", addr, err)
	}

	for i := 0; i < nRetry; i++ {
		time.Sleep(100 * time.Millisecond)
		if conn.GetState() == connectivity.Ready {
			ready = true
			break
		}
	}

	if !ready {
		err := errors.New("Connection failure")
		log.WithFields(log.Fields{"host": host, "port": port}).Error(err)
		return nil, err
	}

	// Initialie the client connection
	return pb.NewEndpointClient(conn), nil
}
