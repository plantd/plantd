package context

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

// ConfigureClient creates a connection to the configure gRPC service
func ConfigureClient(config *MasterConfig) (pb.ConfigureEndpointClient, error) {
	log.Println("Connecting to configuration service...")

	var client pb.ConfigureEndpointClient
	addr := fmt.Sprintf("%s:%d", config.Configure.Host, config.Configure.Port)

	// TODO: handle insecure case
	if config.Configure.UseTLS {
		log.Println("Using TLS credentials for configuration service")

		// Load the client certificates
		certificate, err := tls.LoadX509KeyPair(config.Configure.Cert, config.Configure.Key)
		if err != nil {
			return nil, fmt.Errorf("could not load client key pair: %s", err)
		}

		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(config.Configure.CACert)
		if err != nil {
			return nil, fmt.Errorf("could not read ca certificate: %s", err)
		}

		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			return nil, errors.New("failed to append ca certs")
		}

		creds := credentials.NewTLS(&tls.Config{
			ServerName:   addr,
			Certificates: []tls.Certificate{certificate},
			RootCAs:      certPool,
		})

		// Create a connection with the TLS credentials
		conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(creds))
		if err != nil {
			return nil, fmt.Errorf("could not dial %s: %s", addr, err)
		}

		//defer conn.Close()

		// Initialize the client connection
		client = pb.NewConfigureEndpointClient(conn)
	} else {
		log.Println("Using an insecure connection to the configuration service")

		// Create an insecure connection
		conn, err := grpc.Dial(addr, grpc.WithInsecure())
		if err != nil {
			return nil, fmt.Errorf("could not dial %s: %s", addr, err)
		}

		// FIXME: need to figure out how to handle this correctly
		//defer conn.Close()

		// Initialize the client connection
		client = pb.NewConfigureEndpointClient(conn)
	}

	return client, nil
}
