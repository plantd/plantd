package context

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type BrokerConfig struct {
	Env               string          `mapstructure:"env"`
	Endpoint          string          `mapstructure:"endpoint"`
	ClientEndpoint    string          `mapstructure:"client-endpoint"`
	HeartbeatLiveness int             `mapstructure:"heartbeat-liveness"`
	HeartbeatInterval int             `mapstructure:"heartbeat-interval"`
	Configure         configure       `mapstructure:"configure"`
	Units             map[string]unit `mapstructure:"units"`
	Log               logging         `mapstructure:"log"`
}

func LoadBrokerConfig() (*BrokerConfig, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()

	file := os.Getenv("PLANTD_BROKER_CONFIG")
	if file == "" {
		config.SetConfigName("broker")
		config.AddConfigPath(".")
		config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))
		config.AddConfigPath("/etc/plantd")
	} else {
		base := filepath.Base(file)
		if strings.HasSuffix(base, "yaml") ||
			strings.HasSuffix(base, "json") ||
			strings.HasSuffix(base, "hcl") ||
			strings.HasSuffix(base, "toml") ||
			strings.HasSuffix(base, "conf") {
			// strip the file type for viper
			parts := strings.Split(filepath.Base(file), ".")
			base = strings.Join(parts[:len(parts)-1], ".")
		}
		config.SetConfigName(base)
		config.AddConfigPath(filepath.Dir(file))
	}

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_BROKER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c BrokerConfig

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	// initialize logging
	switch c.Log.Formatter {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	level, err := logrus.ParseLevel(c.Log.Level)
	if err == nil {
		logrus.SetLevel(level)
	}

	return &c, nil
}
