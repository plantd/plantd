package context

import (
	"fmt"
	"time"

	//"gitlab.com/plantd/plantd/internal/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
	//"gopkg.in/gormigrate.v1"
)

// OpenDB establishes a connection to the database
func OpenDB(config *MasterConfig) (*gorm.DB, error) {
	log.Printf("Connecting to database '%s' on %s:%d", config.DB.Name, config.DB.Host, config.DB.Port)
	db, err := gorm.Open("postgres",
		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			config.DB.Host, config.DB.Port, config.DB.User, config.DB.Password, config.DB.Name),
	)

	if err != nil {
		panic(err.Error())
	}

	if err = db.DB().Ping(); err != nil {
		log.Fatalf("Unable to connect to db: %s \n", err)
		log.Println("Retry database connection in 5 seconds... ")
		time.Sleep(time.Duration(5) * time.Second)
		return OpenDB(config)
	}

	log.Println("Database is connected")

	return db, nil
}
