package context

import (
	"fmt"

	pb "gitlab.com/plantd/plantctl/proto"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

// ManagerClient creates a client connection to the plantctl manager service
func ManagerClient(config *MasterConfig) (pb.ManagerClient, error) {
	addr := fmt.Sprintf("%s:%d", config.Manager.Host, config.Manager.Port)
	log.Print("Connecting to Manager server at ", addr)

	// Create an insecure connection
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("could not dial: %s: %s", addr, err)
	}

	// Initialie the client connection
	return pb.NewManagerClient(conn), nil
}
