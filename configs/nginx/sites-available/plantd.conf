map $http_upgrade $connection_upgrade {
  default upgrade;
  '' close;
}

upstream websocket {
  server 127.0.0.1:9090;
}

server {
  listen 80;
  server_name _;
  index index.html index.htm;

  location / {
    proxy_pass            http://web:3002/;
    proxy_read_timeout    90s;
    proxy_connect_timeout 90s;
    proxy_send_timeout    90s;
    proxy_set_header      Host $host;
    proxy_set_header      X-Real-IP $remote_addr;
    proxy_set_header      X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header      Proxy "";
  }

  location /images/ {
    root /srv/data;
  }

  location /experiments/ {
    root /srv/data;
  }

  location /api/ {
    rewrite               ^/query/(.*)$ /query/$1 break;
    proxy_pass            http://master:3001/;
    proxy_redirect        off;
    proxy_read_timeout    90s;
    proxy_connect_timeout 90s;
    proxy_send_timeout    90s;
    proxy_set_header      Host $host;
    proxy_set_header      X-Real-IP $remote_addr;
    proxy_set_header      X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header      Proxy "";
  }

  location /admin/control/ {
    rewrite               ^/(.*)$ /$1 break;
    proxy_pass            http://websocket/;
    proxy_redirect        off;
    proxy_read_timeout    90s;
    proxy_connect_timeout 90s;
    proxy_send_timeout    90s;
    proxy_set_header      Host $host;
    proxy_set_header      X-Real-IP $remote_addr;
    proxy_set_header      X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header      Proxy "";
    proxy_set_header      Upgrade $http_upgrade;
    proxy_set_header      Connection $connection_upgrade;
    proxy_set_header      Origin http://$host;
    gzip off;
  }

  location /admin/grafana/ {
    #rewrite               ^/(.*)$ /$1 break;
    proxy_pass             http://grafana:3000/;
    #proxy_redirect        off;
    #proxy_read_timeout    90s;
    #proxy_connect_timeout 90s;
    #proxy_send_timeout    90s;
    #proxy_set_header      Host $host;
    #proxy_set_header      X-Real-IP $remote_addr;
    #proxy_set_header      X-Forwarded-For $proxy_add_x_forwarded_for;
    #proxy_set_header      Proxy "";
  }
}
