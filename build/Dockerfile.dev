# vim:filetype=Dockerfile
# Base image for development that enables hot-reload
#
# Build with:
#
#  docker build -t registry.gitlab.com/plantd/plantd:dev -f build/Dockerfile.dev .

FROM golang:1.13-alpine
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

# Install dependencies
RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
        bash \
        binutils \
    && apk add --no-cache --virtual .build-deps \
        gcc \
        g++ \
        git \
        make \
        pkgconfig \
        alpine-sdk \
        musl-dev \
        util-linux-dev \
        czmq-dev \
        zeromq-dev \
        libsodium-dev

WORKDIR /go/src/gitlab.com/plantd/plantd

COPY . .

# Run the build once to pull dependencies
RUN make setup \
    && make schema \
    && make

# Install development dependencies to debug and live reload api
RUN go get github.com/go-delve/delve/cmd/dlv \
    && go get github.com/githubnemo/CompileDaemon

# Provide information about exposed container ports
EXPOSE 2345
