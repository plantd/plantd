BUILD_DEPS += configure/build
STATIC_DEPS += configure/static
IMAGE_DEPS += configure/image
CONTAINER_DEPS += configure/container
INSTALL_DEPS += configure/image
UNINSTALL_DEPS += configure/uninstall

configure/build: ; $(info $N Building plantd-configure...)
	@go build -o target/plantd-configure -ldflags "-X main.Version=$(VERSION)" ./cmd/configure

configure/static: ; $(info $N Build static plantd-configure binary...)
	@./tools/static configure

# DEPRECATED: should just use build script for this
configure/image: configure/static ; $(info $N Building plantd-configure image...)
	@docker build -t $(REGISTRY)/configure:$(REGISTRY_TAG) -f ./build/configure/Dockerfile .

# DEPRECATED: should just use build script for this
configure/container: configure/image ; $(info $N Running plantd-configure container...)
	@docker run -p "4000:4000" $(REGISTRY)/configure:$(REGISTRY_TAG)

# FIXME: this assumes configure branch and os/arch, consider using build output
.PHONY: configure/install
configure/install: ; $(info $N Installing plantd-configure service...)
	@install -Dm 755 target/plantd-configure "$(DESTDIR)/bin/plantd-configure"
	@install -Dm 644 configs/configure.yaml "$(CONFDIR)/plantd/configure.yaml"
	@install -Dm 644 init/plantd-configure.service "$(SYSTEMDDIR)/system/plantd-configure.service"

.PHONY: configure/uninstall
configure/uninstall: ; $(info $N Uninstalling plantd configure service...)
	@rm "$(DESTDIR)/bin/plantd-configure"
	@rm "$(SYSTEMDDIR)/system/plantd-configure.service"
