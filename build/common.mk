PROJECT=plantd
REGISTRY=registry.gitlab.com/plantd/$(PROJECT)
REGISTRY_TAG=v2
DESTDIR=/usr
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
N := $(shell printf "  \033[35;1m●\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')
VERSION := $(shell egrep "var VERSION" pkg/version.go | sed 's/.*"\(.*\)"/\1/')

# Various targets to be added to
BUILD_DEPS :=
STATIC_DEPS :=
IMAGE_DEPS :=
CONTAINER_DEPS :=
INSTALL_DEPS :=
UNINSTALL_DEPS :=

# These seem like a nice idea to have around
# FIXME: currently don't work

log_success = (echo "\x1B[32m>> $1\x1B[39m")
log_error = (>&2 echo "\x1B[31m>> $1\x1B[39m" && exit 1)

#log-success:
	#$(call log_success, "testing log success message")

#log-error:
	#$(call log_error, "testing log error message")

#test-var:
	#@[ "$(AWS_PROFILE)" ] || $(call log_error, "AWS_PROFILE not set!")
