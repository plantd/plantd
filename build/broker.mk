BUILD_DEPS += broker/build
STATIC_DEPS += broker/static
IMAGE_DEPS += broker/image
CONTAINER_DEPS += broker/container
INSTALL_DEPS += broker/image
UNINSTALL_DEPS += broker/uninstall

broker/build: ; $(info $N Building plantd-broker...)
	@go build -o target/plantd-broker -ldflags "-X main.Version=$(VERSION)" ./cmd/broker

broker/static: ; $(info $N Build static plantd-broker binary...)
	@./tools/static broker

# DEPRECATED: should just use build script for this
broker/image: broker/static ; $(info $N Building plantd-broker image...)
	@docker build -t $(REGISTRY)/broker:$(REGISTRY_TAG) -f ./build/broker/Dockerfile .

# DEPRECATED: should just use build script for this
broker/container: broker/image ; $(info $N Running plantd-broker container...)
	@docker run -p "7200:7200" -p "4041-4045:4041-4045" $(REGISTRY)/broker:$(REGISTRY_TAG)

# FIXME: this assumes broker branch and os/arch, consider using build output
.PHONY: broker/install
broker/install: ; $(info $N Installing plantd-broker service...)
	@install -Dm 755 target/plantd-broker "$(DESTDIR)/bin/plantd-broker"
	@install -Dm 644 configs/broker.yaml "$(CONFDIR)/plantd/broker.yaml"
	@install -Dm 644 init/plantd-broker.service "$(SYSTEMDDIR)/system/plantd-broker.service"

.PHONY: broker/uninstall
broker/uninstall: ; $(info $N Uninstalling plantd broker service...)
	@rm "$(DESTDIR)/bin/plantd-broker"
	@rm "$(SYSTEMDDIR)/system/plantd-broker.service"
