BUILD_DEPS += events/build
STATIC_DEPS += events/static
IMAGE_DEPS += events/image
CONTAINER_DEPS += events/container
INSTALL_DEPS += events/image
UNINSTALL_DEPS += events/uninstall

events/build: ; $(info $N Building plantd-events...)
	@go build -o target/plantd-events -ldflags "-X main.Version=$(VERSION)" ./cmd/events

events/static: ; $(info $N Build static plantd-events binary...)
	@./tools/static events

# DEPRECATED: should just use build script for this
events/image: events/static ; $(info $N Building plantd-events image...)
	@docker build -t $(REGISTRY)/events:$(REGISTRY_TAG) -f ./build/events/Dockerfile .

# DEPRECATED: should just use build script for this
events/container: events/image ; $(info $N Running plantd-events container...)
	@docker run -p "10000-10001:10000-10001" $(REGISTRY)/events:$(REGISTRY_TAG)

# FIXME: this assumes events branch and os/arch, consider using build output
.PHONY: events/install
events/install: ; $(info $N Installing plantd-events service...)
	@install -Dm 755 target/plantd-events "$(DESTDIR)/bin/plantd-events"
	@install -Dm 644 configs/events.yaml "$(CONFDIR)/plantd/events.yaml"
	@install -Dm 644 init/plantd-events.service "$(SYSTEMDDIR)/system/plantd-events.service"

.PHONY: events/uninstall
events/uninstall: ; $(info $N Uninstalling plantd events service...)
	@rm "$(DESTDIR)/bin/plantd-events"
	@rm "$(SYSTEMDDIR)/system/plantd-events.service"
