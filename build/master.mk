BUILD_DEPS += master/build
STATIC_DEPS += master/static
IMAGE_DEPS += master/image
CONTAINER_DEPS += master/container
INSTALL_DEPS += master/image
UNINSTALL_DEPS += master/uninstall

master/build: ; $(info $N Build plantd-master...)
	@go build -o target/plantd-master -ldflags "-X main.Version=$(VERSION)" ./cmd/master

master/static: ; $(info $N Build static plantd-master binary...)
	@./tools/static master

# DEPRECATED: should just use build script for this
master/image: master/static ; $(info $N Building plantd-master image...)
	@docker build -t $(REGISTRY)/master:$(REGISTRY_TAG) -f ./build/master/Dockerfile .

# DEPRECATED: should just use build script for this
master/container: master/image ; $(info $N Running plantd-master container...)
	@docker run -p "3000:3000" $(REGISTRY)/master:$(REGISTRY_TAG)

# FIXME: this assumes master branch and os/arch, consider using build output
.PHONY: master/install
master/install: ; $(info $N Installing plantd-master service...)
	@install -Dm 755 target/plantd-master "$(DESTDIR)/bin/plantd-master"
	@install -Dm 644 configs/master.yaml "$(CONFDIR)/plantd/master.yaml"
	@install -Dm 644 init/plantd-master.service "$(SYSTEMDDIR)/system/plantd-master.service"

.PHONY: master/uninstall
master/uninstall: ; $(info $N Uninstalling plantd master service...)
	@rm "$(DESTDIR)/bin/plantd-master"
	@rm "$(SYSTEMDDIR)/system/plantd-master.service"
