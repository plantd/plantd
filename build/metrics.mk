BUILD_DEPS += metrics/build
STATIC_DEPS += metrics/static
IMAGE_DEPS += metrics/image
CONTAINER_DEPS += metrics/container
INSTALL_DEPS += metrics/image
UNINSTALL_DEPS += metrics/uninstall

metrics/build: ; $(info $N Building plantd-metrics...)
	@go build -o target/plantd-metrics -ldflags "-X main.Version=$(VERSION)" ./cmd/metrics

metrics/static: ; $(info $N Build static plantd-metrics binary...)
	@./tools/static metrics

# DEPRECATED: should just use build script for this
metrics/image: metrics/static ; $(info $N Building plantd-metrics image...)
	@docker build -t $(REGISTRY)/metrics:$(REGISTRY_TAG) -f ./build/metrics/Dockerfile .

# DEPRECATED: should just use build script for this
metrics/container: metrics/image ; $(info $N Running plantd-metrics container...)
	@docker run -p "11000-11001:11000-11001" $(REGISTRY)/metrics:$(REGISTRY_TAG)

# FIXME: this assumes metrics branch and os/arch, consider using build output
.PHONY: metrics/install
metrics/install: ; $(info $N Installing plantd-metrics service...)
	@install -Dm 755 target/plantd-metrics "$(DESTDIR)/bin/plantd-metrics"
	@install -Dm 644 configs/metrics.yaml "$(CONFDIR)/plantd/metrics.yaml"
	@install -Dm 644 init/plantd-metrics.service "$(SYSTEMDDIR)/system/plantd-metrics.service"

.PHONY: metrics/uninstall
metrics/uninstall: ; $(info $N Uninstalling plantd metrics service...)
	@rm "$(DESTDIR)/bin/plantd-metrics"
	@rm "$(SYSTEMDDIR)/system/plantd-metrics.service"
