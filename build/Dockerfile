# Base image:
FROM golang:1.13.4
MAINTAINER Geoff Johnson <geoff.jay@gmail.com>

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go get -u golang.org/x/lint

# Add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-9 main" | \
    tee -a /etc/apt/sources.list

# Install clang from LLVM repository
RUN apt-get update \
    && apt-get install -y --no-install-recommends clang-9

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-9" | tee -a ${set_clang} && chmod a+x ${set_clang}

# Setup build dependencies
RUN apt-get install -y --no-install-recommends libzmq3-dev libczmq-dev

# Install python to use with CI
RUN apt-get install -y --no-install-recommends python3 python3-pip

# Clean up
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
