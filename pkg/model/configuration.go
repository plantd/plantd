package model

import (
	pb "gitlab.com/plantd/go-plantd/proto/v1"

	//"go.mongodb.org/mongo-driver/bson/objectid"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Configuration struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	Name       string             `bson:"name"          json:"name"`
	Namespace  string             `bson:"namespace"     json:"namespace"`
	Objects    []*Object          `bson:"objects"       json:"objects"`
	Properties []*Property        `bson:"properties"    json:"properties"`
}

func (conf Configuration) ToProtobuf() (*pb.Configuration, error) {
	// Convert object children to the equivalent protobuf message
	var objects []*pb.Object
	for _, obj := range conf.Objects {
		obj_, err := obj.ToProtobuf()
		if err != nil {
			return nil, err
		}
		objects = append(objects, obj_)
	}

	// Convert property list to the equivalent protobuf message
	var properties []*pb.Property
	for _, prop := range conf.Properties {
		_prop, err := prop.ToProtobuf()
		if err != nil {
			return nil, err
		}
		properties = append(properties, _prop)
	}

	return &pb.Configuration{
		Id:         conf.ID.Hex(),
		Name:       conf.Name,
		Namespace:  pb.Configuration_Namespace(pb.Configuration_Namespace_value[conf.Namespace]),
		Objects:    objects,
		Properties: properties,
	}, nil
}
