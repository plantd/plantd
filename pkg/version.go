package version

import (
	"strconv"
	"strings"
)

// VERSION represents the platform version
var VERSION = "0.3.0"

// TODO: GetMajorVersion
func GetMajorVersion() int {
	n, _ := strconv.Atoi(strings.Split(VERSION, ".")[0])
	return n
}

// TODO: GetMinorVersion
func GetMinorVersion() int {
	n, _ := strconv.Atoi(strings.Split(VERSION, ".")[1])
	return n
}

// TODO: GetMicroVersion
func GetMicroVersion() int {
	n, _ := strconv.Atoi(strings.Split(VERSION, ".")[2])
	return n
}
