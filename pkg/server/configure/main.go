package configure

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net"

	pb "gitlab.com/plantd/go-plantd/proto/v1"

	pconf "gitlab.com/plantd/plantd/internal/configure"
	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"

	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

func Run(config *gcontext.ConfigureConfig, db *mongo.Database) {
	ctx := context.Background()
	configure := pconf.NewConfigure(db)

	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "configure", configure)

	// create the channel to listen on
	addr := fmt.Sprintf("%s:%d", config.Server.Host, config.Server.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Errorf("failed to listen: %s", err)
	}

	var opts []grpc.ServerOption

	if config.Server.UseTLS {

		logger.Info("using TLS credentials")

		// Load the certificates from disk
		certificate, err := tls.LoadX509KeyPair(config.Server.Cert, config.Server.Key)
		if err != nil {
			logger.Errorf("could not load server key pair: %s", err)
		}

		// Create a certificate pool from the certificate authority
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(config.Server.CACert)
		if err != nil {
			logger.Errorf("could not read ca certificate: %s", err)
		}

		// Append the client certificates from the CA
		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			logger.Error("failed to append client certs")
		}

		// Create the TLS credentials
		creds := credentials.NewTLS(&tls.Config{
			ClientAuth:   tls.RequireAndVerifyClientCert,
			Certificates: []tls.Certificate{certificate},
			ClientCAs:    certPool,
		})

		opts = append(opts, grpc.Creds(creds))
	}

	srv := grpc.NewServer(opts...)
	pb.RegisterConfigureEndpointServer(srv, configure)
	reflection.Register(srv)
	if err := srv.Serve(lis); err != nil {
		logger.Errorf("failed to serve: %v", err)
	}
}
