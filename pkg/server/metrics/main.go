package metrics

import (
	"gitlab.com/plantd/go-zapi/bus"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
)

func Run(config *context.MetricsConfig) {
	metrics := bus.NewBus(
		"metric",
		"metric",
		config.Backend,
		config.Frontend,
		config.Capture,
	)
	done := make(chan bool)

	go metrics.Run(done)

	<-done

	logger.Info("closing bus example")
}
