package events

import (
	"gitlab.com/plantd/go-zapi/bus"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
)

func Run(config *context.EventsConfig) {
	events := bus.NewBus(
		"event",
		"event",
		config.Backend,
		config.Frontend,
		config.Capture,
	)
	done := make(chan bool)

	go events.Run(done)

	<-done

	logger.Info("closing bus example")
}
