package broker

import (
	"gitlab.com/plantd/go-zapi/mdp"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/service"
	"gitlab.com/plantd/plantd/internal/task"

	"github.com/sirupsen/logrus"
	"golang.org/x/net/context"
)

func Run(config *gcontext.BrokerConfig) {
	var clients = make(map[string]*mdp.Client)
	var endpoints = make(map[string]*service.Endpoint)
	var tasks = make(map[string]*task.Task)

	for name, unit := range config.Units {
		logger.WithFields(logrus.Fields{
			"unit":     name,
			"endpoint": unit.Endpoint,
		}).Info("adding service unit")

		var err error

		// ZeroMQ clients
		clients[name], err = gcontext.ConnectClient(config)
		if err != nil {
			logger.WithFields(logrus.Fields{
				"unit": name,
				"err":  err,
			}).Error("failed to create mdp client")
		}

		// gRPC services
		endpoints[name] = service.NewEndpoint(name, clients[name], config)

		// message proxies
		tasks[name], err = task.NewTask(name, endpoints[name], config)
		if err != nil {
			logger.WithFields(logrus.Fields{
				"unit": name,
				"err":  err,
			}).Error("failed to create task")
		}
	}

	ctx := context.Background()

	// Include services in context
	ctx = context.WithValue(ctx, "config", config)
	for unit, endpoint := range endpoints {
		ctx = context.WithValue(ctx, unit, endpoint)
	}

	logger.Info("starting tasks")
	for _, t := range tasks {
		go t.Start()
	}

	broker, _ := mdp.NewBroker()
	if err := broker.Bind(config.Endpoint); err != nil {
		logger.WithFields(logrus.Fields{"err": err}).Error("failed to bind broker")
	}

	done := make(chan bool, 1)
	go broker.Run(done)

	<-done

	logger.Info("broker shutting down...")
}
