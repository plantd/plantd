package master

import (
	"gitlab.com/plantd/plantd/internal/context"

	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/auth0"
	"github.com/markbates/goth/providers/google"
)

// InitalizeAuthProviders does just that, with Goth providers
func InitalizeAuthProviders(config *context.MasterConfig) error {
	providers := []goth.Provider{}

	// Initialize Goth providers
	for _, p := range config.Auth.Providers {
		switch p.Provider {
		case "google":
			providers = append(providers, google.New(p.ClientKey, p.Secret,
				config.SchemaVersionedEndpoint("/auth/"+p.Provider+"/callback"),
				p.Scopes...))
		case "auth0":
			providers = append(providers, auth0.New(p.ClientKey, p.Secret,
				config.SchemaVersionedEndpoint("/auth/"+p.Provider+"/callback"),
				p.Domain, p.Scopes...))
		}
	}

	goth.UseProviders(providers...)

	return nil
}
