package route

import (
	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/handler"
	"gitlab.com/plantd/plantd/internal/middleware"
	"gitlab.com/plantd/plantd/internal/orm"

	"github.com/gin-gonic/gin"
)

// Ping routes
func Ping(config *context.MasterConfig, r *gin.Engine, orm *orm.ORM) error {
	// Simple keep-alive/ping handler
	r.GET(config.VersionedEndpoint("/ping"), handler.Ping())
	r.GET(config.VersionedEndpoint("/secure-ping"),
		middleware.Auth(config.VersionedEndpoint("/secure-ping"), config, orm), handler.Ping())
	return nil
}
