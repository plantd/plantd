package route

import (
	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/handler"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/middleware"
	"gitlab.com/plantd/plantd/internal/orm"

	"github.com/gin-gonic/gin"
)

// GraphQL routes
func GraphQL(config *context.MasterConfig, r *gin.Engine, orm *orm.ORM) error {
	// GraphQL paths
	gqlPath := config.VersionedEndpoint(config.GraphQL.Path)
	pgqlPath := config.GraphQL.PlaygroundPath
	g := r.Group(gqlPath)

	// GraphQL handler
	g.POST("", middleware.Auth(g.BasePath(), config, orm), handler.GraphqlHandler(config, orm))
	//g.POST("", handler.GraphqlHandler(config, orm))
	logger.Info("GraphQL @ ", gqlPath)

	// Playground handler
	if config.GraphQL.IsPlaygroundEnabled {
		logger.Info("GraphQL Playground @ ", g.BasePath()+pgqlPath)
		g.GET(pgqlPath, handler.PlaygroundHandler(g.BasePath()))
	}

	return nil
}
