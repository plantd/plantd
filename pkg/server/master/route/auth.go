package route

import (
	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/handler/auth"
	"gitlab.com/plantd/plantd/internal/orm"

	"github.com/gin-gonic/gin"
)

// Auth routes
func Auth(cfg *context.MasterConfig, r *gin.Engine, orm *orm.ORM) error {
	// OAuth handlers
	g := r.Group(cfg.VersionedEndpoint("/auth"))
	g.GET("/:provider", auth.Begin())
	g.GET("/:provider/callback", auth.Callback(cfg, orm))
	// g.GET(:provider/refresh", auth.Refresh(cfg, orm))
	return nil
}
