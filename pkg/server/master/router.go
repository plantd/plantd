package master

import (
	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/orm"
	"gitlab.com/plantd/plantd/pkg/server/master/route"

	"github.com/gin-gonic/gin"
)

// RegisterRoutes register the routes for the server
func RegisterRoutes(config *context.MasterConfig, r *gin.Engine, orm *orm.ORM) (err error) {
	// Auth routes
	if err = route.Auth(config, r, orm); err != nil {
		return err
	}

	// Serve GraphiQL
	// TODO: probably don't need this anymore after switch to playground
	//r.StaticFile("/graphql", "web/graphiql.html")

	// GraphQL server routes
	if err = route.GraphQL(config, r, orm); err != nil {
		return err
	}

	// Miscellaneous routes
	if err = route.Ping(config, r, orm); err != nil {
		return err
	}

	return err
}
