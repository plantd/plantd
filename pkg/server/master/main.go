package master

import (
	"net/http"
	"time"

	"gitlab.com/plantd/plantd/internal/service/configure"

	"gitlab.com/plantd/plantd/internal/middleware"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/orm"
	"gitlab.com/plantd/plantd/internal/service"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/ginrus"
	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
)

// Run launches the server
func Run(config *gcontext.MasterConfig, orm *orm.ORM) {
	// Set configuration values here
	// TODO: move into configuration
	var (
		readHeaderTimeout = 1 * time.Second
		writeTimeout      = 10 * time.Second
		idleTimeout       = 90 * time.Second
		maxHeaderBytes    = http.DefaultMaxHeaderBytes
	)

	// Create an Manager service connection
	managerClient, err := gcontext.ManagerClient(config)
	if err != nil {
		logger.Fatalf("Unable to connect to manager service: %s\n", err)
	}

	// Initialize the services
	configure.Setup(config)

	ctx := context.Background()
	roleService := service.NewRoleService(orm.DB)
	userService := service.NewUserService(orm.DB, roleService)
	//authService := service.NewAuthService(config)
	managerService := service.NewManagerService(managerClient)
	brokerService := service.NewBrokerService(config)

	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "roleService", roleService)
	ctx = context.WithValue(ctx, "userService", userService)
	//ctx = context.WithValue(ctx, "authService", authService)
	ctx = context.WithValue(ctx, "managerService", managerService)
	ctx = context.WithValue(ctx, "brokerService", brokerService)

	router := initRouter()

	// Initialize the Auth providers
	if err := InitalizeAuthProviders(config); err != nil {
		logger.Fatal("Failed to initialize:", err)
	}

	// Routes and Handlers
	if err := RegisterRoutes(config, router, orm); err != nil {
		logger.Fatal("Failed to register routes:", err)
	}

	// Inform the user where the server is listening
	logger.Info("Running @ " + config.SchemaVersionedEndpoint(""))

	addr := config.ListenEndpoint()

	// Configure HTTP server
	s := &http.Server{
		Addr:              addr,
		Handler:           router,
		ReadHeaderTimeout: readHeaderTimeout,
		WriteTimeout:      writeTimeout,
		IdleTimeout:       idleTimeout,
		MaxHeaderBytes:    maxHeaderBytes,
	}

	// Run the server
	logger.Infof("Listen for requests on %s", s.Addr)
	if err := s.ListenAndServe(); err != nil {
		logger.Fatal("server.ListenAndServer:", err)
	}
	logger.Info("Shutting down")
}

func initRouter() *gin.Engine {
	router := gin.Default()

	// Add middleware
	router.Use(ginrus.Ginrus(logger.GetLogger(), time.RFC3339, true))
	router.Use(middleware.Context())
	router.Use(cors.New(cors.Config{
		// TODO: add config for CORS_ORIGIN
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"POST", "GET", "OPTIONS", "PUT", "DELETE"},
		AllowHeaders: []string{
			"Content-Type",
			"Accept-Encoding",
			"X-CSRF-Token",
			"Authorization",
			"Accept",
			"Origin",
			"Cache-Control",
			"X-Requested-With",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	return router
}
