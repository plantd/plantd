package master

import (
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"

	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/orm"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/util"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRouter(t *testing.T) {
	router := gin.Default()

	// Load configuration file
	configDir := filepath.Join(util.RootDir(), "configs")
	config, err := gcontext.LoadMasterConfig(configDir)
	if err != nil {
		t.Fatalf("Unable to load configuration: %s\n", err)
	}

	// Connect to the database
	db, err := orm.Factory(config)
	if err != nil {
		logger.Panicf("Unable to connect to db: %s\n", err)
	}
	defer func() {
		if err := db.DB.Close(); err != nil {
			logger.Fatalf("Unable to close db: %s\n", err)
		}
	}()

	if err := RegisterRoutes(config, router, db); err != nil {
		t.Fatal(err)
	}

	t.Run("ping", func(t *testing.T) {
		w := httptest.NewRecorder()
		url := config.SchemaVersionedEndpoint("/ping")
		req, _ := http.NewRequest("GET", url, nil)
		router.ServeHTTP(w, req)

		assert.Equal(t, 200, w.Code)
		assert.Equal(t, "OK", w.Body.String())
	})
}
