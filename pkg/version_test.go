package version

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVersion(t *testing.T) {
	assert.Equal(t, 0, GetMajorVersion(), "The major version should match")
	assert.Equal(t, 3, GetMinorVersion(), "The minor version should match")
	assert.Equal(t, 0, GetMicroVersion(), "The micro version should match")
}
