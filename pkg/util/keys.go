package util

// ContextKeys holds the context keys through the project
type ContextKeys struct {
	ProviderCtxKey       ContextKey // Provider in Auth
	UserCtxKey           ContextKey // User db object in Auth
	GinContextKey        ContextKey
	ConfigCtxKey         ContextKey
	LoggerCtxKey         ContextKey
	RoleServiceCtxKey    ContextKey
	UserServiceCtxKey    ContextKey // XXX: redundant?
	AuthServiceCtxKey    ContextKey // XXX: redundant?
	ManagerServiceCtxKey ContextKey
	BrokerServiceCtxKey  ContextKey
}

var (
	// MasterContextKeys the project's context keys
	MasterContextKeys = ContextKeys{
		ProviderCtxKey:       "provider",
		UserCtxKey:           "auth-user",
		GinContextKey:        "gin",
		ConfigCtxKey:         "config",
		LoggerCtxKey:         "logger",
		RoleServiceCtxKey:    "role-service",
		UserServiceCtxKey:    "user-service",
		AuthServiceCtxKey:    "auth-service",
		ManagerServiceCtxKey: "manager-service",
		BrokerServiceCtxKey:  "broker-service",
	}
)
