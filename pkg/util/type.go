package util

// ContextKey defines a type for context keys shared in the app
type ContextKey string
