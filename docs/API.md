# API

## GraphQL

[Here][graphql]

## Protobuf

[Here][protobuf]

## gRPC

[Here][grpc]

## ToDo

- move the protobuf stuff into this repository?
- generate docs from protobuf files
- generate docs from GraphQL schema

[graphql]: api/GraphQL.md
[protobuf]: api/protobuf.md
[grpc]: api/gRPC.md
