## Docker

A base image is available at the registry location [registry.gitlab.com/plantd/plantd:v2](registry-base) and was built
using `build/Dockerfile`. This image is used during CI, and as a base image for building binaries.

### Generating

This should only be done if wanting to upgrade the version of `go` for all images that use this.

```sh
docker build -t registry.gitlab.com/plantd/plantd:v2 -f build/Dockerfile .
docker push registry.gitlab.com/plantd/plantd:v2
```

[registry-base]: registry.gitlab.com/plantd/plantd:v2
