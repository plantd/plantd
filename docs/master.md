# Plantd Master

## Build

Clone and build the server.

```sh
make setup
make schema
make master/build
```

## Publishing

```sh
./tools/build build-image master v2
./tools/build push-image master v2
```

## Running

A lot of the instructions that are given here are to help get started during development, in production some things will
need to be changed according to the environment it will be running in. After the build steps have completed
the easiest way to launch the service is with the command:

```sh
./target/plantd-master
```

### Environment

This is a sample environment that can be used to run `plantd-master`. Almost any variable beginning with `PLANTD_MASTER`
can also be set in the configuration file given as `PLANTD_MASTER_CONFIG`. Also see the file `.env.development` for
additional settings.

```sh
SESSION_SECRET=plantd
AUTH_API_KEY_HEADER=x-api-key
PLANTD_MASTER_CONFIG=configs/master.yaml
PLANTD_MASTER_DB_HOST=127.0.0.1
PLANTD_MASTER_CONFIGURE_HOST=127.0.0.1
PLANTD_MASTER_CONFIGURE_PORT=4000
PLANTD_MASTER_MANAGER_HOST=127.0.0.1
PLANTD_MASTER_MANAGER_PORT=5211
PLANTD_MASTER_DB_DSN=postgres://postgres:postgres@127.0.0.1/plantd?sslmode\=disable
PLANTD_MASTER_PORT=3003
PLANTD_MASTER_PROVIDER_AUTH0_KEY=<generated at auth0>
PLANTD_MASTER_PROVIDER_AUTH0_SECRET=<generated at auth0>
PLANTD_MASTER_PROVIDER_AUTH0_DOMAIN=<generated at auth0>
PLANTD_MASTER_PROVIDER_AUTH0_SCOPES=email,profile,openid
```

### Docker

```sh
docker pull registry.gitlab.com/plantd/plantd/master:v2
docker run -it --rm --name=plantd-master \
  -v configs:configs \
  -e PLANTD_MASTER_CONFIG=configs/master.yaml \
  -e PLANTD_MASTER_DB_DSN=postgres://postgres:postgres@127.0.0.1/plantd?sslmode\=disable \
  registry.gitlab.com/plantd/plantd/master:v2
```

### API

After starting the service it should be possible to start using the GraphQL API. If it has been configured to start with
the service the [GraphQL Playground][graphql-playground] interface can be accessed at
http://localhost:3003/v1/graphql/playground.

### Authenticating

#### API Key

_This section needs work_

Users are created as part of the initial migration during development that are given a value for a user API key that can
be used as a header in a request. The name of the header can be set using the `AUTH_API_KEY_HEADER` variable, by default
this is `x-api-key`.

##### Examples

The easiest way of retrieving the API key to use with the examples is by connecting to the database using any preferred
client.

###### CuRL

```sh
curl -H 'Content-Type: application/json' \
  -H 'x-api-key: <insert-api-key-here>' \
  -d '{"query": "query{users{edges{node{email}}}}"}' \
  http://localhost:3003/v1/graphql/
```

#### OAuth2

Once the service is running it should be possible to authenticate using any of the providers that can be used with the
[goth](https://github.com/markbates/goth) package. This includes Google, Auth0, GitLab, and Github, among many others.
A sample of the `auth` section of the configuration that would be used for Google is:

```yaml
auth:
  jwt-secret: "1234"
  jwt-algorithm: "HS512"
  jwt-expire-in: "40000s"
  providers:
    - provider: "google"
      client-key: "217608228261-toul5gcg41dpcbup36rqppjchlhehanc.apps.googleusercontent.com"
      secret: "IdbSMezIfJ6C8V379_gkb35Y"
      scopes: "email,profile,openid"
```

The values for `client-key` and `secret` were from a once working (and now deactivated) set of web application
credentials created at https://console.cloud.google.com/.

Once the server has been configured for a provider the user can visit the authentication endpoint for the server, for
example at http://localhost:3003/v1/auth/google given the above configuration, and a Bearer token will be returned that
can be used in subsequent requests.

##### Examples

###### CuRL

```sh
curl -H 'Content-Type: application/json' \
  -H 'Authentication: Bearer <insert-token-here>' \
  -d '{"query": "query{users{edges{node{email}}}}"}' \
  http://localhost:3003/v1/graphql/
```

<!-- Links -->

[graphql-playground]: https://github.com/prisma-labs/graphql-playground
