# Roadmap

* [x] bring `master` into this repository
* [ ] bring `configure` into this repository
* [ ] bring `broker` into this repository
* [ ] bring `unit` into this repository
* [ ] bring `go-zapi` into this repository
* [ ] bring `go-plantd` into this repository
* [ ] bring `protobuf` API into this repository

## Changes

* [ ] merge the functionality of the `unit` service into `broker`

## Documentation

* [ ] build docs for GraphQL schema
* [ ] build docs for gRPC API
* [ ] create technical documentation using `hugo`
