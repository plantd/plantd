# Configure

[![Build Status](https://gitlab.com/plantd/configure/configure/badges/master/build.svg)](https://gitlab.com/plantd/configure/configure/commits/master)
[![Coverage Report](https://gitlab.com/plantd/configure/configure/badges/master/coverage.svg)](https://gitlab.com/plantd/configure/configure/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/configure/configure)](https://goreportcard.com/report/gitlab.com/plantd/configure/configure)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## Setup

Clone and build the server.

```sh
git clone https://gitlab.com/plantd/configure/configure
cd configure
make
sudo make install
```

## Publishing

```sh
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
  -a -tags netgo -ldflags '-w -extldflags "-static"' \
  -o bin/plantd-configure-static cmd/configure/*.go
docker build -t registry.gitlab.com/plantd/configure:v1 .
docker push registry.gitlab.com/plantd/configure:v1
```

### Running

```sh
docker pull registry.gitlab.com/plantd/configure:v1
docker run -it --rm --name=plantd-configure \
  -e PLANTD_CONFIGURE_SERVER_BIND=127.0.0.1 \
  -e PLANTD_CONFIGURE_SERVER_PORT=4000 \
	-e PLANTD_CONFIGURE_DB_HOST=127.0.0.1 \
  -e PLANTD_CONFIGURE_DB_PORT=27017 \
  -e PLANTD_CONFIGURE_DB_DBNAME=plantd \
	registry.gitlab.com/plantd/configure:v1
```
