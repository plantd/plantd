[![Build Status](https://gitlab.com/plantd/broker/badges/master/build.svg)](https://gitlab.com/plantd/broker/commits/master)
[![Coverage Report](https://gitlab.com/plantd/broker/badges/master/coverage.svg)](https://gitlab.com/plantd/broker/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/broker)](https://goreportcard.com/report/gitlab.com/plantd/broker)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

---

# Plantd Broker

Currently in beta and subject to API changes.

The message broker service is responsible for taking gRPC requests and
converting them into equivalent ZeroMQ ones that are handled by services
connected as workers.

## Setup

### Dependencies

ZeroMQ/GoMQ has instructions [here][gomq] on setup that requires

* libsodium
* libzmq
* czmq

In Arch these can be installed by

```sh
pacman -S libsodium libzmq
git clone https://aur.archlinux.org/czmq.git
cd czmq
makepkg -si
```

### Building

Build the gRPC service and messages using the protobuf repo.

```sh
git clone https://gitlab.com/plantd/broker
cd broker
make
```

### Using TLS

The `certstrap` utility from Square was used during development to create certificate files.

If a hostname is used:

```sh
certstrap --depot-path cert init --common-name "ca"
certstrap --depot-path cert request-cert --domain example.com
certstrap --depot-path cert sign example.com --CA "ca"
```

If an IP address is used:

```sh
certstrap --depot-path cert init --common-name "ca"
certstrap --depot-path cert request-cert --common-name localhost --ip 127.0.0.1
certstrap --depot-path cert sign localhost --CA "ca"
```

### Publishing

```sh
docker build -t registry.gitlab.com/plantd/broker:v1 -f ./build/Dockerfile .
docker push registry.gitlab.com/plantd/broker:v1
```

## Roadmap

* [ ] Move workers out
  > The worker processes are currently within this service, but they do not need
    to be. These should exist in the associated services, and connect to this
    broker.
* [ ] Implement the titanic pattern device
* [ ] Implement load balancing
* [ ] Add Curve for ZeroMQ security
* [ ] Move pubsub bus from other service into this one

<!-- Links -->
[gomq]: https://github.com/zeromq/gomq
