-- plantd schema mutation to de-initialize

begin;

-- drop tables with relationships first
drop table rel_users_roles;

drop table profiles;
drop table networks;
drop table users;
drop table roles;

commit;
