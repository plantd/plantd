-- plantd schema mutation to initialize

begin;

create table users (
    id varchar(45) primary key,
    email varchar(255) not null unique,
    password bytea not null,
    ip_address varchar(45),
    created_at timestamp default current_timestamp
);

create table roles (
    id varchar(25) primary key,
    name varchar(45) not null unique,
    created_at timestamp default current_timestamp
);

create table profiles (
    id varchar(25) primary key,
    first_name varchar(45) not null,
    last_name varchar(45),
    created_at timestamp default current_timestamp
);

create table networks (
    id varchar(25) primary key,
    name varchar(45) not null,
    created_at timestamp default current_timestamp
);

-- create relationship tables

create table rel_users_roles (
    user_id varchar(45) references users (id) on update cascade,
    role_id varchar(25) references roles (id) on update cascade,
    constraint users_roles_pkey primary key (user_id, role_id)
);

-- add some initial data

insert into roles values('0', 'admin');
insert into roles values('1', 'operator');

commit;
