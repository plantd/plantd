-- alter tables to add other contraints

begin;

-- alter table users
--     add foreign key (id) references profiles (id)
--         deferrable initially deferred;

alter table profiles
    add foreign key (id) references users (id)
        deferrable initially deferred;

alter table profiles
    add foreign key (id) references networks (id)
        deferrable initially deferred;

-- alter table networks
--     add foreign key (id) references profiles (id)
--         deferrable initially deferred;

commit;
