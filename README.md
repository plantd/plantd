[![Pipeline Status](https://gitlab.com/plantd/plantd/badges/master/pipeline.svg)](https://gitlab.com/plantd/plantd/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/plantd/plantd/badge.svg)](https://coveralls.io/gitlab/plantd/plantd)
[![Coverage Status](https://codecov.io/gl/plantd/plantd/branch/master/graph/badge.svg)](https://codecov.io/gl/plantd/plantd)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/plantd/plantd)](https://goreportcard.com/report/gitlab.com/plantd/plantd)
[![FOSSA Status](https://app.fossa.com/api/projects/custom%2B16630%2Fgit%40gitlab.com%3Aplantd%2Fplantd.git.svg?type=shield)](https://app.fossa.com/projects/custom%2B16630%2Fgit%40gitlab.com%3Aplantd%2Fplantd.git?ref=badge_shield)

---

# Plantd

Services and utilities for constructing and working with distributed control
systems.

This repository is currently in early stages, but it is meant to consolidate
what's available in a few others that are spread out. Notes and plans while
this takes place can be seen in the [roadmap](docs/roadmap.md).

## Services

Specific instructions for individual services have been carried over from
previous projects and are subject to change as the instructions here improve.

* [Master](docs/master.md)
* [Broker](docs/broker.md)
* [Configure](docs/configure.md)

## Develop

### Dependencies

[TODO] need to add various `go` packages that are used, eg. `go lint`.

```sh
go get -u github.com/fzipp/gocyclo
go get -u golang.org/x/tools/cmd/cover
go get -u github.com/mattn/goveralls
go get -u github.com/pressly/sup/cmd/sup
```

The `pre-commit` package needs to be installed in order to push commits.

```sh
python3 -m pip install --user pre-commit
pre-commit install
```

### Building

#### Development

The development containers are setup with hot-reloading, if everything is right
you should be able to skip ahead to [Start Services][#start-services], but if
not the containers that the stack file wants can be built with the following
commands.

```sh
./tools/build build dev
```

Once these are built the `docker-compose` command in [Start Services][#start-services]
can be used to run everything during development.

#### Deployment

To build the containers for normal execution using static binaries execute the
following command.

```sh
./tools/build build v2
```

Publishing them is done similarly, but requires access to the GitLab registry.

```sh
./tools/build push v2
```

### Start Services

The best way to start the services that `plantd` depends on is by using the
`docker-compose` stack configuration.

```sh
docker-compose -f build/stack-dev.yml up -d
```

### Setup

A database named `plantd` needs to exist in the database, this should be run once.

```sh
psql -h localhost -U postgres -c "create database plantd;"
```

Apply the migrations by installing the `migrate` utility and running it once,
changing user name and passwords as required.

```sh
./tools/setup
migrate -source file://data/1.0/ \
  -database postgres://postgres:postgres@localhost:5432/plantd?sslmode=disable up
```

If something fails and you don't care about the data in the database the `drop`
command can be run to remove all tables.

## License

Unless otherwise specified the license used for `plantd` projects is MIT. For
more information see [here](LICENSE).
