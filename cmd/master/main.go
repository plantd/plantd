package main

import (
	"os"

	"gitlab.com/plantd/plantd/internal/orm"
	"gitlab.com/plantd/plantd/pkg/server/master"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/util"
)

func main() {
	util.CheckArgs(os.Args)

	// Load configuration file
	config, err := gcontext.LoadMasterConfig(".")
	if err != nil {
		logger.Fatalf("Unable to load configuration: %s\n", err)
	}

	// Connect to the database
	db, err := orm.Factory(config)
	if err != nil {
		logger.Panicf("Unable to connect to db: %s\n", err)
	}
	defer func() {
		if err := db.DB.Close(); err != nil {
			logger.Fatalf("Unable to close db: %s\n", err)
		}
	}()

	master.Run(config, db)
}
