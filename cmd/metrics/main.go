package main

import (
	"os"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/util"
	"gitlab.com/plantd/plantd/pkg/server/metrics"
)

func main() {
	util.CheckArgs(os.Args)

	config, err := context.LoadMetricsConfig()
	if err != nil {
		logger.Errorf("configuration error: %s", err)
		os.Exit(1)
	}

	metrics.Run(config)
}
