package main

import (
	"os"

	"gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/util"
	"gitlab.com/plantd/plantd/pkg/server/configure"
)

func main() {
	util.CheckArgs(os.Args)

	config, err := context.LoadConfigureConfig(".")
	if err != nil {
		logger.Fatalf("failed to read configuration: %s\n", err)
	}

	db, err := context.OpenMongoDB(config)
	if err != nil {
		logger.Fatalf("unable to connect to db: %s\n", err)
	}

	configure.Run(config, db)
}
