package main

import (
	"os"

	gcontext "gitlab.com/plantd/plantd/internal/context"
	"gitlab.com/plantd/plantd/internal/logger"
	"gitlab.com/plantd/plantd/internal/util"
	"gitlab.com/plantd/plantd/pkg/server/broker"
)

func main() {
	util.CheckArgs(os.Args)

	config, err := gcontext.LoadBrokerConfig()
	if err != nil {
		logger.Errorf("configuration error: %s", err)
		os.Exit(1)
	}

	broker.Run(config)
}
